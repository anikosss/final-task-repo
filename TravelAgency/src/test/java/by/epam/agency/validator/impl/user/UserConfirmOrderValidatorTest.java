package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class UserConfirmOrderValidatorTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private UserConfirmOrderValidator validator = UserConfirmOrderValidator.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addSessionAttribute("user", new User(1, "login", "password", "email", RolesEnum.ADMIN));
        context.addRequestParameter("cart", new String[]{ "2" });
        context.addRequestParameter("name", null);
        context.addRequestParameter("phone", new String[] { "wrong phone" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(UserConfirmOrderValidator.getInstance(), validator);
    }

    @Ignore
    @Test
    public void testWrongCart() {
        context.addRequestParameter("cart", new String[]{ "3" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testEmptyParam() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongParam() {
        context.addRequestParameter("name", new String[]{ "Ivan" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongUserCart() {
        context.addRequestParameter("cart", new String[]{ "8" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("name", new String[]{ "Ivan" });
        context.addRequestParameter("phone", new String[]{ "+375(25)646-14-40" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}