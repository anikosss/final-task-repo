package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class MakeTourHotValidatorTest extends Assert {
    private MakeTourHotValidator validator = MakeTourHotValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("tour", null);
        context.addRequestParameter("sale", null);
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(MakeTourHotValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyTour() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR, validator.validate(context));
    }

    @Test
    public void testWrongTour() {
        context.addRequestParameter("sale", new String[]{ "15" });
        context.addRequestParameter("tour", new String[]{ "wrong tour" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_TOUR, validator.validate(context));
    }

    @Ignore
    @Test
    public void testEmptySale() {
        context.addRequestParameter("tour", new String[]{ "9" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_SALE, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongSale() {
        context.addRequestParameter("tour", new String[]{ "9" });
        context.addRequestParameter("sale", new String[]{ "120" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_SALE, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("tour", new String[]{ "9" });
        context.addRequestParameter("sale", new String[]{ "15" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}