package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class AddToCartValidatorTest extends Assert {
    private AddToCartValidator validator = AddToCartValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("tour", null);
        context.addRequestParameter("hotel", null);
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(AddToCartValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyTour() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR, validator.validate(context));
    }

    @Ignore
    @Test
    public void testEmptyHotel() {
        context.addRequestParameter("tour", new String[]{ "9" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_HOTEL, validator.validate(context));
    }

    @Test
    public void testWrongTour() {
        context.addRequestParameter("tour", new String[]{ "wrong_tour" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_TOUR, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongHotel() {
        context.addRequestParameter("tour", new String[]{ "9" });
        context.addRequestParameter("hotel", new String[]{ "wrong_hotel" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistInCart() {
        User user = new User();
        user.setId(1);
        context.addSessionAttribute("user", user);
        context.addRequestParameter("tour", new String[]{ "9" });
        context.addRequestParameter("hotel", new String[]{ "15" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongHotelInTour() {
        context.addRequestParameter("tour", new String[]{ "8" });
        context.addRequestParameter("hotel", new String[]{ "15" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        User user = new User();
        user.setId(1);
        context.addSessionAttribute("user", user);
        context.addRequestParameter("tour", new String[]{ "9" });
        context.addRequestParameter("hotel", new String[]{ "17" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}