package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class PlaceInfoValidatorTest extends Assert {
    private PlaceInfoValidator validator = PlaceInfoValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("title", null);
        context.addRequestParameter("country", null);
        context.addRequestParameter("image", new String[] { "wrong image" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(PlaceInfoValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyCountry() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_COUNTRY, validator.validate(context));
    }

    @Test
    public void testWrongCountry() {
        context.addRequestParameter("country", new String[]{ "wrong country" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY, validator.validate(context));
    }

    @Test
    public void testEmptyFields() {
        context.addRequestParameter("country", new String[] { "1" } );
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongFields() {
        context.addRequestParameter("country", new String[] { "1" } );
        context.addRequestParameter("title", new String[] { "title" } );
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("country", new String[] { "1" } );
        context.addRequestParameter("title", new String[] { "title" } );
        context.addRequestParameter("image", new String[] { "place-title.jpg" } );
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}