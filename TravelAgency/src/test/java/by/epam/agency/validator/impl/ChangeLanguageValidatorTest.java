package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ChangeLanguageValidatorTest extends Assert {
    private RequestContext context = new RequestContext();
    private ChangeLanguageValidator validator = ChangeLanguageValidator.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addRequestParameter("lang", null);
        System.out.println("set up");
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ChangeLanguageValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyLang() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_LANGUAGE, validator.validate(context));
    }

    @Test
    public void testWrongLang() {
        context.addRequestParameter("lang", new String[] { "wrong lang" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_LANGUAGE, validator.validate(context));
    }


    @Test
    public void testValidate() {
        context.addRequestParameter("lang", new String[] { "en_EN" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}