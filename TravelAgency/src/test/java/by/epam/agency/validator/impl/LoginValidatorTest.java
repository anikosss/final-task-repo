package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.*;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class LoginValidatorTest extends Assert {
    private LoginValidator validator = LoginValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("login", null);
        context.addRequestParameter("password", null);
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(LoginValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyParams() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("login", new String[]{ "wrong login" });
        context.addRequestParameter("password", new String[]{ "wrong password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongUser() {
        context.addRequestParameter("login", new String[]{ "wrongLogin" });
        context.addRequestParameter("password", new String[]{ "wrongPassword" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_LOGIN, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongPassword() {
        context.addRequestParameter("login", new String[]{ "admin" });
        context.addRequestParameter("password", new String[]{ "wrongPassword" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("login", new String[]{ "admin" });
        context.addRequestParameter("password", new String[]{ "qweqwe" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}