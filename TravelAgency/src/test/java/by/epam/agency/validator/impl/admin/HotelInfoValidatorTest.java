package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HotelInfoValidatorTest extends Assert {
    private HotelInfoValidator validator = HotelInfoValidator.getInstance();
    private RequestContext context = new RequestContext();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addRequestParameter("title", null);
        context.addRequestParameter("stars", new String[] { "wrong value" });
        context.addRequestParameter("allRooms", new String[] { "22" });
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(HotelInfoValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyParams() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("title", new String[]{ "title" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("title", new String[]{ "title" });
        context.addRequestParameter("stars", new String[]{ "4" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}