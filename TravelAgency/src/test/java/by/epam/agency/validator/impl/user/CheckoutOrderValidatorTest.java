package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class CheckoutOrderValidatorTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private CheckoutOrderValidator validator = CheckoutOrderValidator.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addSessionAttribute("user", new User(1, "asd", "asd", "asd", RolesEnum.USER));
        context.addRequestParameter("cart", null);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(CheckoutOrderValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyCart() {
        context.addRequestParameter("cart", null);
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Test
    public void testWrongCart() {
        context.addRequestParameter("cart", new String[]{ "wrong cart" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongUserCart() {
        context.addRequestParameter("cart", new String[]{ "2" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("cart", new String[]{ "2" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}