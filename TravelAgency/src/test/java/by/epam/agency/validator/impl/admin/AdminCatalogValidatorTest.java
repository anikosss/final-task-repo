package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AdminCatalogValidatorTest extends Assert {
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("page", null);
        context.addRequestParameter("action", new String[]{ "all_tours" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(AdminCatalogValidator.getInstance(), validator);
    }

    @Test
    public void testWrongPage() {
        context.addRequestParameter("page", new String[]{ "wrong page" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PAGE, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("page", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}