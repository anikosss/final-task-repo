package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ShoppingCartValidatorTest extends Assert {
    private ShoppingCartValidator validator = ShoppingCartValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("cart", null);
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ShoppingCartValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyCart() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Test
    public void testWrongCart() {
        context.addRequestParameter("cart", new String[]{ "3" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_CART, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("cart", new String[]{ "2" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}