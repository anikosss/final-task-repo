package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ToursCatalogValidatorTest extends Assert {
    private ToursCatalogValidator validator = ToursCatalogValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addRequestParameter("action", new String[]{ "view_catalog" });
        context.addRequestParameter("page", null);
        context.addRequestParameter("country", null);
        context.addRequestParameter("place", null);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ToursCatalogValidator.getInstance(), validator);
    }

    @Test
    public void testWrongCountry() {
        context.addRequestParameter("action", new String[]{ "nearest_catalog" } );
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY, validator.validate(context));
    }

    @Test
    public void testWrongPlace() {
        context.addRequestParameter("action", new String[]{ "place_catalog" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PLACE, validator.validate(context));
    }

    @Test
    public void testWrongPage() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PAGE, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("page", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }

}