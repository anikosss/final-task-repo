package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ChoosePlaceValidatorTest extends Assert {
    private ChoosePlaceValidator validator = ChoosePlaceValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("place", null);
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ChoosePlaceValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyPlace() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_PLACE, validator.validate(context));
    }

    @Test
    public void testWrongPlace() {
        context.addRequestParameter("place", new String[]{ "wrong place" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PLACE, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongPlaceInCountry() {
        context.addRequestParameter("country", new String[]{ "1" });
        context.addRequestParameter("place", new String[] { "6" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("country", new String[]{ "1" });
        context.addRequestParameter("place", new String[] { "5" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}