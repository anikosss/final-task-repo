package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HotelValidatorTest extends Assert {
    private HotelValidator validator = HotelValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("hotel", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(HotelValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyHotel() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_HOTEL, validator.validate(context));
    }

    @Test
    public void testWrongHotel() {
        context.addRequestParameter("hotel", new String[]{ "wrong hotel" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("hotel", new String[] { "14" } );
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}