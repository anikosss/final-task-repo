package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.*;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class RegistrationValidatorTest extends Assert {
    private RegistrationValidator validator = RegistrationValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("login", null);
        context.addRequestParameter("password", new String[] { "password" });
        context.addRequestParameter("repeatPassword", new String[] { "Password" });
        context.addRequestParameter("email", new String[] { "wrong_email" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(RegistrationValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyFields() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongFields() {
        context.addRequestParameter("login", new String[] { "login" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testRepeatPassword() {
        context.addRequestParameter("login", new String[] { "login" });
        context.addRequestParameter("email", new String[] { "login@mail.ru" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistLogin() {
        context.addRequestParameter("login", new String[]{ "admin" });
        context.addRequestParameter("email", new String[] { "login@mail.ru" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_LOGIN, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistEmail() {
        context.addRequestParameter("login", new String[]{ "newLogni" });
        context.addRequestParameter("email", new String[] { "admin@mail.ru" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_EMAIL, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("login", new String[]{ "newlogin" });
        context.addRequestParameter("email", new String[] { "login@mail.ru" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}