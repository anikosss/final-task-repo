package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ChangeEntityValidatorTest extends Assert {
    private ChangeEntityValidator validator = ChangeEntityValidator.getInstance();
    private RequestContext hotelContext = new RequestContext();
    private RequestContext placeContext = new RequestContext();
    private RequestContext tourContext = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        hotelContext.addRequestParameter("action", new String[]{ "change_hotel" });
        placeContext.addRequestParameter("action", new String[]{ "change_place" });
        tourContext.addRequestParameter("action", new String[]{ "change_tour" });
        hotelContext.addRequestParameter("hotel", null);
        hotelContext.addRequestParameter("title", null);
        hotelContext.addRequestParameter("stars", new String[] { "wrong value" });
        hotelContext.addRequestParameter("allRooms", new String[] { "22" });
        placeContext.addRequestParameter("place", null);
        placeContext.addRequestParameter("title", null);
        placeContext.addRequestParameter("country", null);
        placeContext.addRequestParameter("image", new String[] { "wrong image" });
        tourContext.addRequestParameter("arriveDate", new String[] { "unsupported date format" });
        tourContext.addRequestParameter("returnDate", new String[] { "2016-05-05" });
        tourContext.addRequestParameter("arriveTime", new String[] { "22:22:22" });
        tourContext.addRequestParameter("returnTime", new String[] { "22:22:22" });
        tourContext.addRequestParameter("price", null);
        tourContext.addRequestParameter("seatsClass", new String[] { "econom" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        hotelContext.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ChangeEntityValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyHotel() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_HOTEL, validator.validate(hotelContext));
    }

    @Test
    public void testWrongHotel() {
        hotelContext.addRequestParameter("hotel", new String[]{ "wrong hotel" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL, validator.validate(hotelContext));
    }

    @Ignore
    @Test
    public void testEmptyHotelParams() {
        hotelContext.addRequestParameter("hotel", new String[]{ "14" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(hotelContext));
    }

    @Ignore
    @Test
    public void testWrongHotelParams() {
        hotelContext.addRequestParameter("hotel", new String[]{ "14" });
        hotelContext.addRequestParameter("title", new String[]{ "title" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(hotelContext));
    }

    @Test
    public void testEmptyPlace() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_PLACE, validator.validate(placeContext));
    }

    @Test
    public void testWrongPlace() {
        placeContext.addRequestParameter("place", new String[]{ "wrong place" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PLACE, validator.validate(placeContext));
    }

    @Ignore
    @Test
    public void testEmptyPlaceParams() {
        placeContext.addRequestParameter("place", new String[] { "5" });
        placeContext.addRequestParameter("country", new String[] { "1" } );
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(placeContext));
    }

    @Ignore
    @Test
    public void testWrongPlaceParams() {
        placeContext.addRequestParameter("place", new String[] { "5" });
        placeContext.addRequestParameter("country", new String[] { "1" } );
        placeContext.addRequestParameter("title", new String[] { "title" } );
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(placeContext));
    }

    @Test
    public void testEmptyTour() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR, validator.validate(tourContext));
    }

    @Test
    public void testWrongTour() {
        tourContext.addRequestParameter("tour", new String[]{ "wrong tour" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_TOUR, validator.validate(tourContext));
    }

    @Ignore
    @Test
    public void testEmptyTourParams() {
        tourContext.addRequestParameter("tour", new String[]{ "8" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(tourContext));
    }

    @Ignore
    @Test
    public void testWrongTourParams() {
        tourContext.addRequestParameter("tour", new String[]{ "8" });
        tourContext.addRequestParameter("price", new String[]{ "1200" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(tourContext));
    }

    @Ignore
    @Test
    public void testValidate() {
        hotelContext.addRequestParameter("hotel", new String[]{ "14" });
        hotelContext.addRequestParameter("title", new String[]{ "title" });
        hotelContext.addRequestParameter("stars", new String[]{ "4" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(hotelContext));
    }
}