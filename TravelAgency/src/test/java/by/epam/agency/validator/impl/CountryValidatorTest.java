package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CountryValidatorTest extends Assert {
    private CountryValidator validator = CountryValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("country", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }


    @Test
    public void testGetInstance() {
        assertEquals(CountryValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyCountry() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_COUNTRY, validator.validate(context));
    }

    @Test
    public void testWrongCountry() {
        context.addRequestParameter("country", new String[]{ "wrong country" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("country", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}