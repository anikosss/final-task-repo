package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.MessagesEnum;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class TourInfoValidatorTest extends Assert {
    private TourInfoValidator validator = TourInfoValidator.getInstance();
    private RequestContext context = new RequestContext();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addRequestParameter("arriveDate", new String[] { "unsupported date format" });
        context.addRequestParameter("returnDate", new String[] { "2016-05-05" });
        context.addRequestParameter("arriveTime", new String[] { "22:22:22" });
        context.addRequestParameter("returnTime", new String[] { "22:22:22" });
        context.addRequestParameter("price", null);
        context.addRequestParameter("seatsClass", new String[] { "econom" });
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(TourInfoValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyParams() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("price", new String[]{ "1200" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testWrongTime() {
        context.addRequestParameter("price", new String[]{ "1200" });
        context.addRequestParameter("arriveDate", new String[]{ "2016-05-05" });
        context.addRequestParameter("arriveTime", new String[] { "22:22" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_DATE_FORMAT, validator.validate(context));
    }

    @Ignore
    @Test
    public void testWrongDate() {
        context.addRequestParameter("arriveDate", new String[] { "2016-02-12" });
        context.addRequestParameter("returnDate", new String[] { "2016-02-13" });
        context.addRequestParameter("price", new String[]{ "1200" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_DATE, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("arriveDate", new String[] { "2016-05-01" });
        context.addRequestParameter("price", new String[]{ "1200" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }

}