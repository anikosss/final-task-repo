package by.epam.agency.pool;

import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

public class ConnectionsPoolTest extends Assert {
    private static final Logger LOGGER = Logger.getLogger(ConnectionsPoolTest.class);
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private int size;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        size = Integer.parseInt(ConfigManager.getParam(ConfigEnum.DB_SIZE));
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testInit() {
        assertEquals(size, pool.getConnectionsCount());
    }

    @Test(timeout = 10000)
    public void testGetConnection() {
        ArrayList<PoolConnection> temp = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            PoolConnection connection = pool.getConnection();
            temp.add(connection);
        }
        int expected = 3;
        int actual = pool.getFreeConnectionsCount();
        assertEquals(expected, actual);
        for (PoolConnection poolConnection : temp) {
            pool.returnConnection(poolConnection);
        }
        assertEquals(10, pool.getFreeConnectionsCount());
    }

    @Test
    public void testReturnConnection() throws SQLException {
        ArrayList<PoolConnection> temp = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            PoolConnection connection = pool.getConnection();
            temp.add(connection);
        }
        for (PoolConnection connection : temp) {
            connection.close();
        }
        int actual = pool.getFreeConnectionsCount();
        assertEquals(size, actual);
        try(PoolConnection connection = pool.getConnection()) {
            assertEquals(pool.getFreeConnectionsCount(), 9);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        assertEquals(10, pool.getFreeConnectionsCount());
        System.out.println(pool.getConnectionsCount());
    }
}