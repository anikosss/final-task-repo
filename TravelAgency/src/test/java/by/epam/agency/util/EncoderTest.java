package by.epam.agency.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EncoderTest extends Assert {
    private String key;
    private String text;
    
    @Before
    public void setUp() {
        key = "KeyForEncodingInformation";
        text = "qweqwe";
    }

    @Test
    public void testEncode() {
        String encoded = Encoder.encode(text);
        String decoded = Encoder.decode(encoded);
        System.out.println(encoded);
        assertEquals(text, decoded);
    }

}