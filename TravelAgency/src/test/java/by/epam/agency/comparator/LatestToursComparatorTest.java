package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LatestToursComparatorTest extends Assert {
    private List<Tour> list = new ArrayList<>();
    
    @Before
    public void setUp() {
        list.add(new Tour());
        list.get(0).setId(1);
        list.add(new Tour());
        list.get(1).setId(6);
        list.add(new Tour());
        list.get(2).setId(2);
    }

    @After
    public void tearDown() throws Exception {
        list.clear();
    }

    @Test
    public void testCompare() {
        Collections.sort(list, Collections.reverseOrder(new LatestToursComparator()));
        assertEquals(6, list.get(0).getId());
    }
}