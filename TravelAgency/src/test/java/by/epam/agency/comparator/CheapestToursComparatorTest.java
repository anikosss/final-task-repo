package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class CheapestToursComparatorTest extends Assert {
    private Tour tour1 = new Tour();
    private Tour tour2 = new Tour();
    private ArrayList<Tour> list = new ArrayList<>();
    
    @Before
    public void setUp() {
        tour1.setPrice(2000);
        tour2.setPrice(1000);
        list.add(tour1);
        list.add(tour2);
    }

    @After
    public void tearDown() throws Exception {
        list.clear();
    }

    @Test
    public void testCompare() {
        Collections.sort(list, new CheapestToursComparator());
        assertEquals(1000, list.get(0).getPrice(), 0.001);
    }
}