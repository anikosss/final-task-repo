package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NearestToursComparatorTest extends Assert {
    private List<Tour> tours = new ArrayList<>();
    
    @Before
    public void setUp() {
        Tour tour1 = new Tour();
        Tour tour2 = new Tour();
        tour1.setArriveDate(Date.valueOf("2016-02-12"));
        tour2.setArriveDate(Date.valueOf("2016-02-11"));
        tours.add(tour1);
        tours.add(tour2);
    }

    @Ignore
    @Test
    public void testCompare() {
        Collections.sort(tours, new NearestToursComparator());
        assertEquals(Date.valueOf("2016-02-11"), tours.get(0).getArriveDate());
    }
}