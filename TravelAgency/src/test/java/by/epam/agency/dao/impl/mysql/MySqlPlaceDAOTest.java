package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.util.List;

public class MySqlPlaceDAOTest extends Assert {
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<Place> placeDAO;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        placeDAO = factory.getPlaceDAO();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        assertEquals(MySqlPlaceDAO.getInstance(), placeDAO);
    }

    @Ignore
    @Test
    public void testCreate() {
        Place place = new Place(1, "Budva", MySqlCountryDAO.getInstance().read(1), null);
        boolean result = placeDAO.create(place);
        assertTrue(result);
    }

    @Ignore
    @Test
    public void testRead() {
        Place place = placeDAO.read(1);
        System.out.println(place.getHotels());
        assertEquals("Budva", place.getTitle());
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<Place> list = placeDAO.readAll();
        assertEquals(1, list.size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        Place place = placeDAO.read(1);
        place.setTitle("Bechichi");
        assertTrue(placeDAO.update(place));
        assertEquals("Bechichi", placeDAO.read(1).getTitle());
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(placeDAO.delete(8));
    }

}