package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.util.Encoder;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.util.List;

public class MySqlUserDAOTest extends Assert {
    private IDAOFactory factory;
    IBaseDAO<User> userDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        userDAO = factory.getUserDAO();
        pool.init();
    }

    @After
    public void tearDown() {
        pool.destroy();
    }

    @Ignore
    @Test
    public void testGetInstance() {
        IBaseDAO<User> tempDAO = factory.getUserDAO();
        assertEquals(userDAO, tempDAO);
    }

    @Ignore
    @Test
    public void testRead() {
        User user = userDAO.read(1);
        assertEquals(1, user.getId());
    }

    @Ignore
    @Test
    public void testCreate() {
        User user = new User(1, "qweqwe", Encoder.encode("qweqwe"), "qwe@gmail.com", RolesEnum.USER);
        boolean result = userDAO.create(user);
        assertEquals(true, result);
        User dbUser = userDAO.read(1);
        assertEquals("qweqwe", dbUser.getLogin());
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<User> list = userDAO.readAll();
        assertEquals(1, list.size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        User newUser = new User(1, "admin", Encoder.encode("qweqwe"), "qwe@mail.ru", RolesEnum.ADMIN);
        boolean result = userDAO.update(newUser);
        User dbUser = userDAO.read(1);
        assertEquals(true, result);
        assertEquals("admin", dbUser.getLogin());
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(userDAO.delete(3));
    }

    @Ignore
    @Test
    public void testExecute() {
        List<User> list = userDAO.execute(QueriesEnum.USER_BY_LOGIN, new String[]{ "admin" });
        assertEquals(1, list.get(0).getId());
    }

}