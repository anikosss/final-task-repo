package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.util.SeatsClassesEnum;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class MySqlTourDAOTest extends Assert {
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<Tour> tourDAO;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        tourDAO = factory.getTourDAO();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Ignore
    @Test
    public void testGetInstance() {
        assertEquals(MySqlTourDAO.getInstance(), MySqlTourDAO.getInstance());
    }

    @Ignore
    @Test
    public void testCreate() {
        Tour tour = new Tour(1, Date.valueOf("2015-11-15"), Time.valueOf("15:15:15"), Date.valueOf("2015-11-25"),
                Time.valueOf("15:15:15"), 12, 8, 1200, SeatsClassesEnum.ECONOM, "default" , 0,
                MySqlPlaceDAO.getInstance().read(5));
        assertTrue(tourDAO.create(tour));
    }

    @Ignore
    @Test
    public void testRead() {
        int key = 6;
        Tour tour = tourDAO.read(key);
        assertEquals(6, tour.getId());
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<Tour> list = tourDAO.readAll();
        assertEquals(2, list.size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        Tour tour = new Tour(6, Date.valueOf("2015-11-15"), Time.valueOf("15:15:15"), Date.valueOf("2015-11-25"),
                Time.valueOf("15:15:15"), 12, 8, 1200, SeatsClassesEnum.ECONOM, "hot" , 5,
                MySqlPlaceDAO.getInstance().read(1));
        tourDAO.update(tour);
        Tour dbTour = tourDAO.read(6);
        assertEquals(tour.getSale(), dbTour.getSale());
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(tourDAO.delete(11));
    }

}