package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.util.List;

public class MySqlCountryDAOTest extends Assert {
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<Country> countryDAO;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        countryDAO = factory.getCountryDAO();
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        assertEquals(countryDAO, MySqlCountryDAO.getInstance());
    }

    @Ignore
    @Test
    public void testCreate() {
        Country country = new Country(1, "Greece", "description.country.greece");
        assertTrue(countryDAO.create(country));
    }

    @Ignore
    @Test
    public void testRead() {
        Country country = countryDAO.read(1);
        assertEquals(country.getId(), 1);
        assertEquals(country.getName(), "Montenegro");
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<Country> list = countryDAO.readAll();
        assertEquals(2, list.size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        Country country = new Country(2, "ddd", "ddd", "ddd");
        assertTrue(countryDAO.update(country));
        assertEquals(countryDAO.read(2).getName(), "ddd");
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(countryDAO.delete(4));
    }

}