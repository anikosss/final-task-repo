package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.util.List;

public class MySqlHotelDAOTest extends Assert {
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<Hotel> hotelDAO;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        hotelDAO = factory.getHotelDAO();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        assertEquals(MySqlHotelDAO.getInstance(), hotelDAO);
    }

    @Ignore
    @Test
    public void testCreate() {
        Hotel hotel = new Hotel(1, "Dimic Elite", 4, 8, 1);
        assertTrue(hotelDAO.create(hotel));
    }

    @Ignore
    @Test
    public void testRead() {
        Hotel hotel = hotelDAO.read(3);
        assertEquals(8, hotel.getAllRooms());
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<Hotel> list = hotelDAO.readAll();
        assertEquals(3, list.size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        Hotel hotel = hotelDAO.read(3);
        hotel.setAllRooms(10);
        assertTrue(hotelDAO.update(hotel));
        assertEquals(10, hotelDAO.read(3).getAllRooms());
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(hotelDAO.delete(17));
    }

    @Ignore
    @Test
    public void testReadByPlaceId() {
        int key = 1;
        List<Hotel> list = ((MySqlHotelDAO) hotelDAO).readByPlaceId(key);
        assertEquals(2, list.size());
    }

}