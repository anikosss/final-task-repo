package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

import java.util.List;

public class MySqlShoppingCartDAOTest extends Assert {
    private IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<ShoppingCart> shoppingCartDAO = factory.getShoppingCartDAO();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        assertEquals(MySqlShoppingCartDAO.getInstance(), shoppingCartDAO);
    }

    @Ignore
    @Test
    public void testCreate() {
        User user = MySqlUserDAO.getInstance().read(1);
        Tour tour = MySqlTourDAO.getInstance().read(12);
        Hotel hotel = MySqlHotelDAO.getInstance().read(8);
        ShoppingCart shoppingCart = new ShoppingCart(1, user, tour, hotel);
        assertTrue(shoppingCartDAO.create(shoppingCart));
    }

    @Ignore
    @Test
    public void testRead() {
        ShoppingCart shoppingCart = shoppingCartDAO.read(1);
        assertEquals(12, shoppingCart.getTour().getId());
    }

    @Ignore
    @Test
    public void testReadAll() {
        List<ShoppingCart> list = shoppingCartDAO.readAll();
        assertEquals(1, list.size());
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(shoppingCartDAO.delete(9));
    }

    @Ignore
    @Test
    public void testUpdate() {
        ShoppingCart shoppingCart = shoppingCartDAO.read(1);
        Tour tour = MySqlTourDAO.getInstance().read(14);
        shoppingCart.setTour(tour);
        assertTrue(shoppingCartDAO.update(shoppingCart));
    }

    @Ignore
    @Test
    public void testExecute() {
        List<ShoppingCart> list = shoppingCartDAO.execute(QueriesEnum.SHOPPING_CART_BY_USER_ID, new String[] { "1" });
        assertEquals(1, list.size());
    }
}