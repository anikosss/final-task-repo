package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class MySqlOrderDAOTest extends Assert {
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private IBaseDAO<Order> orderDAO;
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        orderDAO = factory.getOrderDAO();
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Ignore
    @Test
    public void testGetInstance() {
        assertEquals(MySqlOrderDAO.getInstance(), orderDAO);
    }

    @Ignore
    @Test
    public void testCreate() {
        Tour tour = MySqlTourDAO.getInstance().read(8);
        User user = MySqlUserDAO.getInstance().read(1);
        Hotel hotel = MySqlHotelDAO.getInstance().read(14);
        Order order = new Order(1, "Mike", "+1432452352", tour, user, hotel);
        assertTrue(orderDAO.create(order));
    }

    @Ignore
    @Test
    public void testRead() {
        Order order = orderDAO.read(5);
        assertEquals(5, order.getId());
    }

    @Ignore
    @Test
    public void testReadAll() {
        assertEquals(1, orderDAO.readAll().size());
    }

    @Ignore
    @Test
    public void testUpdate() {
        Order order = orderDAO.read(5);
        order.setStatus("confirmed");
        assertTrue(orderDAO.update(order));
    }

    @Ignore
    @Test
    public void testDelete() {
        assertTrue(orderDAO.delete(1));
    }

    @Ignore
    @Test
    public void testExecute() {
        assertEquals(1, orderDAO.execute(QueriesEnum.ORDER_BY_HOTEL_ID, 14).size());
    }
}