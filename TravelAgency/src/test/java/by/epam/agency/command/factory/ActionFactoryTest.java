package by.epam.agency.command.factory;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.EmptyCommand;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActionFactoryTest extends Assert {
    ActionFactory factory = ActionFactory.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
    }

    @Test
    public void testGetInstance() {
        assertEquals(ActionFactory.getInstance(), factory);
    }

    @Test
    public void testDefineCommand() throws Exception {
        ActionCommand actual = ActionFactory.defineCommand("unsupported_param");
        assertEquals(actual.getClass(), new EmptyCommand().getClass());
    }
}