package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.factory.ActionFactory;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class DeleteHotelCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ActionCommand command = ActionFactory.defineCommand("delete_hotel");
    private static final String INDEX = PagesManager.getPage(PagesEnum.INDEX);
    private static final String GO_TO_ALL_HOTELS = PagesManager.getPage(PagesEnum.GO_TO_ALL_HOTELS);
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        context.addRequestParameter("hotel", null);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        assertEquals(INDEX, command.execute(context));
        context.addSessionAttribute("user", new User(0, "login", "password", "email@mail.ru", RolesEnum.ADMIN));
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("errorMessage"));
        context.addRequestParameter("hotel", new String[]{ });
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("errorMessage"));
        context.addRequestParameter("hotel", new String[]{ "wrong hotel" });
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("errorMessage"));
        context.addRequestParameter("hotel", new String[]{ "12314" });
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("errorMessage"));
        context.addRequestParameter("hotel", new String[]{ "4" });
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("successMessage"));
    }
}