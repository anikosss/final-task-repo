package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.factory.ActionFactory;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.pool.ConnectionsPool;
import org.junit.*;

public class LoginCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        context.addRequestParameter("admin", new String[] { "qweqwe" });
        context.addRequestParameter("password", new String[] { "qweqwe" });
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("login");
        command.execute(context);
        User user = (User) context.getSessionAttribute("user");
        assertNotNull(user);
        assertEquals(user.getLogin(), context.getRequestParameter("admin")[0]);
    }
}