package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.factory.ActionFactory;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class AddHotelCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ActionCommand command;
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private static final String INDEX = PagesManager.getPage(PagesEnum.INDEX);
    private static final String GO_TO_ALL_HOTELS = PagesManager.getPage(PagesEnum.GO_TO_ALL_HOTELS);
    private static final String CHOOSE_COUNTRY = PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_HOTEL_COUNTRY);
    private static final String CHOOSE_PLACE = PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_HOTEL_PLACE);
    private static final String WRITE_INFO = PagesManager.getPage(PagesEnum.ADMIN_WRITE_HOTEL_INFO);
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        command = ActionFactory.defineCommand("add_hotel");
        context.addSessionAttribute("user", new User(1, "login", "password", "email@mail.ru", RolesEnum.USER));
        context.addRequestParameter("country", new String[] { "unsupported_format" });
        context.addRequestParameter("place", new String[] { "22" } );
        context.addRequestParameter("title", new String[] { "some title" });
        context.addRequestParameter("stars", new String[] { "1231" });
        context.addRequestParameter("allRooms", new String[] { "20" });
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        assertEquals(INDEX, command.execute(context));
        context.addSessionAttribute("user", new User(1, "login", "password", "email@mail.ru", RolesEnum.ADMIN));
        assertEquals(CHOOSE_COUNTRY, command.execute(context));
        context.addRequestParameter("country", new String[] { "1" });
        assertEquals(CHOOSE_PLACE, command.execute(context));
        context.addRequestParameter("place", new String[] { "1" } );
        assertEquals(WRITE_INFO, command.execute(context));
        context.addRequestParameter("stars", new String[] { "5" });
        assertEquals(GO_TO_ALL_HOTELS, command.execute(context));
        assertNotNull(context.getRequestAttribute("successMessage"));
    }
}