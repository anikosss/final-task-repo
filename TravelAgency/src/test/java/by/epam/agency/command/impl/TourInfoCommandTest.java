package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.factory.ActionFactory;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.ConfigEnum;
import by.epam.agency.manager.ConfigManager;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TourInfoCommandTest extends Assert {
    private ActionCommand command = ActionFactory.defineCommand("tour_info");
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private RequestContext context = new RequestContext();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("tour", new String[] { "12" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test
    public void testExecute() {
        command.execute(context);
    }
}