package by.epam.agency.command;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmptyCommandTest {
    private EmptyCommand command;

    @Before
    public void setUp() {
        command = new EmptyCommand();
    }

    @Test
    public void testExecute() {
        String actual = command.execute(new RequestContext());
        Assert.assertEquals(PagesManager.getPage(PagesEnum.ERROR_404), actual);
    }
}