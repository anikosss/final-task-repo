package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.command.factory.ActionFactory;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class RegistrationCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();
    
    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure("src/main/resources/config/log4j-test.xml", LogManager.getLoggerRepository());
        String[] arrLogin = {"login1"};
        String[] arrPassword = {"pass1111"};
        String[] arrRepeatPassword = {"pass1111"};
        String[] arrEmail = {"email1@mail.com"};
        context.addRequestParameter("login", arrLogin);
        context.addRequestParameter("password", arrPassword);
        context.addRequestParameter("repeatPassword", arrRepeatPassword);
        context.addRequestParameter("email", arrEmail);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("registration");
        String page = command.execute(context);
        assertEquals("main-page.jsp", page);
    }
}