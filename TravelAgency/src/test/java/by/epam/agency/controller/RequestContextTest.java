package by.epam.agency.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class RequestContextTest extends Assert {
    private Map<String, Object> requestAttributes = new HashMap<>();
    private Map<String, String[]> requestParameters = new HashMap<>();
    private Map<String, Object> sessionAttributes  = new HashMap<>();

    @Before
    public void setUp() {
        requestAttributes.put("key1", "value1");
        String[] strArray = { "value2", "value3" };
        requestParameters.put("key2", strArray);
        sessionAttributes.put("key3", "value4");
    }

    @After
    public void tearDown() throws Exception {
        requestAttributes.clear();
        requestParameters.clear();
        sessionAttributes.clear();
    }

    @Test
    public void testClearContext() {
        requestAttributes.clear();
        requestParameters.clear();
        sessionAttributes.clear();
        assertEquals(0, requestAttributes.size());
        assertEquals(0, requestParameters.size());
        assertEquals(0, sessionAttributes.size());
    }
}