package by.epam.agency.manager;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <p>Class for returning a config parameter by key</p>
 *
 * @author Dmitry Anikeichenko
 */
public class ConfigManager {
    private static final Logger LOGGER = Logger.getLogger(ConfigManager.class);
    private static final String PATH = "/config/config.properties";
    private static Properties props;

    /**
     * <p>Method to get the configuration parameter by key from properties</p>
     *
     * @param key key for the parameter
     * @return value from properties by key
     */
    public static String getParam(ConfigEnum key) {
        if (props == null) {
            init();
        }
        return props.getProperty(key.getValue());
    }

    /**
     * <p>Initializing properties</p>
     */
    private static void init() {
        try(InputStream inputStream = MessagesManager.class.getResourceAsStream(PATH)) {
            props = new Properties();
            props.load(inputStream);
        } catch (IOException e) {
            LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_CONFIG_LOAD_FAILED), e);
        }
    }
}
