package by.epam.agency.manager;

/**
 * <p>Enum for project configuration</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum ConfigEnum {
    DB_SIZE("db.size"),
    DB_URL("db.url"),
    DB_DRIVER("db.driver"),
    DB_PASSWORD("db.password"),
    DB_USER("db.user"),
    LOG_TEST_PATH("log.test.path");

    String value;

    ConfigEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
