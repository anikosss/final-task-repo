package by.epam.agency.manager;

/**
 * <p>Enum that store keys for pages</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum PagesEnum {
    INDEX("page.index"),
    MAIN("page.main"),
    ADMIN("page.admin"),
    CATALOG("page.catalog"),
    TOUR_INFO("page.tour.info"),
    TOUR_INFO_FORMAT("page.tour.info.format"),
    SHOPPING_CART("page.shopping.cart"),
    CHECKOUT_ORDER("page.checkout.order"),
    PROFILE("page.profile"),
    ERROR_404("page.error.404"),
    GO_TO_MAIN("page.go.to.main"),
    GO_TO_ADD_TOUR("page.go.to.add.tour"),
    GO_TO_ADD_PLACE("page.go.to.add.place"),
    GO_TO_ADD_HOTEL("page.go.to.add.hotel"),
    GO_TO_SHOPPING_CART("page.go.to.shopping.cart"),
    GO_TO_ALL_TOURS("page.go.to.all.tours"),
    GO_TO_ALL_PLACES("page.go.to.all.places"),
    GO_TO_ALL_HOTELS("page.go.to.all.hotels"),
    GO_TO_ALL_ORDERS("page.go.to.all.orders"),
    GO_TO_PROFILE("page.go.to.profile"),
    COUNTRY_INFO("page.country.info"),
    ADMIN_ALL_TOURS("page.admin.all.tours"),
    ADMIN_ALL_PLACES("page.admin.all.places"),
    ADMIN_ALL_HOTELS("page.admin.all.hotels"),
    ADMIN_ALL_ORDERS("page.admin.all.orders"),
    ADMIN_ADD_PLACE("page.admin.add.place"),
    ADMIN_CHOOSE_TOUR_COUNTRY("page.admin.choose.tour.country"),
    ADMIN_CHOOSE_TOUR_PLACE("page.admin.choose.tour.place"),
    ADMIN_CHOOSE_HOTEL_COUNTRY("page.admin.choose.hotel.country"),
    ADMIN_CHOOSE_HOTEL_PLACE("page.admin.choose.hotel.place"),
    ADMIN_WRITE_TOUR_INFO("page.admin.write.tour.info"),
    ADMIN_WRITE_HOTEL_INFO("page.admin.write.hotel.info"),
    ADMIN_TOUR_DETAILS("page.admin.tour.details"),
    ADMIN_PLACE_DETAILS("page.admin.place.details"),
    ADMIN_HOTEL_DETAILS("page.admin.hotel.details"),
    ADMIN_CHANGE_TOUR("page.admin.change.tour"),
    ADMIN_CHANGE_PLACE("page.admin.change.place"),
    ADMIN_CHANGE_HOTEL("page.admin.change.hotel"),
    FORMAT_TOUR_DETAILS("page.format.tour.details"),
    FORMAT_PLACE_DETAILS("page.format.place.details"),
    FORMAT_HOTEL_DETAILS("page.format.hotel.details");

    String value;

    PagesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
