package by.epam.agency.manager;

/**
 * <p>Enum that store keys for all errors messages to the log-file</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum ErrorsEnum {
    PROPERTIES_LOAD_FAILED("error.properties.load.failed"),
    DB_CONFIG_LOAD_FAILED("error.db.config.load.failed"),
    DB_DRIVER_LOAD_FAILED("error.db.driver.load.failed"),
    DB_POOL_INIT("error.db.pool.init"),
    DB_POOL_GET_CONNECTION("error.db.pool.get.connection"),
    DB_POOL_DESTROY("error.db.pool.destroy"),
    DB_POOL_TIMEOUT("error.db.pool.timeout"),
    DAO_USER_CREATE("error.dao.user.create"),
    DAO_USER_READ("error.dao.user.read"),
    DAO_USER_READ_ALL("error.dao.user.read.all"),
    DAO_TOUR_CREATE("error.dao.tour.create"),
    DAO_TOUR_READ("error.dao.tour.read"),
    DAO_TOUR_READ_ALL("error.dao.tour.read.all"),
    DAO_TOUR_UPDATE("error.dao.tour.update"),
    DAO_TOUR_DELETE("error.dao.tour.delete"),
    DAO_OLD_TOURS_DELETE("error.dao.old.tours.delete"),
    DAO_PLACE_CREATE("error.dao.place.create"),
    DAO_PLACE_READ("error.dao.place.read"),
    DAO_PLACE_READ_ALL("error.dao.place.read.all"),
    DAO_PLACE_UPDATE("error.dao.place.update"),
    DAO_PLACE_DELETE("error.dao.place.delete"),
    DAO_ORDER_CREATE("error.dao.order.create"),
    DAO_ORDER_READ("error.dao.order.read"),
    DAO_ORDER_READ_ALL("error.dao.order.read.all"),
    DAO_ORDER_READ_BY_HOTEL("error.dao.order.read.by.hotel"),
    DAO_ORDER_UPDATE("error.dao.order.update"),
    DAO_ORDER_DELETE("error.dao.order.delete"),
    DAO_CART_CREATE("error.dao.cart.create"),
    DAO_CART_READ("error.dao.cart.read"),
    DAO_CART_READ_ALL("error.dao.cart.read.all"),
    DAO_CART_DELETE("error.dao.cart.delete"),
    DAO_COUNTRY_READ("error.dao.country.read"),
    DAO_COUNTRY_READ_ALL("error.dao.country.read.all"),
    DAO_HOTEL_CREATE("error.dao.hotel.create"),
    DAO_HOTEL_READ("error.dao.hotel.read"),
    DAO_HOTEL_READ_ALL("error.dao.hotel.read.all"),
    DAO_HOTEL_UPDATE("error.dao.hotel.update"),
    DAO_HOTEL_DELETE("error.dao.hotel.delete"),
    CONTEXT_EXTRACT_FAILED("error.context.extract.failed"),
    CONTEXT_INSERT_FAILED("error.context.insert.failed"),
    CONTEXT_EMPTY_ACTION("error.context.empty.action"),
    VALIDATE_EMPTY_FIELDS("error.validate.empty.fields"),
    VALIDATE_DIFFERENT_PASS("error.validate.different.pass"),
    VALIDATE_EXIST_LOGIN("error.validate.exist.login"),
    VALIDATE_EXIST_EMAIL("error.validate.exist.email"),
    VALIDATE_INVALID_VALUES("error.validate.invalid.values"),
    VALIDATE_CONTEXT("error.validate.context"),
    VALIDATE_WRONG_LOGIN("error.validate.wrong.login"),
    VALIDATE_WRONG_PASSWORD("error.validate.wrong.pass"),
    VALIDATE_SESSION_USER("error.validate.session.user"),
    REGISTRATION_FAILS("error.registration.fails");

    String value;

    ErrorsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
