package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class PlaceValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(PlaceValidator.class);
    private static final String PLACE = "place";

    private static class InstanceHolder {
        private static final PlaceValidator INSTANCE = new PlaceValidator();
    }

    private PlaceValidator() {}

    public static PlaceValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_PLACE;
        try {
            String[] placeParam = context.getRequestParameter(PLACE);
            if (placeParam == null || placeParam.length == 0 || placeParam[0] == null || placeParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_PLACE;
            int placeId = Integer.parseInt(placeParam[0]);
            Place place = PlaceService.getPlaceById(placeId);
            if (place == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
