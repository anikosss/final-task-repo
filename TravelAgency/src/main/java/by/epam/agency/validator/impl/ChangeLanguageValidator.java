package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeLanguageValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(ChangeLanguageValidator.class);
    private static final String LANG = "lang";
    private static final String REQUEST_PAGE = "requestPage";
    private static final String REGEX_LANG = "(ru_RU)|(en_EN)";

    private ChangeLanguageValidator() {}

    public static ChangeLanguageValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static class InstanceHolder {
        private static final ChangeLanguageValidator INSTANCE = new ChangeLanguageValidator();
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_LANGUAGE;
        try {
            String[] langParam = context.getRequestParameter(LANG);
            String[] pageParam = context.getRequestParameter(REQUEST_PAGE);
            if (!validateOnEmpty(langParam)) {
                throw new ValidateException();
            }
            String lang = langParam[0];
            Matcher matcher = Pattern.compile(REGEX_LANG).matcher(lang);
            if (!matcher.matches()) {
                throw new ValidateException();
            }
            if (!validateOnEmpty(pageParam)) {
                context.addRequestParameter(REQUEST_PAGE, new String[] { PagesManager.getPage(PagesEnum.GO_TO_MAIN) });
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String [] params) {
        return (params != null && params.length > 0 && params[0] != null && !params[0].isEmpty());
    }
}
