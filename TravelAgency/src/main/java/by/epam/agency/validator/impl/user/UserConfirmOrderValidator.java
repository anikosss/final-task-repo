package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.ShoppingCartValidator;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserConfirmOrderValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(UserConfirmOrderValidator.class);
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String USER = "user";
    private static final String CART = "cart";
    private static final String REGEX_NAME = "[a-zA-Zа-яА-Я\\s']{3,80}";
    private static final String REGEX_PHONE = "(\\+[\\d]{3})(\\([\\d]{2}\\))([\\d]{3})\\-([\\d]{2})\\-([\\d]{2})";
    private ShoppingCartValidator cartValidator = ShoppingCartValidator.getInstance();

    private static class InstanceHolder {
        private static final UserConfirmOrderValidator INSTANCE = new UserConfirmOrderValidator();
    }

    private UserConfirmOrderValidator() {}

    public static UserConfirmOrderValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            result = cartValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            if (!validateUserCart(context)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_CART;
                throw new ValidateException();
            }
            String[] nameParam = context.getRequestParameter(NAME);
            String[] phoneParam = context.getRequestParameter(PHONE);
            if (!validateOnEmpty(nameParam, phoneParam)) {
                throw new ValidateException();
            }
            String name = nameParam[0];
            String phone = phoneParam[0];
            Matcher nameMatcher = Pattern.compile(REGEX_NAME).matcher(name);
            Matcher phoneMatcher = Pattern.compile(REGEX_PHONE).matcher(phone);
            if (!nameMatcher.matches() || !phoneMatcher.matches()) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... params) {
        for (String[] param : params) {
            if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateUserCart(RequestContext context) {
        User user = (User) context.getSessionAttribute(USER);
        List<ShoppingCart> userCart = ShoppingCartService.getAllByUserId(user.getId());
        int cartId = Integer.parseInt(context.getRequestParameter(CART)[0]);
        ShoppingCart shoppingCart = ShoppingCartService.getShoppingCartById(cartId);
        if (userCart == null) {
            return false;
        }
        return userCart.contains(shoppingCart);
    }
}
