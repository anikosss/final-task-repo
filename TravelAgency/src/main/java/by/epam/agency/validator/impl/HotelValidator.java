package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class HotelValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(HotelValidator.class);
    private static final String HOTEL = "hotel";

    private static class InstanceHolder {
        private static final HotelValidator INSTANCE = new HotelValidator();
    }

    private HotelValidator() {}

    public static HotelValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_HOTEL;
        try {
            String[] hotelParam = context.getRequestParameter(HOTEL);
            if (hotelParam == null || hotelParam.length == 0 || hotelParam[0] == null || hotelParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL;
            int hotelId = Integer.parseInt(hotelParam[0]);
            Hotel hotel = HotelService.getHotelById(hotelId);
            if (hotel == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
