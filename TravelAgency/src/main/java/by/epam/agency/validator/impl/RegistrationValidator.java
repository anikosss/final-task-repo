package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.UserService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(RegistrationValidator.class);
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String REPEAT_PASSWORD = "repeatPassword";
    private static final String EMAIL = "email";
    private static final String REGEX_LOGIN = "^[a-zA-Z](.[\\w9_-]*){3,20}";
    private static final String REGEX_PASSWORD = "[\\w]{6,30}";
    private static final String REGEX_EMAIL = "[\\w]{1,20}(@){1}[\\w]{2,6}(\\.)[\\w]{2,4}";

    private static class InstanceHolder {
        private static final RegistrationValidator INSTANCE = new RegistrationValidator();
    }

    private RegistrationValidator() {}

    public static RegistrationValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] loginParam = context.getRequestParameter(LOGIN);
            String[] passwordParam = context.getRequestParameter(PASSWORD);
            String[] repeatPasswordParam = context.getRequestParameter(REPEAT_PASSWORD);
            String[] emailParam = context.getRequestParameter(EMAIL);
            if (!validateOnEmpty(loginParam, passwordParam, repeatPasswordParam, emailParam)) {
                throw new ValidateException();
            }
            String login = loginParam[0];
            String password = passwordParam[0];
            String repeatPassword = repeatPasswordParam[0];
            String email = emailParam[0];
            if (!validateOnRegex(login, password, email)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            if (!password.equals(repeatPassword)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD;
                throw new ValidateException();
            }
            User user = UserService.getUserByLogin(login);
            if (user != null) {
                result = MessagesEnum.ERROR_VALIDATE_EXIST_LOGIN;
                throw new ValidateException();
            }
            user = UserService.getUserByEmail(email);
            if (user != null) {
                result = MessagesEnum.ERROR_VALIDATE_EXIST_EMAIL;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String login, String password, String email) throws ValidateException {
        Matcher loginMatcher = Pattern.compile(REGEX_LOGIN).matcher(login);
        Matcher passwordMatcher = Pattern.compile(REGEX_PASSWORD).matcher(password);
        Matcher emailMatcher = Pattern.compile(REGEX_EMAIL).matcher(email);
        return (loginMatcher.matches() && passwordMatcher.matches() && emailMatcher.matches());
    }
}
