package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.OrderValidator;
import org.apache.log4j.Logger;

public class CancelOrderValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(CancelOrderValidator.class);
    private static final String ORDER = "order";
    private static final String CONFIRMED = "confirmed";
    private OrderValidator orderValidator = OrderValidator.getInstance();

    private static class InstanceHolder {
        private static final CancelOrderValidator INSTANCE = new CancelOrderValidator();
    }

    private CancelOrderValidator() {}

    public static CancelOrderValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_ORDER;
        try {
            result = orderValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            Order order = OrderService.getOrderById(orderId);
            if (order.getStatus().equals(CONFIRMED)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
