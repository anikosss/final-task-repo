package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class ShoppingCartValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(ShoppingCartValidator.class);
    private static final String CART = "cart";

    private static class InstanceHolder {
        private static final ShoppingCartValidator INSTANCE = new ShoppingCartValidator();
    }

    private ShoppingCartValidator() {}

    public static ShoppingCartValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_CART;
        try {
            String[] cartParam = context.getRequestParameter(CART);
            if (cartParam == null || cartParam.length == 0 || cartParam[0] == null || cartParam[0].isEmpty()) {
                throw new ValidateException();
            }
            int cartId = Integer.parseInt(cartParam[0]);
            ShoppingCart shoppingCart = ShoppingCartService.getShoppingCartById(cartId);
            if (shoppingCart == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
