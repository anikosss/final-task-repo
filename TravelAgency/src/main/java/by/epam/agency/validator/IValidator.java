package by.epam.agency.validator;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;

/**
 * <p>Interface that describes validate operations by validators</p>
 *
 * @author Dmitry Anikeichenko
 */
public interface IValidator {

    /**
     * <p>Method that validate request</p>
     *
     * @param context Context with information from the request
     * @return {@link MessagesEnum} that contains key of message to send it to user
     */
    MessagesEnum validate(RequestContext context);
}
