package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.IEntity;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ToursCatalogValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(ToursCatalogValidator.class);
    private static final String PAGE = "page";
    private static final String COUNTRY = "country";
    private static final String PLACE = "place";
    private static final String ACTION = "action";
    private static final String VIEW_CATALOG = "view_catalog";
    private static final String COUNTRY_CATALOG = "country_catalog";
    private static final String NEAREST_CATALOG = "nearest_catalog";
    private static final String LAST_CATALOG = "last_catalog";
    private static final String CHEAPEST_CATALOG = "cheapest_catalog";
    private static final String PLACE_CATALOG = "place_catalog";
    private static final String PAGES_COUNT = "pagesCount";
    private static final String REGEX_PAGE = "(?m)[1-9]{1}[\\d]{0,5}";
    private static final int VIEW_CATALOG_COUNT = 8;

    private static class InstanceHolder {
        private static final ToursCatalogValidator INSTANCE = new ToursCatalogValidator();
    }

    private ToursCatalogValidator() {}

    public static ToursCatalogValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
        try {
            String action = context.getRequestParameter(ACTION)[0];
            if (action.equals(VIEW_CATALOG)) {
                List<Tour> allTours = TourService.getAllTours();
                result = validate(context, allTours, VIEW_CATALOG_COUNT);
            }
            if (action.equals(COUNTRY_CATALOG) || action.equals(NEAREST_CATALOG) ||
                    action.equals(LAST_CATALOG) || action.equals(CHEAPEST_CATALOG)) {
                if (!validateCountry(context)) {
                    return MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY;
                }
                int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
                List<Tour> countryTours = TourService.getByCountryId(countryId);
                result = validate(context, countryTours, VIEW_CATALOG_COUNT);
            }
            if (action.equals(PLACE_CATALOG)) {
                if (!validatePlace(context)) {
                    return MessagesEnum.ERROR_VALIDATE_WRONG_PLACE;
                }
                int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
                List<Tour> placesTours = TourService.getByPlaceId(placeId);
                result = validate(context, placesTours, VIEW_CATALOG_COUNT);
            }
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private MessagesEnum validate(RequestContext context, List<Tour> list, int tourPerPage) throws ValidateException {
        int maxPage = calculateMaxPage(list, tourPerPage);
        context.addRequestAttribute(PAGES_COUNT, maxPage);
        String[] pageParam = context.getRequestParameter(PAGE);
        validatePageParam(context, pageParam);
        int page = Integer.parseInt(pageParam[0]);
        return validatePage(context, page, maxPage);
    }

    private boolean validateCountry(RequestContext context) {
        boolean result = false;
        try {
            String[] countryParam = context.getRequestParameter(COUNTRY);
            if (countryParam == null || countryParam.length == 0 || countryParam[0].isEmpty()) {
                throw new ValidateException();
            }
            int countryId = Integer.parseInt(countryParam[0]);
            Country country = CountryService.getCountryById(countryId);
            if (country == null) {
                throw new ValidateException();
            }
            result = true;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY));
        }
        return result;
    }

    private boolean validatePlace(RequestContext context) {
        boolean result = false;
        try {
            String[] placeParam = context.getRequestParameter(PLACE);
            if (placeParam == null || placeParam.length == 0 || placeParam[0].isEmpty()) {
                throw new ValidateException();
            }
            int placeId = Integer.parseInt(placeParam[0]);
            Place place = PlaceService.getPlaceById(placeId);
            if (place == null) {
                throw new ValidateException();
            }
            result = true;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(MessagesEnum.ERROR_VALIDATE_WRONG_PLACE));
        }
        return result;
    }

    private int calculateMaxPage(List<? extends IEntity> list, int countPerPage) {
        int listSize = list.size();
        int maxPage = listSize / countPerPage;
        if (listSize % countPerPage != 0) {
            maxPage++;
        }
        return maxPage;
    }

    private void validatePageParam(RequestContext context, String[] param) throws ValidateException {
        if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            throw new ValidateException();
        }
        Matcher pageMatcher = Pattern.compile(REGEX_PAGE).matcher(param[0]);
        if (!pageMatcher.matches()) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            throw new ValidateException();
        }
    }

    private MessagesEnum validatePage(RequestContext context, int page, int maxPage) throws ValidateException {
        MessagesEnum result = MessagesEnum.SUCCESS;
        if (page > maxPage) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            if (maxPage != 0) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
            }
        }
        return result;
    }

}
