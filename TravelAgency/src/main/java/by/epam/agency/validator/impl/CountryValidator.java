package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class CountryValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(CountryValidator.class);
    private static final String COUNTRY = "country";

    private static class InstanceHolder {
        private static final CountryValidator INSTANCE = new CountryValidator();
    }

    private CountryValidator() {}

    public static CountryValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_COUNTRY;
        try {
            String[] countryParam = context.getRequestParameter(COUNTRY);
            if (countryParam == null || countryParam.length == 0 ||
                    countryParam[0] == null || countryParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY;
            int countryId = Integer.parseInt(countryParam[0]);
            Country country = CountryService.getCountryById(countryId);
            if (country == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
