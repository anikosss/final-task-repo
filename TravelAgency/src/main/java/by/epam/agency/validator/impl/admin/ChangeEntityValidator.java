package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.HotelValidator;
import by.epam.agency.validator.impl.PlaceValidator;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

public class ChangeEntityValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(ChangeEntityValidator.class);
    private static final String CHANGE_HOTEL = "change_hotel";
    private static final String CHANGE_PLACE = "change_place";
    private static final String CHANGE_TOUR = "change_tour";
    private static final String ACTION = "action";

    private static class InstanceHolder {
        private static final ChangeEntityValidator INSTANCE = new ChangeEntityValidator();
    }

    private ChangeEntityValidator() {}

    public static ChangeEntityValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = null;
        String action = context.getRequestParameter(ACTION)[0];
        switch (action) {
            case CHANGE_HOTEL:
                result = validateChangeHotel(context);
                break;
            case CHANGE_PLACE:
                result = validateChangePlace(context);
                break;
            case CHANGE_TOUR:
                result = validateChangeTour(context);
                break;
        }
        return result;
    }

    private MessagesEnum validateChangeHotel(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL;
        try {
            HotelValidator hotelValidator = HotelValidator.getInstance();
            result = hotelValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            HotelInfoValidator infoValidator = HotelInfoValidator.getInstance();
            result = infoValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private MessagesEnum validateChangePlace(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_PLACE;
        try {
            PlaceValidator placeValidator = PlaceValidator.getInstance();
            result = placeValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            PlaceInfoValidator infoValidator = PlaceInfoValidator.getInstance();
            result = infoValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private MessagesEnum validateChangeTour(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_TOUR;
        try {
            TourValidator tourValidator = TourValidator.getInstance();
            result = tourValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            TourInfoValidator infoValidator = TourInfoValidator.getInstance();
            result = infoValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
