package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.HotelValidator;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class AddToCartValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(AddToCartValidator.class);
    private static final String USER = "user";
    private static final String TOUR = "tour";
    private static final String HOTEL = "hotel";
    private HotelValidator hotelValidator = HotelValidator.getInstance();
    private TourValidator tourValidator = TourValidator.getInstance();

    private static class InstanceHolder {
        private static final AddToCartValidator INSTANCE = new AddToCartValidator();
    }

    private AddToCartValidator() {}

    public static AddToCartValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_TOUR_OR_HOTEL;
        try {
            result = tourValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = hotelValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            int hotelId = Integer.parseInt(context.getRequestParameter(HOTEL)[0]);
            Tour tour = TourService.getTourById(tourId);
            if (!validateHotelInTour(tour, hotelId)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_HOTEL;
                throw new ValidateException();
            }
            if (!validateTourInCart(context, tourId, hotelId)) {
                result = MessagesEnum.ERROR_VALIDATE_EXIST_CART;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateHotelInTour(Tour tour, int hotelId) {
        Hotel hotel = HotelService.getHotelById(hotelId);
        return tour.getPlace().getHotels().contains(hotel);
    }

    private boolean validateTourInCart(RequestContext context, int tourId, int hotelId) {
        User user = (User) context.getSessionAttribute(USER);
        List<ShoppingCart> list = ShoppingCartService.getByUserAndTour(user.getId(), tourId);
        if (list == null || list.size() == 0) {
            return true;
        }
        for (ShoppingCart shoppingCart : list) {
            if (shoppingCart.getHotel().getId() == hotelId) {
                return false;
            }
        }
        return true;
    }
}
