package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TourInfoValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(TourInfoValidator.class);
    private static final String ARRIVE_DATE = "arriveDate";
    private static final String ARRIVE_TIME = "arriveTime";
    private static final String RETURN_DATE = "returnDate";
    private static final String RETURN_TIME = "returnTime";
    private static final String PRICE = "price";
    private static final String SEATS_CLASS = "seatsClass";
    private static final String REGEX_DATE = "[\\d]{4}-[\\d]{2}-[\\d]{2}";
    private static final String REGEX_TIME = "([\\d]{2}:[\\d]{2})|([\\d]{2}:[\\d]{2}:[\\d]{2})";
    private static final String REGEX_PRICE = "[\\d]{1,7}[\\.\\d]{0,4}";
    private static final String REGEX_SEATS_CLASS = "(econom)|(business)";

    private static class InstanceHolder {
        private static final TourInfoValidator INSTANCE = new TourInfoValidator();
    }

    private TourInfoValidator() {}

    public static TourInfoValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] arriveDateParam = context.getRequestParameter(ARRIVE_DATE);
            String[] arriveTimeParam = context.getRequestParameter(ARRIVE_TIME);
            String[] returnDateParam = context.getRequestParameter(RETURN_DATE);
            String[] returnTimeParam = context.getRequestParameter(RETURN_TIME);
            String[] priceParam = context.getRequestParameter(PRICE);
            String[] seatsClassParam = context.getRequestParameter(SEATS_CLASS);
            if (!validateOnEmpty(arriveDateParam, arriveTimeParam, returnDateParam, returnTimeParam,
                    priceParam, seatsClassParam)) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
            if (!validateOnRegex(arriveDateParam[0], arriveTimeParam[0], returnDateParam[0], returnTimeParam[0],
                    priceParam[0], seatsClassParam[0])) {
                throw new ValidateException();
            }
            double price = Double.parseDouble(priceParam[0]);
            if (price <= 0) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_DATE_FORMAT;
            if (!validateDate(arriveDateParam[0], returnDateParam[0])) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... params) {
        for (String[] param : params) {
            if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String arriveDate, String arriveTime, String returnDate, String returnTime,
                                    String price, String seatsClass) {
        Matcher arriveDateMatcher = Pattern.compile(REGEX_DATE).matcher(arriveDate);
        Matcher arriveTimeMatcher = Pattern.compile(REGEX_TIME).matcher(arriveTime);
        Matcher returnDateMatcher = Pattern.compile(REGEX_DATE).matcher(returnDate);
        Matcher returnTimeMatcher = Pattern.compile(REGEX_TIME).matcher(returnTime);
        Matcher priceMatcher = Pattern.compile(REGEX_PRICE).matcher(price);
        Matcher seatsClassMatcher = Pattern.compile(REGEX_SEATS_CLASS).matcher(seatsClass);
        return (arriveDateMatcher.matches() && arriveTimeMatcher.matches() && returnDateMatcher.matches() &&
                returnTimeMatcher.matches() && priceMatcher.matches() && seatsClassMatcher.matches());
    }

    private boolean validateDate(String arriveDateParam, String returnDateParam) throws IllegalArgumentException {
        Date arriveDate = Date.valueOf(arriveDateParam);
        Date returnDate = Date.valueOf(returnDateParam);
        Date todayDate = new Date(System.currentTimeMillis());
        return (!(arriveDate.after(returnDate)) && !(arriveDate.getTime() == returnDate.getTime())
                && !(arriveDate.before(todayDate)) && !returnDate.before(todayDate)
                && !(returnDate.getTime() == todayDate.getTime()));
    }
}
