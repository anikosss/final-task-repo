package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.PlaceValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class ChoosePlaceValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(ChoosePlaceValidator.class);
    private static final String COUNTRY = "country";
    private static final String PLACE = "place";
    private static final String PLACES = "places";
    private PlaceValidator placeValidator = PlaceValidator.getInstance();

    private static class InstanceHolder {
        private static final ChoosePlaceValidator INSTANCE = new ChoosePlaceValidator();
    }

    private ChoosePlaceValidator() {}

    public static ChoosePlaceValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_PLACE;
        try {
            result = placeValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY;
            if (!validatePlaceInCountry(context)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        if (!result.equals(MessagesEnum.SUCCESS)) {
            List<Place> places = PlaceService.getAllPlaces();
            context.addRequestAttribute(PLACES, places);
        }
        return result;
    }

    private boolean validatePlaceInCountry(RequestContext context) {
        int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
        int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
        Place place = PlaceService.getPlaceById(placeId);
        return place.getCountry().getId() == countryId;
    }
}
