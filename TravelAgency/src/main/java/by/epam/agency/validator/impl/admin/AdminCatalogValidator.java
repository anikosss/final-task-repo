package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.IEntity;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.service.OrderService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminCatalogValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(AdminCatalogValidator.class);
    private static final String PAGE = "page";
    private static final String ACTION = "action";
    private static final String ALL_TOURS = "all_tours";
    private static final String ALL_PLACES = "all_places";
    private static final String ALL_HOTELS = "all_hotels";
    private static final String ALL_ORDERS = "all_orders";
    private static final String PAGES_COUNT = "pagesCount";
    private static final String REGEX_PAGE = "(?m)[1-9]{1}[\\d]{0,5}";
    private static final int COUNT_PER_PAGE = 20;

    private static class InstanceHolder {
        private static final AdminCatalogValidator INSTANCE = new AdminCatalogValidator();
    }

    private AdminCatalogValidator() {}

    public static AdminCatalogValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
        try {
            String action = context.getRequestParameter(ACTION)[0];
            List<Tour> allTours;
            switch (action) {
                case ALL_TOURS:
                    allTours = TourService.getAllTours();
                    result = validate(context, allTours, COUNT_PER_PAGE);
                    break;
                case ALL_PLACES:
                    List<Place> allPlaces = PlaceService.getAllPlaces();
                    result = validate(context, allPlaces, COUNT_PER_PAGE);
                    break;
                case ALL_HOTELS:
                    List<Hotel> allHotels = HotelService.getAllHotels();
                    result = validate(context, allHotels, COUNT_PER_PAGE);
                    break;
                case ALL_ORDERS:
                    List<Order> allOrders = OrderService.getAllOrders();
                    result = validate(context, allOrders, COUNT_PER_PAGE);
                    break;
            }
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private MessagesEnum validate(RequestContext context, List<? extends IEntity> list, int countPerPage)
            throws ValidateException {
        int maxPage = calculateMaxPage(list, countPerPage);
        context.addRequestAttribute(PAGES_COUNT, maxPage);
        String[] pageParam = context.getRequestParameter(PAGE);
        validatePageParam(context, pageParam);
        int page = Integer.parseInt(pageParam[0]);
        return validatePage(context, page, maxPage);
    }

    private int calculateMaxPage(List<? extends IEntity> list, int countPerPage) {
        int listSize = list.size();
        int maxPage = listSize / countPerPage;
        if (listSize % countPerPage != 0) {
            maxPage++;
        }
        return maxPage;
    }

    private void validatePageParam(RequestContext context, String[] param) throws ValidateException {
        if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            throw new ValidateException();
        }
        Matcher pageMatcher = Pattern.compile(REGEX_PAGE).matcher(param[0]);
        if (!pageMatcher.matches()) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            throw new ValidateException();
        }
    }

    private MessagesEnum validatePage(RequestContext context, int page, int maxPage) throws ValidateException {
        MessagesEnum result = MessagesEnum.SUCCESS;
        if (page > maxPage) {
            context.addRequestParameter(PAGE, new String[] { "1" });
            if (maxPage != 0) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
            }
        }
        return result;
    }
}
