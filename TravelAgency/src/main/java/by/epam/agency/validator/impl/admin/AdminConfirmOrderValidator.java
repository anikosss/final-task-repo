package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.OrderValidator;
import org.apache.log4j.Logger;

public class AdminConfirmOrderValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(AdminConfirmOrderValidator.class);
    private static final String ORDER = "order";
    private static final String NEW = "new";
    private OrderValidator orderValidator = OrderValidator.getInstance();

    private static class InstanceHolder {
        private static final AdminConfirmOrderValidator INSTANCE = new AdminConfirmOrderValidator();
    }

    private AdminConfirmOrderValidator() {}

    public static AdminConfirmOrderValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_ORDER;
        try {
            result = orderValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            Order order = OrderService.getOrderById(orderId);
            if (!order.getStatus().equals(NEW)) {
                result = MessagesEnum.ERROR_VALIDATE_ADMIN_CONFIRM;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch ( ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
