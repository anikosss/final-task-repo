package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.UserService;
import by.epam.agency.util.Encoder;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(LoginValidator.class);
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String REGEX_LOGIN = "^[a-zA-Z](.[\\w9_-]*){3,20}";
    private static final String REGEX_PASSWORD = "[\\w]{6,30}";

    private static class InstanceHolder {
        private static final LoginValidator INSTANCE = new LoginValidator();
    }

    private LoginValidator() {}

    public static LoginValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] loginParam = context.getRequestParameter(LOGIN);
            String[] passwordParam = context.getRequestParameter(PASSWORD);
            if (!validateOnEmpty(loginParam) || !validateOnEmpty(passwordParam)) {
                result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
                throw new ValidateException();
            }
            String login = loginParam[0];
            String password = passwordParam[0];
            if (!validateOnRegex(login, password)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            User user = UserService.getUserByLogin(login);
            if (user == null) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_LOGIN;
                throw new ValidateException();
            }
            if (!Encoder.decode(user.getPassword()).equals(password)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[] param) {
        return (param != null && param.length > 0 && param[0] != null && !param[0].isEmpty());
    }

    private boolean validateOnRegex(String login, String password) {
        Matcher loginMatcher = Pattern.compile(REGEX_LOGIN).matcher(login);
        Matcher passwordMatcher = Pattern.compile(REGEX_PASSWORD).matcher(password);
        return (loginMatcher.matches() && passwordMatcher.matches());
    }

}
