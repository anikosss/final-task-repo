package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HotelInfoValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(HotelInfoValidator.class);
    private static final String DEFAULT_IMAGE = "hotel-image-not-found.jpg";
    private static final String TITLE = "title";
    private static final String STARS = "stars";
    private static final String ALL_ROOMS = "allRooms";
    private static final String MAIN_IMAGE = "mainImage";
    private static final String ROOM_IMAGE = "roomImage";
    private static final String REGEX_TITLE = "[а-яА-Я\\w\\s'\\-\\.]{1,}";
    private static final String REGEX_STARS = "[1234567]{1}";
    private static final String REGEX_ALL_ROOMS = "(?m)^[1-9]{1}[\\d]{0,}";
    private static final String REGEX_IMAGE = "((hotel)|(room))-[\\w\\-_'\\.]{1,80}";

    private static class InstanceHolder {
        private static final HotelInfoValidator INSTANCE = new HotelInfoValidator();
    }

    private HotelInfoValidator() {}

    public static HotelInfoValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] titleParam = context.getRequestParameter(TITLE);
            String[] starsParam = context.getRequestParameter(STARS);
            String[] allRoomsParam = context.getRequestParameter(ALL_ROOMS);
            String[] mainImageParam = context.getRequestParameter(MAIN_IMAGE);
            String[] roomImageParam = context.getRequestParameter(ROOM_IMAGE);
            if (!validateOnEmpty(titleParam, starsParam, allRoomsParam)) {
                throw new ValidateException();
            }
            String mainImage = processImage(mainImageParam);
            String roomImage = processImage(roomImageParam);
            context.addRequestParameter(MAIN_IMAGE, new String[]{ mainImage });
            context.addRequestParameter(ROOM_IMAGE, new String[] { roomImage });
            result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
            if (!validateOnRegex(titleParam[0], starsParam[0], allRoomsParam[0], mainImage, roomImage)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private String processImage(String[] imageParam) {
        if (imageParam == null || imageParam.length == 0 || imageParam[0] == null || imageParam[0].isEmpty()) {
            return DEFAULT_IMAGE;
        }
        return imageParam[0];
    }

    private boolean validateOnEmpty(String[]... params) {
        for (String[] param : params) {
            if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String title, String stars, String allRooms, String mainImage, String roomImage) {
        Matcher titleMatcher = Pattern.compile(REGEX_TITLE).matcher(title);
        Matcher starsMatcher = Pattern.compile(REGEX_STARS).matcher(stars);
        Matcher allRoomsMatcher = Pattern.compile(REGEX_ALL_ROOMS).matcher(allRooms);
        Matcher mainImageMatcher = Pattern.compile(REGEX_IMAGE).matcher(mainImage);
        Matcher roomImageMatcher = Pattern.compile(REGEX_IMAGE).matcher(roomImage);
        return (titleMatcher.matches() && starsMatcher.matches() && allRoomsMatcher.matches() &&
                mainImageMatcher.matches() && roomImageMatcher.matches());
    }
}
