package by.epam.agency.validator.impl.user;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.ShoppingCartValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class CheckoutOrderValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(CheckoutOrderValidator.class);
    private static final String CART = "cart";
    private static final String USER = "user";
    private ShoppingCartValidator cartValidator = ShoppingCartValidator.getInstance();

    private static class InstanceHolder {
        private static final CheckoutOrderValidator INSTANCE = new CheckoutOrderValidator();
    }

    private CheckoutOrderValidator() {}

    public static CheckoutOrderValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_CART;
        try {
            result = cartValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            int cartId = Integer.parseInt(context.getRequestParameter(CART)[0]);
            ShoppingCart shoppingCart = ShoppingCartService.getShoppingCartById(cartId);
            User user = (User) context.getSessionAttribute(USER);
            List<ShoppingCart> userOrders = ShoppingCartService.getAllByUserId(user.getId());
            if (!userOrders.contains(shoppingCart)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_CART;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
