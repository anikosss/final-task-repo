package by.epam.agency.validator.impl;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class TourValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(TourValidator.class);
    private static final String TOUR = "tour";

    private static class InstanceHolder {
        private static final TourValidator INSTANCE = new TourValidator();
    }

    private TourValidator() {}

    public static TourValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR;
        try {
            String[] tourParam = context.getRequestParameter(TOUR);
            if (tourParam == null || tourParam.length == 0 || tourParam[0] == null || tourParam[0].isEmpty()) {
                throw new ValidateException(MessagesManager.getMessage(result));
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_TOUR;
            int tourId = Integer.parseInt(tourParam[0]);
            Tour tour = TourService.getTourById(tourId);
            if (tour == null) {
                throw new ValidateException(MessagesManager.getMessage(result));
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
