package by.epam.agency.validator.exception;

public class ValidateException extends Exception {

    public ValidateException() {
        super();
    }

    public ValidateException(String message) {
        super(message);
    }
}
