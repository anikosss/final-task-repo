package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

public class MakeTourHotValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(MakeTourHotValidator.class);
    private static final String SALE = "sale";
    private TourValidator tourValidator = TourValidator.getInstance();

    private static class InstanceHolder {
        private static final MakeTourHotValidator INSTANCE = new MakeTourHotValidator();
    }

    private MakeTourHotValidator() {}

    public static MakeTourHotValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR;
        try {
            result = tourValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            String[] saleParam = context.getRequestParameter(SALE);
            result = MessagesEnum.ERROR_VALIDATE_EMPTY_SALE;
            if (saleParam == null || saleParam.length == 0 || saleParam[0] == null || saleParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_SALE;
            int sale = Integer.parseInt(saleParam[0]);
            if (sale < 1 || sale > 99) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
