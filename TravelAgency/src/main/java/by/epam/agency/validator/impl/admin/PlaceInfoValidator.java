package by.epam.agency.validator.impl.admin;

import by.epam.agency.controller.RequestContext;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.validator.IValidator;
import by.epam.agency.validator.exception.ValidateException;
import by.epam.agency.validator.impl.CountryValidator;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaceInfoValidator implements IValidator {
    private static final Logger LOGGER = Logger.getLogger(PlaceInfoValidator.class);
    private static final String DEFAULT_IMAGE = "place-image-not-found.jpg";
    private static final String TITLE = "title";
    private static final String IMAGE = "image";
    private static final String REGEX_TITLE = "[а-яА-Я\\w\\s'\\-]{3,50}";
    private static final String REGEX_IMAGE = "(place)-[\\w\\-_'\\.]{1,80}";
    private CountryValidator countryValidator = CountryValidator.getInstance();

    private static class InstanceHolder {
        private static final PlaceInfoValidator INSTANCE = new PlaceInfoValidator();
    }

    private PlaceInfoValidator() {}

    public static PlaceInfoValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_COUNTRY;
        try {
            result = countryValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
            String[] titleParam = context.getRequestParameter(TITLE);
            String[] imageParam = context.getRequestParameter(IMAGE);
            if (!validateOnEmpty(titleParam)) {
                throw new ValidateException();
            }
            String image;
            if (imageParam == null || imageParam.length == 0 || imageParam[0] == null || imageParam[0].isEmpty()) {
                image = DEFAULT_IMAGE;
                context.addRequestParameter(IMAGE, new String[] { image });
            } else {
                image = imageParam[0];
            }
            String title = titleParam[0];
            result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
            if (!validateOnRegex(title, image)) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... params) {
        for (String[] param : params) {
            if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String title, String image) {
        Matcher titleMatcher = Pattern.compile(REGEX_TITLE).matcher(title);
        Matcher imageMatcher = Pattern.compile(REGEX_IMAGE).matcher(image);
        return (titleMatcher.matches() && imageMatcher.matches());
    }
}
