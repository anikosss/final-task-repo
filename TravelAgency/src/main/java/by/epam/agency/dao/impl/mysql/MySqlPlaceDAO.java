package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Place} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlPlaceDAO implements IBaseDAO<Place> {
    private static final Logger LOGGER = Logger.getLogger(MySqlPlaceDAO.class);
    private static final String SQL_CREATE = "INSERT INTO places(title, image, country_id) VALUES (?,?,?)";
    private static final String SQL_READ = "SELECT * FROM places WHERE place_id = ? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM places";
    private static final String SQL_UPDATE = "UPDATE places SET title = ?, image = ?, country_id = ? WHERE place_id = ?";
    private static final String SQL_DELETE = "DELETE FROM places WHERE place_id = ?";
    private static final String SQL_READ_BY_COUNTRY_ID = "SELECT * FROM places WHERE country_id = ?";
    private static final String ROW_PLACE_ID = "places.place_id";
    private static final String ROW_TITLE = "places.title";
    private static final String ROW_IMAGE = "places.image";
    private static final String ROW_COUNTRY_ID = "places.country_id";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlPlaceDAO INSTANCE = new MySqlPlaceDAO();
    }

    private MySqlPlaceDAO() {}

    public static MySqlPlaceDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Place entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getImage());
            statement.setInt(3, entity.getCountry().getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Place read(int key) {
        Place result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createPlace(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Place> readAll() {
        List<Place> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Place place = createPlace(resultSet);
                result.add(place);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Place entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getImage());
            statement.setInt(3, entity.getCountry().getId());
            statement.setInt(4, entity.getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Place> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Place> execute(QueriesEnum query, int param) {
        List<Place> result = null;
        switch (query) {
            case PLACE_BY_COUNTRY_ID:
                result = readByCountryId(param);
                break;
        }
        return result;
    }

    /**
     * <p>Method for reading Place entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Place} from resultSet
     */
    private Place createPlace(ResultSet resultSet) {
        Place place = null;
        try {
            int placeId = resultSet.getInt(ROW_PLACE_ID);
            String title = resultSet.getString(ROW_TITLE);
            String image = resultSet.getString(ROW_IMAGE);
            Country country = MySqlCountryDAO.getInstance().read(resultSet.getInt(ROW_COUNTRY_ID));
            List<Hotel> hotels = MySqlHotelDAO.getInstance().readByPlaceId(placeId);
            place = new Place(placeId, title, image, country, hotels);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return place;
    }

    private List<Place> readByCountryId(int countryId) {
        List<Place> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_COUNTRY_ID)) {
            statement.setInt(1, countryId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Place place = createPlace(resultSet);
                result.add(place);
            }
            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
