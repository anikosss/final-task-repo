package by.epam.agency.dao;

import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.dao.factory.impl.mysql.MySqlDAOFactory;

/**
 * <p>Enum for choosing which database will be used in the project</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum DAOEnum {
    MYSQL(new MySqlDAOFactory());

    IDAOFactory factory;

    DAOEnum(IDAOFactory factory) {
        this.factory = factory;
    }

    public IDAOFactory getFactory() {
        return this.factory;
    }
}
