package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.*;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Order} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlOrderDAO implements IBaseDAO<Order> {
    private static final Logger LOGGER = Logger.getLogger(MySqlOrderDAO.class);
    private static final String SQL_CREATE = "INSERT INTO orders (name, phone, price, tour_status, tour_id, user_id, " +
            "hotel_id) VALUES (?,?,?,?,?,?,?)";
    private static final String SQL_READ = "SELECT * FROM orders WHERE order_id = ? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM orders ORDER BY status DESC";
    private static final String SQL_UPDATE = "UPDATE orders SET name = ?, phone = ?, price = ?, tour_status = ?, " +
            "order_date = ?, tour_id = ?, user_id = ?, hotel_id = ?, status = ? WHERE order_id = ?";
    private static final String SQL_DELETE = "DELETE FROM orders WHERE order_id = ?";
    private static final String SQL_READ_BY_HOTEL_ID = "SELECT * FROM orders WHERE hotel_id = ?";
    private static final String SQL_READ_BY_STATUS = "SELECT * FROM orders WHERE user_id = ? and status = ?";
    private static final String STATUS_NEW = "new";
    private static final String STATUS_CONFIRMED = "confirmed";
    private static final String ROW_ORDER_ID = "orders.order_id";
    private static final String ROW_NAME = "orders.name";
    private static final String ROW_PHONE = "orders.phone";
    private static final String ROW_PRICE = "orders.price";
    private static final String ROW_TOUR_STATUS = "orders.tour_status";
    private static final String ROW_ORDER_DATE = "orders.order_date";
    private static final String ROW_TOUR_ID = "orders.tour_id";
    private static final String ROW_USER_ID = "orders.user_id";
    private static final String ROW_HOTEL_ID = "orders.hotel_id";
    private static final String ROW_STATUS = "orders.status";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlOrderDAO INSTANCE = new MySqlOrderDAO();
    }

    private MySqlOrderDAO() {}

    public static MySqlOrderDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Order entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getPhone());
            statement.setDouble(3, entity.getTour().getPrice());
            statement.setString(4, entity.getTour().getStatus());
            statement.setInt(5, entity.getTour().getId());
            statement.setInt(6, entity.getUser().getId());
            statement.setInt(7, entity.getHotel().getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Order read(int key) {
        Order result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createOrder(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Order> readAll() {
        List<Order> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Order entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getPhone());
            statement.setDouble(3, entity.getPrice());
            statement.setString(4, entity.getTourStatus());
            statement.setDate(5, entity.getOrderDate());
            statement.setInt(6, entity.getTour().getId());
            statement.setInt(7, entity.getUser().getId());
            statement.setInt(8, entity.getHotel().getId());
            statement.setString(9, entity.getStatus());
            statement.setInt(10, entity.getId());
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Order> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Order> execute(QueriesEnum query, int param) {
        List<Order> result = null;
        switch (query) {
            case ORDER_BY_HOTEL_ID:
                result = readByHotelId(param);
                break;
            case ORDER_NOT_CONFIRMED:
                result = getOrdersByStatus(param, STATUS_NEW);
                break;
            case ORDER_CONFIRMED:
                result = getOrdersByStatus(param, STATUS_CONFIRMED);
                break;
        }
        return result;
    }

    /**
     * <p>Method for reading Order entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Order} from resultSet
     */
    private Order createOrder(ResultSet resultSet) {
        Order result = null;
        try {
            int orderId = resultSet.getInt(ROW_ORDER_ID);
            String name = resultSet.getString(ROW_NAME);
            String phone = resultSet.getString(ROW_PHONE);
            double price = resultSet.getDouble(ROW_PRICE);
            String tourStatus = resultSet.getString(ROW_TOUR_STATUS);
            Date orderDate = resultSet.getDate(ROW_ORDER_DATE);
            int tourId = resultSet.getInt(ROW_TOUR_ID);
            int userId = resultSet.getInt(ROW_USER_ID);
            int hotelId = resultSet.getInt(ROW_HOTEL_ID);
            String status =resultSet.getString(ROW_STATUS);
            Tour tour = MySqlTourDAO.getInstance().read(tourId);
            User user = MySqlUserDAO.getInstance().read(userId);
            Hotel hotel = MySqlHotelDAO.getInstance().read(hotelId);
            result = new Order(orderId, name, phone, price, tourStatus, orderDate, tour, user, hotel, status);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Order> readByHotelId(int key) {
        List<Order> result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_HOTEL_ID)) {
            statement.setInt(1, key);
            result = new ArrayList<>();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> getOrdersByStatus(int userId, String status) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_STATUS)) {
            statement.setInt(1, userId);
            statement.setString(2, status);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

}
