package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Country} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlCountryDAO implements IBaseDAO<Country> {
    private static final String SQL_CREATE = "INSERT INTO countries (name, description, image) VALUES (?,?,?)";
    private static final String SQL_READ = "SELECT * FROM countries WHERE country_id = ? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM countries";
    private static final String SQL_UPDATE = "UPDATE countries SET name = ?, description = ?, image = ? " +
            "WHERE country_id = ?";
    private static final String SQL_DELETE = "DELETE FROM countries WHERE country_id = ?";
    private static final String ROW_COUNTRY_ID = "countries.country_id";
    private static final String ROW_NAME = "countries.name";
    private static final String ROW_DESCRIPTION = "countries.description";
    private static final String ROW_IMAGE = "countries.image";
    private static final Logger LOGGER = Logger.getLogger(MySqlCountryDAO.class);
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlCountryDAO INSTANCE = new MySqlCountryDAO();
    }

    private MySqlCountryDAO() {}

    public static MySqlCountryDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }


    @Override
    public boolean create(Country entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setString(3, entity.getImage());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Country read(int key) {
        Country result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createCountry(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Country> readAll() {
        List<Country> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Country country = createCountry(resultSet);
                result.add(country);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Country entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setString(3, entity.getImage());
            statement.setInt(4, entity.getId());
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Country> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Country> execute(QueriesEnum query, int param) {
        return null;
    }

    /**
     * <p>Method for reading Country entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Country} from resultSet
     */
    private Country createCountry(ResultSet resultSet) {
        Country result = null;
        try {
            int countryId = resultSet.getInt(ROW_COUNTRY_ID);
            String name = resultSet.getString(ROW_NAME);
            String description = resultSet.getString(ROW_DESCRIPTION);
            String image = resultSet.getString(ROW_IMAGE);
            result = new Country(countryId, name, description, image);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
