package by.epam.agency.dao.factory;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.entity.impl.*;

/**
 * <p>Interface for DAO factory for all entities</p>
 *
 * @author Dmitry Anikeichenko
 */
public interface IDAOFactory {
    IBaseDAO<User> getUserDAO();
    IBaseDAO<Tour> getTourDAO();
    IBaseDAO<Place> getPlaceDAO();
    IBaseDAO<Hotel> getHotelDAO();
    IBaseDAO<Country> getCountryDAO();
    IBaseDAO<Order> getOrderDAO();
    IBaseDAO<ShoppingCart> getShoppingCartDAO();
}
