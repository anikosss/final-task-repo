package by.epam.agency.dao.exception;

/**
 * <p>Exception throws when error occurred while working with database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class DAOException extends Exception {

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }
}
