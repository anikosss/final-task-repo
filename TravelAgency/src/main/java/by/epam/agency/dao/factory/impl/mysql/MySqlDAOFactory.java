package by.epam.agency.dao.factory.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.dao.impl.mysql.*;
import by.epam.agency.entity.impl.*;

/**
 * <p>Main factory that defines right DAOFactory based on type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlDAOFactory implements IDAOFactory {

    @Override
    public IBaseDAO<User> getUserDAO() {
        return MySqlUserDAO.getInstance();
    }

    @Override
    public IBaseDAO<Tour> getTourDAO() {
        return MySqlTourDAO.getInstance();
    }

    @Override
    public IBaseDAO<Place> getPlaceDAO() {
        return MySqlPlaceDAO.getInstance();
    }

    @Override
    public IBaseDAO<Hotel> getHotelDAO() {
        return MySqlHotelDAO.getInstance();
    }

    @Override
    public IBaseDAO<Country> getCountryDAO() {
        return MySqlCountryDAO.getInstance();
    }

    @Override
    public IBaseDAO<Order> getOrderDAO() {
        return MySqlOrderDAO.getInstance();
    }

    @Override
    public IBaseDAO<ShoppingCart> getShoppingCartDAO() {
        return MySqlShoppingCartDAO.getInstance();
    }
}
