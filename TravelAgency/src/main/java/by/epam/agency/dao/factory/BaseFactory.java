package by.epam.agency.dao.factory;

import by.epam.agency.dao.DAOEnum;

/**
 * <p>Main factory that defines right DAOFactory based on type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class BaseFactory {

    /**
     * <p>Method for defining {@link IDAOFactory}</p>
     *
     * @param key type of the database
     * @return {@link IDAOFactory} for the used database
     */
    public static IDAOFactory defineFactory(DAOEnum key) {
        return key.getFactory();
    }
}
