package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.util.SeatsClassesEnum;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Tour} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlTourDAO implements IBaseDAO<Tour> {
    private static final Logger LOGGER = Logger.getLogger(MySqlTourDAO.class);
    private static final String SQL_CREATE = "INSERT INTO tours (arrive_date, arrive_time, return_date, return_time, " +
            "price, default_price, seats_class, place_id) VALUES (?,?,?,?,?,?,?,?)";
    private static final String SQL_READ = "SELECT * FROM tours INNER JOIN places ON " +
            "tours.place_id = places.place_id WHERE tours.tour_id=? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM tours";
    private static final String SQL_UPDATE = "UPDATE tours SET arrive_date = ?, arrive_time = ?, return_date = ?, " +
            "return_time = ?, price = ?, default_price = ?, seats_class = ?, status = ?, sale = ? WHERE tour_id = ?";
    private static final String SQL_DELETE = "DELETE FROM tours WHERE tour_id = ?";
    private static final String SQL_READ_BY_COUNTRY_ID = "SELECT * FROM tours INNER JOIN places ON " +
            "tours.place_id = places.place_id WHERE places.country_id = ?";
    private static final String SQL_READ_BY_PLACE_ID = "SELECT * FROM tours WHERE place_id = ?";
    private static final String SQL_DELETE_OLD_TOURS = "DELETE FROM tours WHERE return_date < CURRENT_DATE()";
    private static final String ROW_TOURS_TOUR_ID = "tours.tour_id";
    private static final String ROW_TOURS_ARRIVE_DATE = "tours.arrive_date";
    private static final String ROW_TOURS_ARRIVE_TIME = "tours.arrive_time";
    private static final String ROW_TOURS_RETURN_DATE = "tours.return_date";
    private static final String ROW_TOURS_RETURN_TIME = "tours.return_time";
    private static final String ROW_TOURS_PRICE = "tours.price";
    private static final String ROW_TOURS_DEFAULT_PRICE = "tours.default_price";
    private static final String ROW_TOURS_SEATS_CLASS = "tours.seats_class";
    private static final String ROW_TOURS_STATUS = "tours.status";
    private static final String ROW_TOURS_SALE = "tours.sale";
    private static final String ROW_TOURS_PLACE_ID = "tours.place_id";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlTourDAO INSTANCE = new MySqlTourDAO();
    }

    private MySqlTourDAO() {}

    public static MySqlTourDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Tour entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setDate(1, entity.getArriveDate());
            statement.setTime(2, entity.getArriveTime());
            statement.setDate(3, entity.getReturnDate());
            statement.setTime(4, entity.getReturnTime());
            statement.setDouble(5, entity.getPrice());
            statement.setDouble(6, entity.getDefaultPrice());
            statement.setString(7, entity.getSeatsClass().getValue());
            statement.setInt(8, entity.getPlace().getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Tour read(int key) {
        Tour result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createTour(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Tour> readAll() {
        List<Tour> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Tour tour = createTour(resultSet);
                result.add(tour);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Tour entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setDate(1, entity.getArriveDate());
            statement.setTime(2, entity.getArriveTime());
            statement.setDate(3, entity.getReturnDate());
            statement.setTime(4, entity.getReturnTime());
            statement.setDouble(5, entity.getPrice());
            statement.setDouble(6, entity.getDefaultPrice());
            statement.setString(7, entity.getSeatsClass().getValue());
            statement.setString(8, entity.getStatus());
            statement.setInt(9, entity.getSale());
            statement.setInt(10, entity.getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Tour> execute(QueriesEnum query, Object[] params) {
        List<Tour> result = null;
        switch (query) {
            case DELETE_OLD_TOURS:
                if (deleteOldTours()) {
                    result = new ArrayList<>();
                }
                break;
        }
        return result;
    }

    @Override
    public List<Tour> execute(QueriesEnum query, int param) {
        List<Tour> result = null;
        switch (query) {
            case TOUR_BY_COUNTRY_ID:
                result = readByCountryId(param);
                break;
            case TOUR_BY_PLACE_ID:
                result = readByPlaceId(param);
                break;
        }
        return result;
    }

    /**
     * <p>Method for reading Tour entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Tour} from resultSet
     */
    private Tour createTour(ResultSet resultSet) {
        Tour result = null;
        try {
            int tourId = resultSet.getInt(ROW_TOURS_TOUR_ID);
            Date arriveDate = resultSet.getDate(ROW_TOURS_ARRIVE_DATE);
            Time arriveTime = resultSet.getTime(ROW_TOURS_ARRIVE_TIME);
            Date returnDate = resultSet.getDate(ROW_TOURS_RETURN_DATE);
            Time returnTime = resultSet.getTime(ROW_TOURS_RETURN_TIME);
            int allPlaces = 0;
            double price = resultSet.getDouble(ROW_TOURS_PRICE);
            double oldPrice = resultSet.getDouble(ROW_TOURS_DEFAULT_PRICE);
            SeatsClassesEnum seatsClass = SeatsClassesEnum.valueOf(resultSet.getString(ROW_TOURS_SEATS_CLASS).toUpperCase());
            String status = resultSet.getString(ROW_TOURS_STATUS);
            int sale = resultSet.getInt(ROW_TOURS_SALE);
            int placeId = resultSet.getInt(ROW_TOURS_PLACE_ID);
            Place place = MySqlPlaceDAO.getInstance().read(placeId);
            result = new Tour(tourId, arriveDate, arriveTime, returnDate, returnTime, allPlaces, price,
                    oldPrice, seatsClass, status, sale, place);
            result.setStatus(status);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Tour> readByCountryId(int countryId) {
        List<Tour> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_COUNTRY_ID)) {
            statement.setInt(1, countryId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tour tour = createTour(resultSet);
                result.add(tour);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Tour> readByPlaceId(int placeId) {
        List<Tour> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_PLACE_ID)) {
            statement.setInt(1, placeId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Tour tour = createTour(resultSet);
                result.add(tour);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private boolean deleteOldTours() {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE_OLD_TOURS)) {
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
