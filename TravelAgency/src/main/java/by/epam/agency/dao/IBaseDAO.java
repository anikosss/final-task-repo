package by.epam.agency.dao;

import by.epam.agency.entity.IEntity;

import java.util.List;

/**
 * <p>Interface that describes all operations with database with entities</p>
 *
 * @param <T> entity parameter
 * @author Dmitry Anikeichenko
 */
public interface IBaseDAO<T extends IEntity> {
    boolean create(T entity);
    T read(int key);
    List<T> readAll();
    boolean update(T entity);
    boolean delete(int key);
    List<T> execute(QueriesEnum query, Object[] params);
    List<T> execute(QueriesEnum query, int param);
}
