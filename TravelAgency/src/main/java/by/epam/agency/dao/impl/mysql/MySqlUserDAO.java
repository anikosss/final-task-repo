package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link User} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlUserDAO implements IBaseDAO<User> {
    private static final Logger LOGGER = Logger.getLogger(MySqlUserDAO.class);
    private static final String SQL_CREATE = "INSERT INTO users(login, password, email) VALUES(?,?,?)";
    private static final String SQL_READ = "SELECT * FROM users WHERE user_id=? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM users";
    private static final String SQL_UPDATE = "UPDATE users SET login=?, password=?, email=?, role=? WHERE user_id=?";
    private static final String SQL_DELETE = "DELETE FROM users WHERE user_id = ?";
    private static final String SQL_READ_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String SQL_READ_BY_EMAIL = "SELECT * FROM users WHERE email = ?";
    private static final String ROW_ID = "users.user_id";
    private static final String ROW_LOGIN = "users.login";
    private static final String ROW_PASSWORD = "users.password";
    private static final String ROW_EMAIL = "users.email";
    private static final String ROW_ROLE = "users.role";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlUserDAO INSTANCE = new MySqlUserDAO();
    }

    private MySqlUserDAO() {}

    public static MySqlUserDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(User user) {
        try(PoolConnection connection = pool.getConnection();
            PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public User read(int key) {
        User result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<User> readAll() {
        List<User> result = new ArrayList<>();
        try(PoolConnection connection = pool.getConnection();
            PreparedStatement statement = connection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                User user = createUser(resultSet);
                result.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(User user) {
        try(PoolConnection connection = pool.getConnection();
            PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getRole().getValue());
            statement.setInt(5, user.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<User> execute(QueriesEnum query, Object[] params) {
        List<User> result = null;
        User user;
        switch (query) {
            case USER_BY_LOGIN:
                String login = (String) params[0];
                user = readByLogin(login);
                result = new ArrayList<>();
                if (user != null) {
                    result.add(user);
                }
                break;
            case USER_BY_EMAIL:
                String email = (String) params[0];
                user = readByEmail(email);
                result = new ArrayList<>();
                if (user != null) {
                    result.add(user);
                }
        }
        return result;
    }

    @Override
    public List<User> execute(QueriesEnum query, int param) {
        return null;
    }

    /**
     * <p>Method for reading User entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link User} from resultSet
     */
    private User createUser(ResultSet resultSet) {
        User result = null;
        try {
            int id = resultSet.getInt(ROW_ID);
            String login = resultSet.getString(ROW_LOGIN);
            String pass = resultSet.getString(ROW_PASSWORD);
            String email = resultSet.getString(ROW_EMAIL);
            String role = resultSet.getString(ROW_ROLE);
            result = new User(id, login, pass, email, RolesEnum.valueOf(role.toUpperCase()));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private User readByLogin(String login) {
        User result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_LOGIN)) {
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private User readByEmail(String email) {
        User result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_EMAIL)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

}
