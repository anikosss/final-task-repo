package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Hotel} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlHotelDAO implements IBaseDAO<Hotel> {
    private static final Logger LOGGER = Logger.getLogger(MySqlHotelDAO.class);
    private static final String SQL_CREATE = "INSERT INTO hotels(title, stars, all_rooms, " +
            "main_image, room_image, place_id) VALUES (?,?,?,?,?,?);";
    private static final String SQL_READ = "SELECT * FROM hotels WHERE hotel_id = ? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM hotels";
    private static final String SQL_UPDATE = "UPDATE hotels SET title = ?, stars = ?, all_rooms = ?, main_image = ?, " +
            "room_image = ?, place_id = ? WHERE hotel_id = ?";
    private static final String SQL_DELETE = "DELETE FROM hotels WHERE hotel_id = ?";
    private static final String SQL_READ_BY_PLACE_ID = "SELECT * FROM hotels INNER JOIN places ON " +
            "hotels.place_id = places.place_id WHERE hotels.place_id = ?";
    private static final String ROW_HOTEL_ID = "hotels.hotel_id";
    private static final String ROW_TITLE = "hotels.title";
    private static final String ROW_STARS = "hotels.stars";
    private static final String ROW_ALL_ROOMS = "hotels.all_rooms";
    private static final String ROW_MAIN_IMAGE = "hotels.main_image";
    private static final String ROW_ROOM_IMAGE = "hotels.room_image";
    private static final String ROW_PLACE_ID = "hotels.place_id";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlHotelDAO INSTANCE = new MySqlHotelDAO();
    }

    private MySqlHotelDAO() {}

    public static MySqlHotelDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }


    @Override
    public boolean create(Hotel entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getStars());
            statement.setInt(3, entity.getAllRooms());
            statement.setString(4, entity.getMainImage());
            statement.setString(5, entity.getRoomImage());
            statement.setInt(6, entity.getPlaceId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Hotel read(int key) {
        Hotel result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createHotel(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Hotel> readAll() {
        List<Hotel> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Hotel hotel = createHotel(resultSet);
                result.add(hotel);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Hotel entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getStars());
            statement.setInt(3, entity.getAllRooms());
            statement.setString(4, entity.getMainImage());
            statement.setString(5, entity.getRoomImage());
            statement.setInt(6, entity.getPlaceId());
            statement.setInt(7, entity.getId());
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Hotel> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Hotel> execute(QueriesEnum query, int param) {
        return null;
    }

    /**
     * <p>Method for reading Hotel entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Hotel} from resultSet
     */
    private Hotel createHotel(ResultSet resultSet) {
        Hotel result = null;
        try {
            int hotelId = resultSet.getInt(ROW_HOTEL_ID);
            String title = resultSet.getString(ROW_TITLE);
            int stars = resultSet.getInt(ROW_STARS);
            int allRooms = resultSet.getInt(ROW_ALL_ROOMS);
            String mainImage = resultSet.getString(ROW_MAIN_IMAGE);
            String roomImage = resultSet.getString(ROW_ROOM_IMAGE);
            int placeId = resultSet.getInt(ROW_PLACE_ID);
            result = new Hotel(hotelId, title, stars, allRooms, allRooms, mainImage, roomImage, placeId);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    public List<Hotel> readByPlaceId(int key) {
        List<Hotel> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_PLACE_ID)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Hotel hotel = createHotel(resultSet);
                result.add(hotel);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
