package by.epam.agency.dao;

/**
 * <p>Enum for not simple CRUD operations</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum QueriesEnum {
    USER_BY_LOGIN,
    USER_BY_EMAIL,
    COUNTRY_BY_NAME,
    ORDER_BY_HOTEL_ID,
    ORDER_CONFIRM,
    ORDER_NOT_CONFIRMED,
    ORDER_CONFIRMED,
    SHOPPING_CART_BY_USER_ID,
    SHOPPING_CART_BY_USER_AND_TOUR,
    TOUR_BY_COUNTRY_ID,
    TOUR_BY_PLACE_ID,
    PLACE_BY_COUNTRY_ID,
    DELETE_OLD_TOURS;
}
