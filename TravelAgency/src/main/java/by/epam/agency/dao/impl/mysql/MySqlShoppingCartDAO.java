package by.epam.agency.dao.impl.mysql;

import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.entity.impl.*;
import by.epam.agency.pool.ConnectionsPool;
import by.epam.agency.pool.PoolConnection;
import by.epam.agency.service.HotelService;
import by.epam.agency.service.TourService;
import by.epam.agency.service.UserService;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link ShoppingCart} entity from MySQL database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class MySqlShoppingCartDAO implements IBaseDAO<ShoppingCart> {
    private static final Logger LOGGER = Logger.getLogger(MySqlShoppingCartDAO.class);
    private static final String SQL_CREATE = "INSERT INTO shopping_carts (user_id, tour_id, hotel_id) VALUES (?, ?, ?)";
    private static final String SQL_READ = "SELECT * FROM shopping_carts WHERE shopping_cart_id = ? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM shopping_carts";
    private static final String SQL_UPDATE = "UPDATE shopping_carts SET user_id = ?, tour_id = ?, hotel_id = ? " +
            "WHERE shopping_cart_id = ?";
    private static final String SQL_DELETE = "DELETE FROM shopping_carts WHERE shopping_cart_id = ?";
    private static final String SQL_READ_BY_USER_ID = "SELECT * FROM shopping_carts WHERE user_id = ?";
    private static final String SQL_READ_BY_USER_AND_TOUR = "SELECT * FROM shopping_carts WHERE (user_id = ? " +
            "AND tour_id = ?)";
    private static final String ROW_SHOPPING_CART_ID = "shopping_carts.shopping_cart_id";
    private static final String ROW_USER_ID = "shopping_carts.user_id";
    private static final String ROW_TOUR_ID = "shopping_carts.tour_id";
    private static final String ROW_HOTEL_ID = "shopping_carts.hotel_id";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlShoppingCartDAO INSTANCE = new MySqlShoppingCartDAO();
    }

    private MySqlShoppingCartDAO() {}

    public static MySqlShoppingCartDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(ShoppingCart entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setInt(1, entity.getUser().getId());
            statement.setInt(2, entity.getTour().getId());
            statement.setInt(3, entity.getHotel().getId());
            statement.execute();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public ShoppingCart read(int key) {
        ShoppingCart result = null;
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createShoppingCart(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<ShoppingCart> readAll() {
        List<ShoppingCart> result = new ArrayList<>();
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_ALL);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                ShoppingCart shoppingCart = createShoppingCart(resultSet);
                result.add(shoppingCart);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(ShoppingCart entity) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setInt(1, entity.getUser().getId());
            statement.setInt(2, entity.getTour().getId());
            statement.setInt(3, entity.getHotel().getId());
            statement.setInt(4, entity.getId());
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<ShoppingCart> execute(QueriesEnum query, Object[] params) {
        List<ShoppingCart> result = null;
        switch (query) {
            case SHOPPING_CART_BY_USER_AND_TOUR:
                int userId = Integer.parseInt((String)params[0]);
                int tourId = Integer.parseInt((String)params[1]);
                result = readByUserAndTour(userId, tourId);
                break;
        }
        return result;
    }

    @Override
    public List<ShoppingCart> execute(QueriesEnum query, int param) {
        List<ShoppingCart> result = null;
        switch (query) {
            case SHOPPING_CART_BY_USER_ID:
                result = readByUserId(param);
                break;
        }
        return result;
    }

    /**
     * <p>Method for reading ShoppingCart entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link ShoppingCart} from resultSet
     */
    private ShoppingCart createShoppingCart(ResultSet resultSet) {
        ShoppingCart shoppingCart = null;
        try {
            int shoppingCartId = resultSet.getInt(ROW_SHOPPING_CART_ID);
            int userId = resultSet.getInt(ROW_USER_ID);
            int tourId = resultSet.getInt(ROW_TOUR_ID);
            int hotelId = resultSet.getInt(ROW_HOTEL_ID);
            User user = UserService.getUserById(userId);
            Tour tour = TourService.getTourById(tourId);
            Hotel hotel = HotelService.getHotelById(hotelId);
            shoppingCart = new ShoppingCart(shoppingCartId, user, tour, hotel);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return  shoppingCart;
    }

    private List<ShoppingCart> readByUserId(int userId) {
        List<ShoppingCart> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_USER_ID)) {
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ShoppingCart shoppingCart = createShoppingCart(resultSet);
                result.add(shoppingCart);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<ShoppingCart> readByUserAndTour(int userId, int tourId) {
        List<ShoppingCart> result = new ArrayList<>();
        ResultSet resultSet = null;
        try(PoolConnection poolConnection = pool.getConnection();
            PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_USER_AND_TOUR)) {
            statement.setInt(1, userId);
            statement.setInt(2, tourId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ShoppingCart shoppingCart = createShoppingCart(resultSet);
                result.add(shoppingCart);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
