package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link ShoppingCart} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class ShoppingCartService {
    private static final Logger LOGGER = Logger.getLogger(ShoppingCartService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<ShoppingCart> shoppingCartDAO = factory.getShoppingCartDAO();

    public static boolean addShoppingCart(ShoppingCart shoppingCart) {
        boolean result = false;
        try {
            result = shoppingCartDAO.create(shoppingCart);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_CART_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<ShoppingCart> getAllByUserId(int userId) {
        List<ShoppingCart> result = null;
        try {
            result = shoppingCartDAO.execute(QueriesEnum.SHOPPING_CART_BY_USER_ID, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_CART_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static ShoppingCart getShoppingCartById(int id) {
        ShoppingCart result = null;
        try {
            result = shoppingCartDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_CART_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteById(int id) {
        boolean result = false;
        try {
            result = shoppingCartDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_CART_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<ShoppingCart> getByUserAndTour(int userId, int tourId) {
        List<ShoppingCart> result = null;
        try {
            result = shoppingCartDAO.execute(QueriesEnum.SHOPPING_CART_BY_USER_AND_TOUR,
                    new String[] { String.valueOf(userId), String.valueOf(tourId) });
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_CART_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
