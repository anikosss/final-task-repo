package by.epam.agency.service.exception;

/**
 * <p>Exception throws when error occurred in service methods</p>
 *
 * @author Dmitry Anikeichenko
 */
public class ServiceException extends Exception {

    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }


}
