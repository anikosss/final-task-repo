package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Hotel} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class HotelService {
    private static final Logger LOGGER = Logger.getLogger(HotelService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<Hotel> hotelDAO = factory.getHotelDAO();

    public static List<Hotel> getAllHotels() {
        List<Hotel> result = null;
        try {
            result = hotelDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_HOTEL_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean addHotel(Hotel hotel) {
        boolean result = false;
        try {
            result = hotelDAO.create(hotel);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_HOTEL_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Hotel getHotelById(int id) {
        Hotel result = null;
        try {
            result = hotelDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_HOTEL_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteById(int id) {
        boolean result = false;
        try {
            result = hotelDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_HOTEL_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateHotel(Hotel hotel) {
        boolean result = false;
        try {
            result = hotelDAO.update(hotel);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_HOTEL_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
