package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Tour} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class TourService {
    private static final Logger LOGGER = Logger.getLogger(TourService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<Tour> tourDAO = factory.getTourDAO();

    public static List<Tour> getAllTours() {
        List<Tour> result = null;
        try {
            result = tourDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean addTour(Tour tour) {
        boolean result = false;
        try {
            result = tourDAO.create(tour);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteTourById(int id) {
        boolean result = false;
        try {
             result = tourDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Tour getTourById(int id) {
        Tour result = null;
        try {
            result = tourDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Tour> getByCountryId(int countryId) {
        List<Tour> result = null;
        try {
            result = tourDAO.execute(QueriesEnum.TOUR_BY_COUNTRY_ID, countryId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Tour> getByPlaceId(int placeId) {
        List<Tour> result = null;
        try {
            result = tourDAO.execute(QueriesEnum.TOUR_BY_PLACE_ID, placeId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateTour(Tour tour) {
        boolean result = false;
        try {
            result = tourDAO.update(tour);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_TOUR_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteOldTours() {
        boolean result = false;
        try {
            List<Tour> list = tourDAO.execute(QueriesEnum.DELETE_OLD_TOURS, null);
            if (list == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_OLD_TOURS_DELETE));
            }
            result = true;
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
