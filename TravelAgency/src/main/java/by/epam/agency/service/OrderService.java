package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Order} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class OrderService {
    private static final Logger LOGGER = Logger.getLogger(OrderService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<Order> orderDAO = factory.getOrderDAO();

    public static Order getOrderById(int id) {
        Order result = null;
        try {
            result = orderDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getByHotelId(int hotelId) {
        List<Order> result = new ArrayList<>();
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_HOTEL_ID, hotelId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_READ_BY_HOTEL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrders() {
        List<Order> result = null;
        try {
            result = orderDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean addNewOrder(Order order) {
        boolean result = false;
        try {
            result = orderDAO.create(order);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateOrder(Order order) {
        boolean result = false;
        try {
            result = orderDAO.update(order);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteById(int id) {
        boolean result = false;
        try {
             result = orderDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getNotConfirmedOrders(int userId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_NOT_CONFIRMED, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getConfirmedOrders(int userId) {
        List<Order> result = new ArrayList<>();
        try {
             result = orderDAO.execute(QueriesEnum.ORDER_CONFIRMED, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

}
