package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;


/**
 * <p>Class that provides methods for manipulate {@link Country} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class CountryService {
    private static final Logger LOGGER = Logger.getLogger(CountryService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<Country> countryDAO = factory.getCountryDAO();

    public static List<Country> getAllCountries() {
        List<Country> result = null;
        try {
            result = countryDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_COUNTRY_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Country getCountryById(int id) {
        Country result = null;
        try {
            result  = countryDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_COUNTRY_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

}
