package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link User} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class UserService {
    private static final Logger LOGGER = Logger.getLogger(UserService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<User> userDAO = factory.getUserDAO();

    public static boolean registerUser(User user) {
        boolean result = true;
        try {
            result = userDAO.create(user);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User loginUser(String login) {
        User result = null;
        try {
            List<User> list = userDAO.execute(QueriesEnum.USER_BY_LOGIN, new String[] { login });
            if (list == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_READ));
            }
            if (!list.isEmpty()) {
                result = list.get(0);
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<User> getAllUsers() {
        List<User> result = null;
        try {
            result = userDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserById(int id) {
        User result = null;
        try {
            result = userDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserByLogin(String login) {
        User result = null;
        try {
            List<User> list = userDAO.execute(QueriesEnum.USER_BY_LOGIN, new String[] { login });
            if (list == null || list.size() == 0) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_READ));
            }
            result = list.get(0);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserByEmail(String email) {
        User result = null;
        try {
            List<User> list = userDAO.execute(QueriesEnum.USER_BY_EMAIL, new String[] { email });
            if (list == null || list.size() == 0) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_READ));
            }
            result = list.get(0);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

}
