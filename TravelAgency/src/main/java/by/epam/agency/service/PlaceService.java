package by.epam.agency.service;

import by.epam.agency.dao.DAOEnum;
import by.epam.agency.dao.IBaseDAO;
import by.epam.agency.dao.QueriesEnum;
import by.epam.agency.dao.factory.BaseFactory;
import by.epam.agency.dao.factory.IDAOFactory;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.ErrorsEnum;
import by.epam.agency.manager.ErrorsManager;
import by.epam.agency.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Place} entities with chosen type of database</p>
 *
 * @author Dmitry Anikeichenko
 */
public class PlaceService {
    private static final Logger LOGGER = Logger.getLogger(PlaceService.class);
    private static IDAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static IBaseDAO<Place> placeDAO = factory.getPlaceDAO();

    public static List<Place> getAllPlaces() {
        List<Place> result = null;
        try {
            result = placeDAO.readAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Place> getPlacesByCountry(int countryId) {
        List<Place> result = new ArrayList<>();
        try {
            result = placeDAO.execute(QueriesEnum.PLACE_BY_COUNTRY_ID, countryId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_READ_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Place getPlaceById(int id) {
        Place result = null;
        try {
            result = placeDAO.read(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_READ));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean addPlace(Place place) {
        boolean result = false;
        try {
            result = placeDAO.create(place);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deletePlaceById(int id) {
        boolean result = false;
        try {
            result = placeDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updatePlace(Place place) {
        boolean result = false;
        try {
            result = placeDAO.update(place);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_PLACE_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
