package by.epam.agency.controller;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * <p>Context for storage, addition and modification information from request</p>
 *
 * @author Dmitry Anikeichenko
 */
public class RequestContext {
    private static final Logger LOGGER = Logger.getLogger(RequestContext.class);
    private HttpSession session;
    private Map<String, Object> requestAttributes = new HashMap<>();
    private Map<String, String[]> requestParameters = new HashMap<>();
    private Map<String, Object> sessionAttributes = new HashMap<>();

    public RequestContext() {

    }

    /**
     * <p>Method for extracting values from http request to context</p>
     *
     * @param request http request from user
     * @return true if there are no exceptions, false otherwise
     */
    public boolean extractValues(HttpServletRequest request) {
        try {
            session = request.getSession();
            Enumeration<String> attributes = request.getAttributeNames();
            while (attributes.hasMoreElements()) {
                String name = attributes.nextElement();
                Object attribute = request.getAttribute(name);
                requestAttributes.put(name, attribute);
            }
            Enumeration<String> parameters = request.getParameterNames();
            while (parameters.hasMoreElements()) {
                String name = parameters.nextElement();
                String[] values = request.getParameterValues(name);
                requestParameters.put(name, values);
            }
            Enumeration<String> sessionAttr = session.getAttributeNames();
            while (sessionAttr.hasMoreElements()) {
                String name = sessionAttr.nextElement();
                Object attribute = session.getAttribute(name);
                sessionAttributes.put(name, attribute);
            }
            return true;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return false;
    }

    /**
     * <p>Method for inserting new or modified values to the request</p>
     *
     * @param request http request from user
     * @return true if there are no exception, false otherwise
     */
    public boolean insertValues(HttpServletRequest request) {
        try {
            for (Map.Entry<String, Object> entry : requestAttributes.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                request.setAttribute(key, value);
            }
            for (Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                request.getSession().setAttribute(key, value);
            }
            clear();
            return true;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return false;
    }

    /**
     * <p>Clearing the context</p>
     */
    public void clear() {
        requestAttributes.clear();
        requestParameters.clear();
        sessionAttributes.clear();
    }

    /**
     * <p>Invalidation session in the request</p>
     */
    public void invalidateSession() {
        if (session != null) {
            session.invalidate();
        }
        sessionAttributes.clear();
    }

    public Object getRequestAttribute(String key) {
        return requestAttributes.get(key);
    }

    public void addRequestAttribute(String key, Object value) {
        requestAttributes.put(key, value);
    }

    public void removeRequestAttribute(String key) {
        requestAttributes.remove(key);
    }

    public List<Object> getAllRequestAttributes() {
        List<Object> result = new ArrayList<>();
        for (Map.Entry<String, Object> entry : requestAttributes.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

    public String[] getRequestParameter(String key) {
        return requestParameters.get(key);
    }

    public void addRequestParameter(String key, String[] value) {
        requestParameters.put(key, value);
    }

    public void removeRequestParameter(String key) {
        requestParameters.remove(key);
    }

    public List<String[]> getAllRequestParameters() {
        List<String[]> result = new ArrayList<>();
        for (Map.Entry<String, String[]> entry : requestParameters.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

    public Object getSessionAttribute(String key) {
        return sessionAttributes.get(key);
    }

    public void addSessionAttribute(String key, Object value) {
        sessionAttributes.put(key, value);
    }

    public void removeSessionAttribute(String key) {
        sessionAttributes.remove(key);
    }

    public List<Object> getAllSessionAttributes() {
        List<Object> result = new ArrayList<>();
        for (Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
            result.add(entry.getValue());
        }
        return result;
    }

}
