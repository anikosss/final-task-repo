package by.epam.agency.controller.util;

/**
 * <p>Enum from choosing how to redirect user to the result page</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum MethodsEnum {
    FORWARD, REDIRECT;
}
