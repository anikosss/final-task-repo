package by.epam.agency.util;

/**
 * <p>Class for encoding passwords</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Encoder {
    private static final String KEY = "KeyForEncodingInformation";

    /**
     * <p>Method for encoding text</p>
     *
     * @param text Text for encoding
     * @return Encoded text
     */
    public static String encode(String text) {
        byte[] bText = text.getBytes();
        byte[] bKey = KEY.getBytes();
        byte[] result = new byte[text.length()];
        for (int i = 0; i < bText.length; i++) {
            result[i] = (byte) (bText[i] ^ bKey[i % bKey.length]);
        }
        return new String(result);
    }

    /**
     * <p>Method for decoding text</p>
     *
     * @param text Text for decoding
     * @return Decoded text
     */
    public static String decode(String text) {
        byte[] bText = text.getBytes();
        byte[] result = new byte[bText.length];
        byte[] bKey = KEY.getBytes();
        for (int i = 0; i < bText.length; i++) {
            result[i] = (byte) (bText[i] ^ bKey[i % bKey.length]);
        }
        return new String(result);
    }
}
