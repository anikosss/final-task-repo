package by.epam.agency.filter;

import by.epam.agency.entity.impl.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for checking action that requires to sign in</p>
 *
 * @author Dmitry Anikeichenko
 */
@WebFilter(filterName = "UserAccessFilter", servletNames = { "Controller" })
public class UserAccessFilter implements Filter {
    private static final String ACTION = "action";
    private static final String USER = "user";
    private static final int CODE_404 = 404;

    private enum UserCommands {
        ADD_TO_CART, CANCEL_ORDER, CHECKOUT_ORDER, DELETE_FROM_CART, LOGOUT, PROFILE, SHOPPING_CART,
        USER_CONFIRM_ORDER
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        processFilter(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from user and possibility to do this action for user</p>
     *
     * @param request http request from user
     * @param response http response for user
     * @throws IOException
     * @throws ServletException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            if (!response.isCommitted()) {
                response.sendError(CODE_404);
            }
            return;
        }
        User user = (User) request.getSession().getAttribute(USER);
        for (UserCommands command : UserCommands.values()) {
            if (command.name().equals(action.toUpperCase())) {
                if (user == null || user.getRole() == null) {
                    if (!response.isCommitted()) {
                        response.sendError(CODE_404);
                    }
                    return;
                }
                break;
            }
        }
    }

    @Override
    public void destroy() {

    }
    
}
