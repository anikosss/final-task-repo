package by.epam.agency.filter;

import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for checking access to the admin pages</p>
 *
 * @author Dmitry Anikeichenko
 */
@WebFilter(filterName = "AdminAccessFilter", servletNames = { "Controller" })
public class AdminAccessFilter implements Filter {
    private static final String ACTION = "action";
    private static final int CODE_404 = 404;

    private enum AdminCommands {
        ALL_TOURS, ALL_PLACES, ALL_HOTELS, ALL_ORDERS, ADD_HOTEL, ADD_PLACE, ADD_TOUR, CHOOSE_HOTEL_COUNTRY,
        CHOOSE_HOTEL_PLACE, CHOOSE_TOUR_COUNTRY, CHOOSE_TOUR_PLACE, DELETE_HOTEL, DELETE_PLACE,
        GO_TO_ADD_HOTEL, GO_TO_ADD_PLACE, GO_TO_ADD_TOUR, ADMIN_CONFIRM_ORDER, DELETE_ORDER, TOUR_DETAILS,
        PLACE_DETAILS, HOTEL_DETAILS, GO_TO_CHANGE_TOUR, GO_TO_CHANGE_PLACE, GO_TO_CHANGE_HOTEL, CHANGE_TOUR,
        CHANGE_PLACE, CHANGE_HOTEL, DELETE_OLD_TOURS, MAKE_TOUR_HOT, DELETE_TOUR, REMOVE_SALE
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        processFilter(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from user and user access rights for this action</p>
     *
     * @param request http request from user
     * @param response http response for user
     * @throws IOException
     * @throws ServletException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            return;
        }
        User user = (User) request.getSession().getAttribute("user");
        for (AdminCommands command : AdminCommands.values()) {
            if (command.name().equals(action.toUpperCase())) {
                if (user == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (user.getRole() == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (!user.getRole().equals(RolesEnum.ADMIN)) {
                    response.sendError(CODE_404);
                    return;
                }
                break;
            }
        }
    }

    @Override
    public void destroy() {

    }
    
}
