package by.epam.agency.tag;

import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * <p>Custom tag class</p>
 *
 * @author Dmitry Anikeichenko
 */
public class CopyrightTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(CopyrightTag.class);
    private static final String TITLE = "Anikeichenko Dmitry";
    private static final String COPYRIGHT = "&#169;";
    private String email;


    /**
     *
     * @param email Email as attribute of the tag
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter writer = pageContext.getOut();
            writer.println(TITLE + "  " + COPYRIGHT);
            writer.println(email);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return SKIP_BODY;
    }
}
