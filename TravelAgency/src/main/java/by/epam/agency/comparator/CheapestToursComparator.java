package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;

import java.util.Comparator;

/**
 * <p>Sorting tours by price</p>
 *
 * @author Dmitry Anikeichenko
 */
public class CheapestToursComparator implements Comparator<Tour> {

    @Override
    public int compare(Tour tour1, Tour tour2) {
        return Double.compare(tour1.getPrice(), tour2.getPrice());
    }
}
