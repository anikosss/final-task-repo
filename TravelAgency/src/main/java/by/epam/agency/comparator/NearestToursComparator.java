package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;

import java.sql.Date;
import java.util.Comparator;

/**
 * <p>Sorting tours days remains to arrive</p>
 *
 * @author Dmitry Anikeichenko
 */
public class NearestToursComparator implements Comparator<Tour> {

    @Override
    public int compare(Tour tour1, Tour tour2) {
        Date todayDate = new Date(System.currentTimeMillis());
        long diff1 = Math.abs(tour1.getArriveDate().getTime() - todayDate.getTime());
        long diff2 = Math.abs(tour2.getArriveDate().getTime() - todayDate.getTime());
        return Long.compare(diff1, diff2);
    }
}
