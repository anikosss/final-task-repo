package by.epam.agency.comparator;

import by.epam.agency.entity.impl.Tour;

import java.util.Comparator;

/**
 * <p>Sorting tours by addition date</p>
 *
 * @author Dmitry Anikeichenko
 */
public class LatestToursComparator implements Comparator<Tour> {

    @Override
    public int compare(Tour tour1, Tour tour2) {
        return Integer.compare(tour1.getId(), tour2.getId());
    }
}
