package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AllHotelsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllHotelsCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String HOTELS = "hotels";
    private static final String PAGE = "page";
    private static final int HOTELS_PER_PAGE = 20;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<Hotel> allHotels = HotelService.getAllHotels();
            List<Hotel> hotels = createHotels(allHotels, page, HOTELS_PER_PAGE);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(HOTELS, hotels);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_HOTELS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Hotel> createHotels(List<Hotel> allHotels, int page, int count) {
        List<Hotel> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allHotels.size() - 1) {
                break;
            }
            result.add(allHotels.get(i));
        }
        return result;
    }
}
