package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.impl.admin.PlaceInfoValidator;
import org.apache.log4j.Logger;

public class AddPlaceCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddPlaceCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String COUNTRY = "country";
    private static final String TITLE = "title";
    private static final String IMAGE = "image";
    private PlaceInfoValidator placeValidator = PlaceInfoValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = placeValidator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_PLACE);
            }
            int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
            Country country = CountryService.getCountryById(countryId);
            String title = context.getRequestParameter(TITLE)[0];
            String image = context.getRequestParameter(IMAGE)[0];
            Place place = new Place(0, title, image, country, null);
            if (!PlaceService.addPlace(place)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_PLACE_ADD.getValue());
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_PLACES);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_PLACE_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
