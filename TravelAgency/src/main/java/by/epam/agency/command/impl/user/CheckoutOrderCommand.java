package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.impl.user.CheckoutOrderValidator;
import org.apache.log4j.Logger;

public class CheckoutOrderCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(CheckoutOrderCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String CART = "cart";
    private static final String ORDER = "order";
    private CheckoutOrderValidator validator = CheckoutOrderValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_SHOPPING_CART);
            }
            int cartId = Integer.parseInt(context.getRequestParameter(CART)[0]);
            ShoppingCart shoppingCart = ShoppingCartService.getShoppingCartById(cartId);
            context.addRequestAttribute(ORDER, shoppingCart);
            result = PagesManager.getPage(PagesEnum.CHECKOUT_ORDER);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
