package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.TourService;
import org.apache.log4j.Logger;

public class DeleteOldToursCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteOldToursCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            boolean deleteResult = TourService.deleteOldTours();
            if (deleteResult) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_DELETE_OLD_TOURS.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
