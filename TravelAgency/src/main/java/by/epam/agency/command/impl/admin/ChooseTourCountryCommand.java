package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.impl.CountryValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class ChooseTourCountryCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChooseTourCountryCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String COUNTRY = "country";
    private static final String PLACES = "places";
    private CountryValidator countryValidator = CountryValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = countryValidator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_TOUR);
            }
            int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
            context.addRequestAttribute(COUNTRY, countryId);
            List<Place> places = PlaceService.getPlacesByCountry(countryId);
            context.addRequestAttribute(PLACES, places);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_TOUR_PLACE);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
