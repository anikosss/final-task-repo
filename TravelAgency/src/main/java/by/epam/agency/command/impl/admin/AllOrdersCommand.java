package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AllOrdersCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllOrdersCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ORDERS = "orders";
    private static final String PAGE = "page";
    private static final int ORDERS_PER_PAGE = 20;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<Order> allOrders = OrderService.getAllOrders();
            List<Order> orders = createOrders(allOrders, page, ORDERS_PER_PAGE);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(ORDERS, orders);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_ORDERS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Order> createOrders(List<Order> allOrders, int page, int count) {
        List<Order> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allOrders.size() - 1) {
                break;
            }
            result.add(allOrders.get(i));
        }
        return result;
    }
}
