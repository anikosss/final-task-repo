package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.impl.PlaceValidator;
import org.apache.log4j.Logger;

public class DeletePlaceCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeletePlaceCommand.class);
    private static final String METHOD = "method";
    private static final String PLACE = "place";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private PlaceValidator validator = PlaceValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_PLACES);
            }
            int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
            if (PlaceService.deletePlaceById(placeId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE,
                        MessagesEnum.SUCCESS_PLACE_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_PLACE_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_PLACES);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_PLACE_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
