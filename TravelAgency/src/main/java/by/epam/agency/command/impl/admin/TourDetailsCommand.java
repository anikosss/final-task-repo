package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

public class TourDetailsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(TourDetailsCommand.class);
    private static final String SUCCESS = "SUCCESS";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String TOUR = "tour";
    private TourValidator validator = TourValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            Tour tour = TourService.getTourById(tourId);
            context.addRequestAttribute(TOUR, tour);
            result = PagesManager.getPage(PagesEnum.ADMIN_TOUR_DETAILS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
