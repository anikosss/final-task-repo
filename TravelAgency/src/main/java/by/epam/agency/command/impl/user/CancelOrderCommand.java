package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.impl.user.CancelOrderValidator;
import org.apache.log4j.Logger;

public class CancelOrderCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(CancelOrderCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ORDER = "order";
    private CancelOrderValidator validator = CancelOrderValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            result = PagesManager.getPage(PagesEnum.GO_TO_PROFILE);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return result;
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            if (OrderService.deleteById(orderId)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_CANCEL_ORDER.getValue());
            }
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ORDER_CANCEL));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
