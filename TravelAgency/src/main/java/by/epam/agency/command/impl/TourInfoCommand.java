package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class TourInfoCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(TourInfoCommand.class);
    private static final String SUCCESS = "SUCCESS";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String TOUR = "tour";
    private static final String METHOD = "method";
    private TourValidator validator = TourValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return result;
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            Tour tour = TourService.getTourById(tourId);
            calculatePlaces(tour);
            context.addRequestAttribute(TOUR, tour);
            result = PagesManager.getPage(PagesEnum.TOUR_INFO);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private int calculateOrderedPlaces(Tour tour, int hotelId) {
        int orderedPlaces = 0;
        List<Order> orders = OrderService.getByHotelId(hotelId);
        for (Order order : orders) {
            if (tour.getArriveDate().before(order.getTour().getReturnDate()) ||
                    tour.getReturnDate().after(order.getTour().getArriveDate())) {
                orderedPlaces++;
            }
        }
        return orderedPlaces;
    }

    private void calculatePlaces(Tour tour) {
        int allPlaces = 0;
        int freePlaces = 0;
        int hotelAllPlaces;
        int hotelFreePlaces;
        for (Hotel hotel : tour.getPlace().getHotels()) {
            hotelAllPlaces = hotel.getAllRooms();
            hotelFreePlaces = hotel.getAllRooms() - calculateOrderedPlaces(tour, hotel.getId());
            allPlaces += hotelAllPlaces;
            freePlaces += hotelFreePlaces;
            hotel.setFreeRooms(hotelFreePlaces);
        }
        tour.setAllPlaces(allPlaces);
        tour.setFreePlaces(freePlaces);
    }

}
