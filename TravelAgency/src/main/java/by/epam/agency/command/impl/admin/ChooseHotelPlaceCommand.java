package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.validator.impl.CountryValidator;
import by.epam.agency.validator.impl.admin.ChoosePlaceValidator;
import org.apache.log4j.Logger;

public class ChooseHotelPlaceCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChooseHotelPlaceCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String COUNTRY = "country";
    private static final String PLACE = "place";
    private CountryValidator countryValidator = CountryValidator.getInstance();
    private ChoosePlaceValidator placeValidator = ChoosePlaceValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = countryValidator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_HOTEL);
            }
            validateResult = placeValidator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_HOTEL);
            }
            String place = context.getRequestParameter(PLACE)[0];
            context.addRequestAttribute(PLACE, place);
            String country = context.getRequestParameter(COUNTRY)[0];
            context.addRequestAttribute(COUNTRY, country);
            result = PagesManager.getPage(PagesEnum.ADMIN_WRITE_HOTEL_INFO);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
