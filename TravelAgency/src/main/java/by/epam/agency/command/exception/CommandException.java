package by.epam.agency.command.exception;

/**
 * <p>Exception throws when error occurred while command is executing</p>
 *
 * @author Dmitry Anikeichenko
 */
public class CommandException extends Exception {

    public CommandException() {
        super();
    }

    public CommandException(String message) {
        super(message);
    }
}
