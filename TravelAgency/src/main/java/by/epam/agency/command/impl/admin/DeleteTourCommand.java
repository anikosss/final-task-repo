package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

public class DeleteTourCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteTourCommand.class);
    private static final String METHOD = "method";
    private static final String TOUR = "tour";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private TourValidator validator = TourValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            if (TourService.deleteTourById(tourId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE,
                        MessagesEnum.SUCCESS_TOUR_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_TOUR_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_TOUR_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
