package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.comparator.CheapestToursComparator;
import by.epam.agency.comparator.LatestToursComparator;
import by.epam.agency.comparator.NearestToursComparator;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.ToursCatalogValidator;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ViewCatalogCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewCatalogCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ACTION = "action";
    private static final String NEAREST_CATALOG = "nearest_catalog";
    private static final String COUNTRY_CATALOG = "country_catalog";
    private static final String LAST_CATALOG = "last_catalog";
    private static final String CHEAPEST_CATALOG = "cheapest_catalog";
    private static final String PLACE_CATALOG = "place_catalog";
    private static final String COUNTRY = "country";
    private static final String PLACE = "place";
    private static final String TOURS = "tours";
    private static final String PAGE = "page";
    private static final int TOURS_PER_PAGE = 8;
    private ToursCatalogValidator validator = ToursCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return result;
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<Tour> allTours = defineTours(context);
            List<Tour> tours = createTours(allTours, page);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(TOURS, tours);
            result = PagesManager.getPage(PagesEnum.CATALOG);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Tour> createTours(List<Tour> allTours, int page) {
        List<Tour> result = new ArrayList<>();
        int start = (page - 1) * TOURS_PER_PAGE;
        int end = start + TOURS_PER_PAGE - 1;
        for (int i = start; i <= end; i++) {
            if (i > allTours.size() - 1) {
                break;
            }
            result.add(allTours.get(i));
        }
        return result;
    }

    private List<Tour> defineTours(RequestContext context) {
        String action = context.getRequestParameter(ACTION)[0];
        List<Tour> result;
        switch (action) {
            case COUNTRY_CATALOG:
                result = processCountryCatalog(context, action);
                Collections.sort(result, new NearestToursComparator());
                break;
            case NEAREST_CATALOG:
                result = processCountryCatalog(context, action);
                Collections.sort(result, new NearestToursComparator());
                break;
            case LAST_CATALOG:
                result = processCountryCatalog(context, action);
                Collections.sort(result, Collections.reverseOrder(new LatestToursComparator()));
                break;
            case CHEAPEST_CATALOG:
                result = processCountryCatalog(context, action);
                Collections.sort(result, new CheapestToursComparator());
                break;
            case PLACE_CATALOG:
                result = processPlaceCatalog(context, action);
                Collections.sort(result, new NearestToursComparator());
                break;
            default:
                result = TourService.getAllTours();
                Collections.sort(result, new NearestToursComparator());
        }
        deleteOldTours(result);
        return result;
    }

    private List<Tour> processCountryCatalog(RequestContext context, String action) {
        int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
        Country country = CountryService.getCountryById(countryId);
        context.addRequestAttribute(COUNTRY, country);
        context.addRequestAttribute(ACTION, action);
        return TourService.getByCountryId(countryId);
    }

    private List<Tour> processPlaceCatalog(RequestContext context, String action) {
        int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
        Place place = PlaceService.getPlaceById(placeId);
        context.addRequestAttribute(PLACE, place);
        context.addRequestAttribute(ACTION, action);
        return TourService.getByPlaceId(placeId);
    }

    private void deleteOldTours(List<Tour> tours) {
        Iterator<Tour> iterator = tours.iterator();
        Date todayDate = new Date(System.currentTimeMillis());
        while(iterator.hasNext()) {
            if (iterator.next().getArriveDate().getTime() < todayDate.getTime()) {
                iterator.remove();
            }
        }
    }
}
