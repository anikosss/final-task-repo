package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.User;
import by.epam.agency.entity.util.RolesEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.UserService;
import by.epam.agency.util.Encoder;
import by.epam.agency.validator.impl.RegistrationValidator;
import org.apache.log4j.Logger;

public class RegistrationCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(RegistrationCommand.class);
    private static final String METHOD = "method";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private RegistrationValidator validator = RegistrationValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            }
            String login = context.getRequestParameter(LOGIN)[0];
            String password = context.getRequestParameter(PASSWORD)[0];
            String email = context.getRequestParameter(EMAIL)[0];
            String encodedPassword = Encoder.encode(password);
            User user = new User(0, login, encodedPassword, email, RolesEnum.USER);
            if (!UserService.registerUser(user)) {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_USER_REGISTRATION.getValue());
            } else {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_REGISTRATION.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_REGISTRATION));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
