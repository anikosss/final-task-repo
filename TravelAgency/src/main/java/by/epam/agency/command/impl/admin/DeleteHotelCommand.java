package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.validator.impl.HotelValidator;
import org.apache.log4j.Logger;

public class DeleteHotelCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteHotelCommand.class);
    private static final String SUCCESS = "SUCCESS";
    private static final String METHOD = "method";
    private static final String HOTEL = "hotel";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private HotelValidator validator = HotelValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_HOTELS);
            }
            int hotelId = Integer.parseInt(context.getRequestParameter(HOTEL)[0]);
            if (HotelService.deleteById(hotelId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_HOTEL_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_HOTEL_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_HOTELS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_HOTEL_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
