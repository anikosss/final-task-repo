package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.UserService;
import by.epam.agency.validator.impl.LoginValidator;
import org.apache.log4j.Logger;

public class LoginCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String LOGIN = "login";
    private static final String USER = "user";
    private LoginValidator validator = LoginValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            }
            String login = context.getRequestParameter(LOGIN)[0];
            User dbUser = UserService.loginUser(login);
            context.addSessionAttribute(USER, dbUser);
            context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_LOGIN.getValue());
            result = PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_LOGIN));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
