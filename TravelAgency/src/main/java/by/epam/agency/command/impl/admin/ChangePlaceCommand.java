package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.impl.admin.ChangeEntityValidator;
import org.apache.log4j.Logger;

public class ChangePlaceCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangePlaceCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String PLACE = "place";
    private static final String COUNTRY = "country";
    private static final String TITLE = "title";
    private static final String IMAGE = "image";
    private static final String DEFAULT_IMAGE = "place-image-not-found.jpg";
    private ChangeEntityValidator validator = ChangeEntityValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return String.format(PagesManager.getPage(PagesEnum.FORMAT_PLACE_DETAILS),
                        Integer.parseInt(context.getRequestParameter(PLACE)[0]));
            }
            int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
            Place place = PlaceService.getPlaceById(placeId);
            changePlace(context, place);
            if (PlaceService.updatePlace(place)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_PLACE_ADD);
            }
            result = String.format(PagesManager.getPage(PagesEnum.FORMAT_PLACE_DETAILS), placeId);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changePlace(RequestContext context, Place place) {
        int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
        Country country = CountryService.getCountryById(countryId);
        place.setCountry(country);
        String title = context.getRequestParameter(TITLE)[0];
        place.setTitle(title);
        String image = context.getRequestParameter(IMAGE)[0];
        if (!image.equals(DEFAULT_IMAGE)) {
            place.setImage(image);
        }
    }
}
