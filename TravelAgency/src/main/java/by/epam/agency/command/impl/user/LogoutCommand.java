package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import org.apache.log4j.Logger;

public class LogoutCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            context.invalidateSession();
            result = PagesManager.getPage(PagesEnum.INDEX);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_LOGOUT));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
