package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.validator.impl.CountryValidator;
import org.apache.log4j.Logger;

public class CountryInfoCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(CountryInfoCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String COUNTRY = "country";
    private CountryValidator validator = CountryValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.ERROR_404);
            }
            int countryId = Integer.parseInt(context.getRequestParameter(COUNTRY)[0]);
            Country country = CountryService.getCountryById(countryId);
            context.addRequestAttribute(COUNTRY, country);
            result = PagesManager.getPage(PagesEnum.COUNTRY_INFO);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
