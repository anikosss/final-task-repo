package by.epam.agency.command;

import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.controller.RequestContext;

public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(RequestContext context) {
        return PagesManager.getPage(PagesEnum.ERROR_404);
    }

}
