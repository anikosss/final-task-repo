package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import org.apache.log4j.Logger;

import java.util.List;

public class ProfileCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ProfileCommand.class);
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String ORDERS = "orders";
    private static final String CONFIRMED_ORDERS = "confirmedOrders";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            User user = (User) context.getSessionAttribute(USER);
            List<Order> orders = OrderService.getNotConfirmedOrders(user.getId());
            List<Order> confirmedOrders = OrderService.getConfirmedOrders(user.getId());
            context.addRequestAttribute(ORDERS, orders);
            context.addRequestAttribute(CONFIRMED_ORDERS, confirmedOrders);
            result = PagesManager.getPage(PagesEnum.PROFILE);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
