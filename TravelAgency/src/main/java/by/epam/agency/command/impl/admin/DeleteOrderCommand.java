package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.impl.OrderValidator;
import org.apache.log4j.Logger;

public class DeleteOrderCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteOrderCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String ORDER = "order";
    private OrderValidator validator = OrderValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            if (OrderService.deleteById(orderId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE,
                        MessagesEnum.SUCCESS_ORDER_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_DELETE_ORDER.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ORDER_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
