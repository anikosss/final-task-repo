package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.util.SeatsClassesEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.admin.ChangeEntityValidator;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.Time;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeTourCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeTourCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String TOUR = "tour";
    private static final String ARRIVE_DATE = "arriveDate";
    private static final String ARRIVE_TIME = "arriveTime";
    private static final String RETURN_DATE = "returnDate";
    private static final String RETURN_TIME = "returnTime";
    private static final String PRICE = "price";
    private static final String SALE = "sale";
    private static final String SEATS_CLASS = "seatsClass";
    private static final String STATUS_DEFAULT = "default";
    private static final String STATUS_HOT = "hot";
    private static final String REGEX_FULL_TIME = "[\\d]{2}:[\\d]{2}:[\\d]{2}";
    private ChangeEntityValidator validator = ChangeEntityValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            MessagesEnum validateResult = validator.validate(context);
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return String.format(PagesManager.getPage(PagesEnum.FORMAT_TOUR_DETAILS),
                        Integer.parseInt(context.getRequestParameter(TOUR)[0]));
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            Tour tour = TourService.getTourById(tourId);
            changeTour(context, tour);
            if (TourService.updateTour(tour)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_TOUR_UPDATE.getValue());
            }
            result = String.format(PagesManager.getPage(PagesEnum.FORMAT_TOUR_DETAILS), tourId);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeTour(RequestContext context, Tour tour) {
        StringBuilder arriveTimeBuilder = new StringBuilder(context.getRequestParameter(ARRIVE_TIME)[0]);
        StringBuilder returnTimeBuilder = new StringBuilder(context.getRequestParameter(RETURN_TIME)[0]);
        Matcher fullTimeMatcher = Pattern.compile(REGEX_FULL_TIME).matcher(arriveTimeBuilder.toString());
        if (!fullTimeMatcher.matches()) {
            arriveTimeBuilder.append(":00");
        }
        fullTimeMatcher = Pattern.compile(REGEX_FULL_TIME).matcher(returnTimeBuilder.toString());
        if (!fullTimeMatcher.matches()) {
            returnTimeBuilder.append(":00");
        }
        Date arriveDate = Date.valueOf(context.getRequestParameter(ARRIVE_DATE)[0]);
        tour.setArriveDate(arriveDate);
        Time arriveTime = Time.valueOf(arriveTimeBuilder.toString());
        tour.setArriveTime(arriveTime);
        Date returnDate = Date.valueOf(context.getRequestParameter(RETURN_DATE)[0]);
        tour.setReturnDate(returnDate);
        Time returnTime = Time.valueOf(returnTimeBuilder.toString());
        tour.setReturnTime(returnTime);
        double defaultPrice = Double.parseDouble(context.getRequestParameter(PRICE)[0]);
        tour.setDefaultPrice(defaultPrice);
        double price = defaultPrice;
        int sale = Integer.parseInt(context.getRequestParameter(SALE)[0]);
        tour.setSale(sale);
        String status = STATUS_DEFAULT;
        if (sale != 0) {
            price = defaultPrice - (defaultPrice * sale / 100);
            status = STATUS_HOT;
        }
        tour.setPrice(price);
        tour.setStatus(status);
        SeatsClassesEnum seatsClass =
                SeatsClassesEnum.valueOf(context.getRequestParameter(SEATS_CLASS)[0].toUpperCase());
        tour.setSeatsClass(seatsClass);
    }
}
