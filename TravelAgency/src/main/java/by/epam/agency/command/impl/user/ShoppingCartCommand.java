package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.ShoppingCartService;
import org.apache.log4j.Logger;

import java.util.List;

public class ShoppingCartCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ShoppingCartCommand.class);
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String SHOPPING_CART = "shoppingCart";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            User user = (User) context.getSessionAttribute(USER);
            List<ShoppingCart> shoppingCart = ShoppingCartService.getAllByUserId(user.getId());
            context.addRequestAttribute(SHOPPING_CART, shoppingCart);
            result = PagesManager.getPage(PagesEnum.SHOPPING_CART);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
