package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.PlaceService;
import by.epam.agency.validator.impl.PlaceValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class GoToChangePlaceCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToChangePlaceCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "SUCCESS";
    private static final String PLACE = "place";
    private static final String COUNTRIES = "countries";
    private PlaceValidator validator = PlaceValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_PLACES);
            }
            int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
            Place place = PlaceService.getPlaceById(placeId);
            List<Country> countries = CountryService.getAllCountries();
            context.addRequestAttribute(PLACE, place);
            context.addRequestAttribute(COUNTRIES, countries);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHANGE_PLACE);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
