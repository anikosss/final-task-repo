package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.impl.user.UserConfirmOrderValidator;
import org.apache.log4j.Logger;

public class UserConfirmOrderCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(UserConfirmOrderCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String METHOD = "method";
    private static final String NAME = "name";
    private static final String CART = "cart";
    private static final String PHONE = "phone";
    private UserConfirmOrderValidator validator = UserConfirmOrderValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_SHOPPING_CART);
            }
            String name = context.getRequestParameter(NAME)[0];
            String phone = context.getRequestParameter(PHONE)[0];
            int cartId = Integer.parseInt(context.getRequestParameter(CART)[0]);
            ShoppingCart shoppingCart = ShoppingCartService.getShoppingCartById(cartId);
            Order order = new Order(1, name, phone, shoppingCart.getTour(), shoppingCart.getUser(),
                    shoppingCart.getHotel());
            if (OrderService.addNewOrder(order)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_USER_CONFIRM_ORDER.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_USER_CONFIRM_ORDER.getValue());
            }
            ShoppingCartService.deleteById(cartId);
            result = PagesManager.getPage(PagesEnum.GO_TO_SHOPPING_CART);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_USER_ORDER_CONFIRM));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
