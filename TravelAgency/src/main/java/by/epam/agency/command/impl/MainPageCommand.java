package by.epam.agency.command.impl;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.comparator.NearestToursComparator;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import by.epam.agency.service.TourService;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MainPageCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(MainPageCommand.class);
    private static final String METHOD = "method";
    private static final String TOURS = "tours";
    private static final String COUNTRIES_LIST = "countriesList";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            List<Tour> allTours = TourService.getAllTours();
            removeOldTours(allTours);
            Collections.sort(allTours, new NearestToursComparator());
            List<Tour> resultTours = createResultTours(allTours);
            List<Country> allCountries = CountryService.getAllCountries();
            context.addRequestAttribute(TOURS, resultTours);
            context.addRequestAttribute(COUNTRIES_LIST, allCountries);
            result = PagesManager.getPage(PagesEnum.MAIN);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Tour> createResultTours(List<Tour> allTours) {
        if (allTours.size() <= 3) {
            return allTours;
        }
        List<Tour> result = new ArrayList<>();
        for (int i = 0; i < 3; i ++) {
            result.add(allTours.get(i));
        }
        return result;
    }

    private void removeOldTours(List<Tour> tours) {
        Iterator<Tour> iterator = tours.iterator();
        Date todayDate = new Date(System.currentTimeMillis());
        while(iterator.hasNext()) {
            if (iterator.next().getArriveDate().getTime() < todayDate.getTime()) {
                iterator.remove();
            }
        }
    }
}
