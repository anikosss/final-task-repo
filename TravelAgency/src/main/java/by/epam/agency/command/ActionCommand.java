package by.epam.agency.command;

import by.epam.agency.controller.RequestContext;

/**
 * <p>Interface that describes execute operation in all commands</p>
 *
 * @author Dmitry Anikeichenko
 */
public interface ActionCommand {

    /**
     * <p>Method for doing user's request</p>
     *
     * @param context Context with information from the request
     * @return Result page
     */
    String execute(RequestContext context);
}
