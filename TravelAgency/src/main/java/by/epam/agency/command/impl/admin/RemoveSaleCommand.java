package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.TourValidator;
import org.apache.log4j.Logger;

public class RemoveSaleCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(RemoveSaleCommand.class);
    private static final String SUCCESS = "SUCCESS";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String METHOD = "method";
    private static final String TOUR = "tour";
    private static final String STATUS_DEFAULT = "default";
    private TourValidator validator = TourValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            Tour tour = TourService.getTourById(tourId);
            tour.setPrice(tour.getDefaultPrice());
            tour.setSale(0);
            tour.setStatus(STATUS_DEFAULT);
            if (TourService.updateTour(tour)) {
                context.addRequestAttribute(SUCCESS_MESSAGE,
                        MessagesEnum.SUCCESS_REMOVE_SALE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_REMOVE_SALE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

}
