package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.validator.impl.user.DeleteFromCartValidator;
import org.apache.log4j.Logger;

public class DeleteFromCartCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteFromCartCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String CART = "cart";
    private static final String METHOD = "method";
    private DeleteFromCartValidator validator = DeleteFromCartValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                if (!validateResult.equals(MessagesEnum.ERROR_VALIDATE_WRONG_CART)) {
                    return PagesManager.getPage(PagesEnum.GO_TO_SHOPPING_CART);
                }
                return PagesManager.getPage(PagesEnum.ERROR_404);
            }
            int cartId = Integer.parseInt(context.getRequestParameter(CART)[0]);
            result = PagesManager.getPage(PagesEnum.GO_TO_SHOPPING_CART);
            if (ShoppingCartService.deleteById(cartId)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_CART_DELETE.getValue());
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
