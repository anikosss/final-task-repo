package by.epam.agency.command;

import by.epam.agency.command.impl.*;
import by.epam.agency.command.impl.admin.*;
import by.epam.agency.command.impl.user.*;

/**
 * <p>Enum that contains all command in project</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum CommandsEnum {
    EMPTY(new EmptyCommand()),
    MAIN_PAGE(new MainPageCommand()),
    LOGIN(new LoginCommand()),
    LOGOUT(new LogoutCommand()),
    REGISTRATION(new RegistrationCommand()),
    VIEW_CATALOG(new ViewCatalogCommand()),
    ADD_TO_CART(new AddToCartCommand()),
    DELETE_FROM_CART(new DeleteFromCartCommand()),
    SHOPPING_CART(new ShoppingCartCommand()),
    CHECKOUT_ORDER(new CheckoutOrderCommand()),
    USER_CONFIRM_ORDER(new UserConfirmOrderCommand()),
    ADMIN_CONFIRM_ORDER(new AdminConfirmOrderCommand()),
    DELETE_ORDER(new DeleteOrderCommand()),
    PROFILE(new ProfileCommand()),
    CANCEL_ORDER(new CancelOrderCommand()),
    COUNTRY_INFO(new CountryInfoCommand()),
    TOUR_INFO(new TourInfoCommand()),
    COUNTRY_CATALOG(new ViewCatalogCommand()),
    PLACE_CATALOG(new ViewCatalogCommand()),
    NEAREST_CATALOG(new ViewCatalogCommand()),
    LAST_CATALOG(new ViewCatalogCommand()),
    CHEAPEST_CATALOG(new ViewCatalogCommand()),
    DELETE_OLD_TOURS(new DeleteOldToursCommand()),
    CHANGE_LANGUAGE(new ChangeLanguageCommand()),
    ALL_TOURS(new AllToursCommand()),
    ALL_PLACES(new AllPlacesCommand()),
    ALL_HOTELS(new AllHotelsCommand()),
    ALL_ORDERS(new AllOrdersCommand()),
    CHOOSE_HOTEL_COUNTRY(new ChooseHotelCountryCommand()),
    CHOOSE_HOTEL_PLACE(new ChooseHotelPlaceCommand()),
    CHOOSE_TOUR_COUNTRY(new ChooseTourCountryCommand()),
    CHOOSE_TOUR_PLACE(new ChooseTourPlaceCommand()),
    ADD_TOUR(new AddTourCommand()),
    ADD_PLACE(new AddPlaceCommand()),
    ADD_HOTEL(new AddHotelCommand()),
    DELETE_TOUR(new DeleteTourCommand()),
    DELETE_PLACE(new DeletePlaceCommand()),
    DELETE_HOTEL(new DeleteHotelCommand()),
    TOUR_DETAILS(new TourDetailsCommand()),
    PLACE_DETAILS(new PlaceDetailsCommand()),
    HOTEL_DETAILS(new HotelDetailsCommand()),
    CHANGE_TOUR(new ChangeTourCommand()),
    CHANGE_PLACE(new ChangePlaceCommand()),
    CHANGE_HOTEL(new ChangeHotelCommand()),
    MAKE_TOUR_HOT(new MakeTourHotCommand()),
    REMOVE_SALE(new RemoveSaleCommand()),
    GO_TO_ADD_TOUR(new GoToAddTourCommand()),
    GO_TO_ADD_PLACE(new GoToAddPlaceCommand()),
    GO_TO_ADD_HOTEL(new GoToAddHotelCommand()),
    GO_TO_CHANGE_TOUR(new GoToChangeTourCommand()),
    GO_TO_CHANGE_PLACE(new GoToChangePlaceCommand()),
    GO_TO_CHANGE_HOTEL(new GoToChangeHotelCommand());

    ActionCommand command;

    CommandsEnum(ActionCommand command) {
        this.command = command;
    }

    public ActionCommand getCommand() {
        return this.command;
    }
}
