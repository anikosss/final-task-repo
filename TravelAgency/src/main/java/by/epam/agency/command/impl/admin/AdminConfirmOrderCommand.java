package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.validator.impl.admin.AdminConfirmOrderValidator;
import org.apache.log4j.Logger;

public class AdminConfirmOrderCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AdminConfirmOrderCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ORDER = "order";
    private static final String CONFIRMED = "confirmed";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private AdminConfirmOrderValidator validator = AdminConfirmOrderValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            Order order = OrderService.getOrderById(orderId);
            order.setStatus(CONFIRMED);
            if (OrderService.updateOrder(order)) {
                context.addRequestAttribute(SUCCESS_MESSAGE,
                        MessagesEnum.SUCCESS_ADMIN_CONFIRM_ORDER.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_ADMIN_CONFIRM_ORDER.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
