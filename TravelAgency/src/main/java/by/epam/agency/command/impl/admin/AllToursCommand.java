package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.comparator.NearestToursComparator;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Order;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.OrderService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AllToursCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllToursCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String TOURS = "tours";
    private static final String PAGE = "page";
    private static final int TOURS_PER_PAGE = 20;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<Tour> allTours = TourService.getAllTours();
            Collections.sort(allTours, new NearestToursComparator());
            List<Tour> tours = createTours(allTours, page, TOURS_PER_PAGE);
            calculatePlaces(tours);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(TOURS, tours);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_TOURS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Tour> createTours(List<Tour> allTours, int page, int count) {
        List<Tour> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allTours.size() - 1) {
                break;
            }
            result.add(allTours.get(i));
        }
        return result;
    }

    private int calculateOrderedPlaces(Tour tour, int hotelId) {
        int orderedPlaces = 0;
        List<Order> orders = OrderService.getByHotelId(hotelId);
        for (Order order : orders) {
            if (tour.getArriveDate().before(order.getTour().getReturnDate()) ||
                    tour.getReturnDate().after(order.getTour().getArriveDate())) {
                orderedPlaces++;
            }
        }
        return orderedPlaces;
    }

    private void calculatePlaces(List<Tour> tours) {
        int allPlaces;
        int freePlaces;
        for (Tour tour : tours) {
            allPlaces = 0;
            freePlaces = 0;
            for (Hotel hotel : tour.getPlace().getHotels()) {
                allPlaces += hotel.getAllRooms();
                freePlaces += hotel.getAllRooms() - calculateOrderedPlaces(tour, hotel.getId());
            }
            tour.setAllPlaces(allPlaces);
            tour.setFreePlaces(freePlaces);
        }
    }
}
