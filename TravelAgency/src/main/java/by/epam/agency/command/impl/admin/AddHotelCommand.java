package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.validator.impl.CountryValidator;
import by.epam.agency.validator.impl.admin.ChoosePlaceValidator;
import by.epam.agency.validator.impl.admin.HotelInfoValidator;
import org.apache.log4j.Logger;

public class AddHotelCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddHotelCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String PLACE = "place";
    private static final String COUNTRY = "country";
    private static final String TITLE = "title";
    private static final String STARS = "stars";
    private static final String ALL_ROOMS = "allRooms";
    private static final String MAIN_IMAGE = "mainImage";
    private static final String ROOM_IMAGE = "roomImage";
    private CountryValidator countryValidator = CountryValidator.getInstance();
    private ChoosePlaceValidator placeValidator = ChoosePlaceValidator.getInstance();
    private HotelInfoValidator hotelValidator = HotelInfoValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            result = validate(context);
            if (result != null) {
                return result;
            }
            String title = context.getRequestParameter(TITLE)[0];
            int stars = Integer.parseInt(context.getRequestParameter(STARS)[0]);
            int allRooms = Integer.parseInt(context.getRequestParameter(ALL_ROOMS)[0]);
            int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
            String mainImage = context.getRequestParameter(MAIN_IMAGE)[0];
            String roomImage = context.getRequestParameter(ROOM_IMAGE)[0];
            Hotel hotel = new Hotel(0, title, stars, allRooms, allRooms, mainImage, roomImage, placeId);
            if (!HotelService.addHotel(hotel)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_PLACE_ADD.getValue());
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_HOTEL_ADD.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_HOTELS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_HOTEL_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private String validate(RequestContext context) {
        MessagesEnum validateResult = countryValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            return PagesManager.getPage(PagesEnum.GO_TO_ADD_HOTEL);
        }
        validateResult = placeValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            String country = context.getRequestParameter(COUNTRY)[0];
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            context.addRequestAttribute(COUNTRY, country);
            return PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_HOTEL_PLACE);
        }
        validateResult = hotelValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            String country = context.getRequestParameter(COUNTRY)[0];
            String place = context.getRequestParameter(PLACE)[0];
            context.addRequestAttribute(COUNTRY, country);
            context.addRequestAttribute(PLACE, place);
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            return PagesManager.getPage(PagesEnum.ADMIN_WRITE_HOTEL_INFO);
        }
        return null;
    }
}
