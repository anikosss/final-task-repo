package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Country;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.CountryService;
import org.apache.log4j.Logger;

import java.util.List;

public class GoToAddTourCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToAddTourCommand.class);
    private static final String COUNTRIES = "countries";
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            List<Country> countries = CountryService.getAllCountries();
            context.addRequestAttribute(COUNTRIES, countries);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_TOUR_COUNTRY);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
