package by.epam.agency.command.impl.user;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.ShoppingCart;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.impl.User;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.service.ShoppingCartService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.user.AddToCartValidator;
import org.apache.log4j.Logger;

public class AddToCartCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddToCartCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String USER = "user";
    private static final String TOUR = "tour";
    private static final String HOTEL = "hotel";
    private static final String METHOD = "method";
    private AddToCartValidator validator = AddToCartValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                if (!validateResult.equals(MessagesEnum.ERROR_VALIDATE_EMPTY_TOUR) &&
                    !validateResult.equals(MessagesEnum.ERROR_VALIDATE_WRONG_TOUR)) {
                    int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
                    return String.format(PagesManager.getPage(PagesEnum.TOUR_INFO_FORMAT), tourId);
                }
                return PagesManager.getPage(PagesEnum.ERROR_404);
            }
            int tourId = Integer.parseInt(context.getRequestParameter(TOUR)[0]);
            Tour tour = TourService.getTourById(tourId);
            int hotelId = Integer.parseInt(context.getRequestParameter(HOTEL)[0]);
            Hotel hotel = HotelService.getHotelById(hotelId);
            User user = (User) context.getSessionAttribute(USER);
            ShoppingCart shoppingCart = new ShoppingCart(1, user, tour, hotel);
            if (ShoppingCartService.addShoppingCart(shoppingCart)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_ADD_TO_CART.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_CART_ADD.getValue());
            }
            result = String.format(PagesManager.getPage(PagesEnum.TOUR_INFO_FORMAT), tourId);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
