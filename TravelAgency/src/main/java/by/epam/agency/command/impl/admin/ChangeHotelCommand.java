package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.HotelService;
import by.epam.agency.validator.impl.admin.ChangeEntityValidator;
import org.apache.log4j.Logger;

public class ChangeHotelCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeHotelCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String HOTEL = "hotel";
    private static final String TITLE = "title";
    private static final String STARS = "stars";
    private static final String ALL_ROOMS = "allRooms";
    private static final String MAIN_IMAGE = "mainImage";
    private static final String ROOM_IMAGE = "roomImage";
    private static final String DEFAULT_IMAGE = "hotel-image-not-found.jpg";
    private ChangeEntityValidator validator = ChangeEntityValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return String.format(PagesManager.getPage(PagesEnum.FORMAT_HOTEL_DETAILS),
                        Integer.parseInt(context.getRequestParameter(HOTEL)[0]));
            }
            int hotelId = Integer.parseInt(context.getRequestParameter(HOTEL)[0]);
            Hotel hotel = HotelService.getHotelById(hotelId);
            changeHotel(context, hotel);
            if (HotelService.updateHotel(hotel)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_HOTEL_ADD);
            }
            result = String.format(PagesManager.getPage(PagesEnum.FORMAT_HOTEL_DETAILS), hotelId);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeHotel(RequestContext context, Hotel hotel) {
        String title = context.getRequestParameter(TITLE)[0];
        hotel.setTitle(title);
        int stars = Integer.parseInt(context.getRequestParameter(STARS)[0]);
        hotel.setStars(stars);
        int allRooms = Integer.parseInt(context.getRequestParameter(ALL_ROOMS)[0]);
        hotel.setAllRooms(allRooms);
        String mainImage = context.getRequestParameter(MAIN_IMAGE)[0];
        if (!mainImage.equals(DEFAULT_IMAGE)) {
            hotel.setMainImage(mainImage);
        }
        String roomImage = context.getRequestParameter(ROOM_IMAGE)[0];
        if (!roomImage.equals(DEFAULT_IMAGE)) {
            hotel.setRoomImage(roomImage);
        }
    }
}
