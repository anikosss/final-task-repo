package by.epam.agency.command.impl.admin;

import by.epam.agency.command.ActionCommand;
import by.epam.agency.controller.RequestContext;
import by.epam.agency.controller.util.MethodsEnum;
import by.epam.agency.entity.impl.Hotel;
import by.epam.agency.entity.impl.Place;
import by.epam.agency.entity.impl.Tour;
import by.epam.agency.entity.util.SeatsClassesEnum;
import by.epam.agency.manager.MessagesEnum;
import by.epam.agency.manager.MessagesManager;
import by.epam.agency.manager.PagesEnum;
import by.epam.agency.manager.PagesManager;
import by.epam.agency.service.PlaceService;
import by.epam.agency.service.TourService;
import by.epam.agency.validator.impl.CountryValidator;
import by.epam.agency.validator.impl.admin.ChoosePlaceValidator;
import by.epam.agency.validator.impl.admin.TourInfoValidator;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class AddTourCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddTourCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String COUNTRY = "country";
    private static final String PLACE = "place";
    private static final String ARRIVE_DATE = "arriveDate";
    private static final String ARRIVE_TIME = "arriveTime";
    private static final String RETURN_DATE = "returnDate";
    private static final String RETURN_TIME = "returnTime";
    private static final String PRICE = "price";
    private static final String SEATS_CLASS = "seatsClass";
    private static final String SEATS_CLASSES = "seatsClasses";
    private static final String DEFAULT = "default";
    private TourInfoValidator tourValidator = TourInfoValidator.getInstance();
    private CountryValidator countryValidator = CountryValidator.getInstance();
    private ChoosePlaceValidator placeValidator = ChoosePlaceValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            result = validate(context);
            if (result != null) {
                return result;
            }
            int placeId = Integer.parseInt(context.getRequestParameter(PLACE)[0]);
            Place place = PlaceService.getPlaceById(placeId);
            String arriveTimeStr = context.getRequestParameter(ARRIVE_TIME)[0] + ":00";
            String returnTimeStr = context.getRequestParameter(RETURN_TIME)[0] + ":00";
            Date arriveDate = Date.valueOf(context.getRequestParameter(ARRIVE_DATE)[0]);
            Time arriveTime = Time.valueOf(arriveTimeStr);
            Date returnDate = Date.valueOf(context.getRequestParameter(RETURN_DATE)[0]);
            Time returnTime = Time.valueOf(returnTimeStr);
            int allPlaces = 0;
            for (Hotel hotel : place.getHotels()) {
                allPlaces += hotel.getAllRooms();
            }
            double price = Double.parseDouble(context.getRequestParameter(PRICE)[0]);
            SeatsClassesEnum seatsClass =
                    SeatsClassesEnum.valueOf(context.getRequestParameter(SEATS_CLASS)[0].toUpperCase());
            String status = DEFAULT;
            int sale = 0;
            Tour tour = new Tour(0, arriveDate, arriveTime, returnDate, returnTime, allPlaces, price, price,
                    seatsClass, status, sale, place);
            if (!TourService.addTour(tour)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_TOUR_ADD.getValue());
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_TOUR_ADD.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_TOURS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_TOUR_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private String validate(RequestContext context) {
        MessagesEnum validateResult = countryValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            return PagesManager.getPage(PagesEnum.GO_TO_ADD_TOUR);
        }
        validateResult = placeValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            String country = context.getRequestParameter(COUNTRY)[0];
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            context.addRequestAttribute(COUNTRY, country);
            return PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_TOUR_PLACE);
        }
        validateResult = tourValidator.validate(context);
        if (!validateResult.equals(MessagesEnum.SUCCESS)) {
            String country = context.getRequestParameter(COUNTRY)[0];
            String place = context.getRequestParameter(PLACE)[0];
            context.addRequestAttribute(COUNTRY, country);
            context.addRequestAttribute(PLACE, place);
            context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            List<String> seatsClasses = new ArrayList<>();
            for (SeatsClassesEnum seatsClass : SeatsClassesEnum.values()) {
                seatsClasses.add(seatsClass.getValue());
            }
            context.addRequestAttribute(SEATS_CLASSES, seatsClasses);
            return PagesManager.getPage(PagesEnum.ADMIN_WRITE_TOUR_INFO);
        }
        return null;
    }
}
