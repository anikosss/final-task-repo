package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;

import java.sql.Date;
import java.time.LocalDate;

/**
 * <p>Class for the order-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Order implements IEntity {
    private int id;
    private String name;
    private String phone;
    private double price;
    private String tourStatus;
    private Date orderDate;
    private Tour tour;
    private User user;
    private Hotel hotel;
    private String status = "new";

    public Order() {}

    public Order(int id, String name, String phone, double price, String tourStatus, Date orderDate, Tour tour,
                 User user, Hotel hotel, String status) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.price = price;
        this.tourStatus = tourStatus;
        this.orderDate = orderDate;
        this.tour = tour;
        this.user = user;
        this.hotel = hotel;
        this.status = status;
    }

    public Order(int id, String name, String phone, Tour tour, User user, Hotel hotel) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.price = tour.getPrice();
        this.tourStatus = tour.getStatus();
        this.orderDate = Date.valueOf(LocalDate.now());
        this.tour = tour;
        this.user = user;
        this.hotel = hotel;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        Order other = (Order) obj;
        if (id != other.id) {
            return false;
        }
        if (!name.equals(other.name)) {
            return false;
        }
        if (!phone.equals(other.phone)) {
            return false;
        }
        if (Double.compare(price, other.price) != 0) {
            return false;
        }
        if (!tourStatus.equals(other.tourStatus)) {
            return false;
        }
        if (!orderDate.equals(other.orderDate)) {
            return false;
        }
        if (!tour.equals(other.tour)) {
            return false;
        }
        if (!user.equals(other.user)) {
            return false;
        }
        if (!hotel.equals(other.hotel)) {
            return false;
        }
        return status.equals(other.status);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + orderDate.hashCode();
        result = 31 * result + tour.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + hotel.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTourStatus() {
        return tourStatus;
    }

    public void setTourStatus(String tourStatus) {
        this.tourStatus = tourStatus;
    }
}
