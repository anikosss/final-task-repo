package by.epam.agency.entity;

/**
 * <p>Interface that unit all entities in the project</p>
 *
 * @author Dmitry Anikeichenko
 */
public interface IEntity {
    int getId();
}
