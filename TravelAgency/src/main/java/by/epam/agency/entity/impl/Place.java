package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;

import java.util.List;

/**
 * <p>Class for the place-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Place implements IEntity {
    private int id;
    private String title;
    private String image = "place-image-not-found.jpg";
    private Country country;
    private List<Hotel> hotels;

    public Place() {}

    public Place(int id, String title, String image, Country country, List<Hotel> hotels) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.country = country;
        this.hotels = hotels;
    }

    public Place(int id, String title, Country country, List<Hotel> hotels) {
        this.id = id;
        this.title = title;
        this.country = country;
        this.hotels = hotels;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (hotels != null ? hotels.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        Place other = (Place) obj;
        if (id != other.id) {
            return false;
        }
        if (!title.equals(other.title)) {
            return false;
        }
        if (!image.equals(other.image)) {
            return false;
        }
        if (!country.equals(other.country)) {
            return false;
        }
        return hotels.equals(other.hotels);

    }

}
