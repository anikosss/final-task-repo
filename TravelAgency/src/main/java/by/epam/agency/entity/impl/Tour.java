package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;
import by.epam.agency.entity.util.SeatsClassesEnum;

import java.sql.Date;
import java.sql.Time;

/**
 * <p>Class for the tour-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Tour implements IEntity {
    private int id;
    private Date arriveDate;
    private Time arriveTime;
    private Date returnDate;
    private Time returnTime;
    private int allPlaces;
    private int freePlaces;
    private double price;
    private double defaultPrice;
    private SeatsClassesEnum seatsClass;
    private String status = "default";
    private int sale = 0;
    private Place place;

    public Tour() {}

    public Tour(int id, Date arriveDate, Time arriveTime, Date returnDate, Time returnTime, int allPlaces,
                double price, double defaultPrice, SeatsClassesEnum seatsClass, String status, int sale, Place place) {
        this.id = id;
        this.arriveDate = arriveDate;
        this.arriveTime = arriveTime;
        this.returnDate = returnDate;
        this.returnTime = returnTime;
        this.allPlaces = allPlaces;
        this.freePlaces = allPlaces;
        this.price = price;
        this.defaultPrice = defaultPrice;
        this.seatsClass = seatsClass;
        this.status = status;
        this.sale = sale;
        this.place = place;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public SeatsClassesEnum getSeatsClass() {
        return seatsClass;
    }

    public void setSeatsClass(SeatsClassesEnum seatsClass) {
        this.seatsClass = seatsClass;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Time getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(Time arriveTime) {
        this.arriveTime = arriveTime;
    }

    public Time getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Time returnTime) {
        this.returnTime = returnTime;
    }

    public int getAllPlaces() {
        return allPlaces;
    }

    public void setAllPlaces(int allPlaces) {
        this.allPlaces = allPlaces;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (arriveDate != null ? arriveDate.hashCode() : 0);
        result = 31 * result + (arriveTime != null ? arriveTime.hashCode() : 0);
        result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
        result = 31 * result + (returnTime != null ? returnTime.hashCode() : 0);
        result = 31 * result + allPlaces;
        result = 31 * result + freePlaces;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(defaultPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (seatsClass != null ? seatsClass.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + sale;
        result = 31 * result + (place != null ? place.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        Tour other = (Tour) obj;
        if (id != other.id) {
            return false;
        }
        if (!arriveDate.equals(other.arriveDate)) {
            return false;
        }
        if (!arriveTime.equals(other.arriveTime)) {
            return false;
        }
        if (!returnDate.equals(other.returnDate)) {
            return false;
        }
        if (!returnTime.equals(other.returnTime)) {
            return false;
        }
        if (allPlaces != other.allPlaces) {
            return false;
        }
        if (Double.compare(other.price, price) != 0) {
            return false;
        }
        if (Double.compare(other.defaultPrice, defaultPrice) != 0) {
            return false;
        }
        if (seatsClass != other.seatsClass) {
            return false;
        }
        if (!status.equals(other.status)) {
            return false;
        }
        if (sale != other.sale) {
            return false;
        }
        return place.equals(other.place);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }
}
