package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;

/**
 * <p>Class for the hotel-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Hotel implements IEntity {
    private int id;
    private String title;
    private int stars;
    private int allRooms;
    private int freeRooms;
    private String mainImage = "image-not-found.jpg";
    private String roomImage = "image-not-found.jpg";
    private int placeId;

    public Hotel() {}

    public Hotel(int id, String title, int stars, int allRooms, int freeRooms, String mainImage,
                  String roomImage, int placeId) {
        this.id = id;
        this.title = title;
        this.stars = stars;
        this.allRooms = allRooms;
        this.freeRooms = freeRooms;
        this.mainImage = mainImage;
        this.roomImage = roomImage;
        this.placeId = placeId;
    }

    public Hotel(int id, String title, int stars, int allRooms, int placeId) {
        this.id = id;
        this.title = title;
        this.stars = stars;
        this.allRooms = allRooms;
        this.placeId = placeId;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getAllRooms() {
        return allRooms;
    }

    public void setAllRooms(int allRooms) {
        this.allRooms = allRooms;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(String roomImage) {
        this.roomImage = roomImage;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + stars;
        result = 31 * result + allRooms;
        result = 31 * result + freeRooms;
        result = 31 * result + (mainImage != null ? mainImage.hashCode() : 0);
        result = 31 * result + (roomImage != null ? roomImage.hashCode() : 0);
        result = 31 * result + placeId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !this.getClass().equals(obj.getClass())) {
            return false;
        }
        Hotel other = (Hotel) obj;
        if (other.getId() != this.id) {
            return false;
        }
        if (!other.getTitle().equals(this.title)) {
            return false;
        }
        if (other.getStars() != this.stars) {
            return false;
        }
        if (other.getAllRooms() != this.allRooms) {
            return false;
        }
        if (!other.getMainImage().equals(this.mainImage)) {
            return false;
        }
        if (!other.getRoomImage().equals(this.roomImage)) {
            return false;
        }
        if (other.getPlaceId() != this.placeId) {
            return false;
        }
        return true;
    }

    public int getFreeRooms() {
        return freeRooms;
    }

    public void setFreeRooms(int freeRooms) {
        this.freeRooms = freeRooms;
    }
}
