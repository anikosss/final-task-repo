package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;

/**
 * <p>Class for the shopping-cart-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class ShoppingCart implements IEntity {
    private int id;
    private User user;
    private Tour tour;
    private Hotel hotel;

    public ShoppingCart() {}

    public ShoppingCart(int id, User user, Tour tour, Hotel hotel) {
        this.id = id;
        this.user = user;
        this.tour = tour;
        this.hotel = hotel;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (tour != null ? tour.hashCode() : 0);
        result = 31 * result + (hotel != null ? hotel.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        ShoppingCart other = (ShoppingCart) obj;
        if (id != other.id) {
            return false;
        }
        if (!user.equals(other.user)) {
            return false;
        }
        if (!tour.equals(other.tour)) {
            return false;
        }
        return hotel.equals(other.hotel);

    }

}
