package by.epam.agency.entity.util;

/**
 * <p>Interface that describes types of users in system</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum RolesEnum {
    USER("user"),
    ADMIN("admin");

    String value;

    RolesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
