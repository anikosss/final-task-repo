package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;
import by.epam.agency.entity.util.RolesEnum;

/**
 * <p>Class for the user-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class User implements IEntity {
    private int id;
    private String login;
    private String password;
    private String email;
    private RolesEnum role;

    public User() {}

    public User(int id, String login, String password, String email, RolesEnum role) {
        this.setId(id);
        this.setLogin(login);
        this.setPassword(password);
        this.setEmail(email);
        this.setRole(role);
    }

    @Override
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RolesEnum getRole() {
        return role;
    }

    public void setRole(RolesEnum role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        User other = (User) obj;
        if (id != other.id) {
            return false;
        }
        if (!login.equals(other.login)) {
            return false;
        }
        if (!password.equals(other.password)) {
            return false;
        }
        if (!email.equals(other.email)) {
            return false;
        }
        return role == other.role;
    }

}
