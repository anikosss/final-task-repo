package by.epam.agency.entity.impl;

import by.epam.agency.entity.IEntity;

/**
 * <p>Class for the country-entity</p>
 *
 * @author Dmitry Anikeichenko
 */
public class Country implements IEntity {
    private int id;
    private String name;
    private String description;
    private String image = "image-not-found.jpg";

    public Country() {}

    public Country(int id, String name, String description, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public Country(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !obj.getClass().equals(this.getClass())) {
            return false;
        }
        Country other = (Country) obj;
        if (id != other.id) {
            return false;
        }
        if (!name.equals(other.name)) {
            return false;
        }
        if (!description.equals(other.description)) {
            return false;
        }
        return image.equals(other.image);
    }

}
