package by.epam.agency.entity.util;

/**
 * <p>Interface that describes types of seats classes in tours</p>
 *
 * @author Dmitry Anikeichenko
 */
public enum SeatsClassesEnum {
    ECONOM("econom"),
    BUSINESS("business");

    String value;

    SeatsClassesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
