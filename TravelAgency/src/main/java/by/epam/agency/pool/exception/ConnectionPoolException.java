package by.epam.agency.pool.exception;

/**
 * <p>Exception throws when error occurred while working with {@link by.epam.agency.pool.PoolConnection}</p>
 *
 * @author Dmitry Anikeichenko
 */
public class ConnectionPoolException extends Exception {

    public ConnectionPoolException() {
        super();
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

}
