package by.epam.agency.pool;

import by.epam.agency.manager.*;
import by.epam.agency.pool.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * <p>Class that contains the database connections</p>
 *
 * @author Dmitry Anikeychenko
 */
public class ConnectionsPool {
    private static final Logger LOGGER = Logger.getLogger(ConnectionsPool.class);
    private int size;
    private String url;
    private String driver;
    private String user;
    private String password;
    private BlockingQueue<PoolConnection> freeConnections;
    private List<PoolConnection> allConnections = new ArrayList<>();

    private static class InstanceHolder {
        private static final ConnectionsPool INSTANCE = new ConnectionsPool();
    }

    private ConnectionsPool() {}

    public static ConnectionsPool getInstance() {
        return InstanceHolder.INSTANCE;
    }

    /**
     * Initializing the connections with params from configuration
     */
    public void init() {
        try {
            size = Integer.parseInt(ConfigManager.getParam(ConfigEnum.DB_SIZE));
            url = ConfigManager.getParam(ConfigEnum.DB_URL);
            driver = ConfigManager.getParam(ConfigEnum.DB_DRIVER);
            user = ConfigManager.getParam(ConfigEnum.DB_USER);
            password = ConfigManager.getParam(ConfigEnum.DB_PASSWORD);
            Class.forName(driver);
            freeConnections = new ArrayBlockingQueue<>(size);
            for (int i = 0; i < size; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PoolConnection poolConnection = new PoolConnection(connection);
                allConnections.add(poolConnection);
                freeConnections.add(poolConnection);
            }
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_POOL_INIT));
        } catch (ClassNotFoundException e) {
            LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_DRIVER_LOAD_FAILED), e);
        } catch (SQLException e) {
            LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_POOL_INIT), e);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }


    /**
     * <p>Method for getting connection from the pool</p>
     *
     * @return {@link PoolConnection}
     */
    public PoolConnection getConnection() {
        PoolConnection connection = null;
        try {
            connection = freeConnections.poll(10, TimeUnit.SECONDS);
            if (connection == null) {
                throw new ConnectionPoolException();
            }
        } catch (ConnectionPoolException e) {
             LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_POOL_TIMEOUT), e);
        } catch (InterruptedException e) {
            LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_POOL_GET_CONNECTION), e);
        }
        return connection;
    }


    /**
     * <p>Method for returning connection to the pool</p>
     *
     * @param connection Connection that will be returning to the pool
     * @return true if connection is successfully returned, false otherwise
     */
    public boolean returnConnection(PoolConnection connection) {
        return freeConnections.offer(connection);
    }


    /**
     * <p>Method for destroying pool, e.g. closing all connections</p>
     */
    public void destroy() {
        try {
            for (PoolConnection poolConnection : allConnections) {
                poolConnection.dispose();
            }
            allConnections.clear();
            freeConnections.clear();
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_POOL_DESTROY));
        } catch (SQLException e) {
            LOGGER.error(ErrorsManager.getMessage(ErrorsEnum.DB_POOL_DESTROY), e);
        }
    }

    public int getFreeConnectionsCount() {
        return freeConnections.size();
    }

    public int getConnectionsCount() {
        return allConnections.size();
    }
}
