package by.epam.agency.pool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>The wrapper for {@link Connection} that implements {@link AutoCloseable} for using
 * try-with-resources</p>
 *
 * @author Dmitry Anikeichenko
 */
public class PoolConnection implements AutoCloseable {
    private Connection connection;

    public PoolConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * <p>Method for closing {@link Connection}</p>
     *
     * @throws SQLException
     */
    public void dispose() throws SQLException {
        if (connection != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            connection.close();
        }
    }

    public Connection getConnection() {
        return this.connection;
    }


    /**
     * <p>Method that returns connection to the pool instead of close</p>
     */
    @Override
    public void close() {
        ConnectionsPool.getInstance().returnConnection(this);
    }

}
