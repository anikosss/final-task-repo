<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.catalog" bundle="${content}"/> </title>
</head>
<body>
<jsp:useBean id="country" class="by.epam.agency.entity.impl.Country" scope="request"/>
<jsp:useBean id="place" class="by.epam.agency.entity.impl.Place" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <c:choose>
                        <c:when test="${country ne null and not empty country.name}">
                            <c:choose>
                                <c:when test="${action eq 'nearest_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=nearest_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:when test="${action eq 'last_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=last_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:when test="${action eq 'cheapest_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=cheapest_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=country_catalog&country=${country.id}&page=${page}">
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:when test="${place ne null and not empty place.title}">
                            <input type="hidden" name="requestPage" value="controller?action=place_catalog&place=${place.id}&page=${page}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="requestPage" value="controller?action=view_catalog&page=${page}">
                        </c:otherwise>
                    </c:choose>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <c:choose>
                        <c:when test="${country ne null and not empty country.name}">
                            <c:choose>
                                <c:when test="${action eq 'nearest_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=nearest_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:when test="${action eq 'last_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=last_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:when test="${action eq 'cheapest_catalog'}">
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=cheapest_catalog&country=${country.id}&page=${page}">
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" name="requestPage"
                                           value="controller?action=country_catalog&country=${country.id}&page=${page}">
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:when test="${place ne null and not empty place.title}">
                            <input type="hidden" name="requestPage" value="controller?action=place_catalog&place=${place.id}&page=${page}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="requestPage" value="controller?action=view_catalog&page=${page}">
                        </c:otherwise>
                    </c:choose>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
    <br><br><br>
    <div class="jumbotron">
        <h1><fmt:message key="content.catalog.header" bundle="${content}" /></h1>
        <c:if test="${country ne null and not empty country.name}">
            <h2><fmt:message key="content.catalog.for.country" bundle="${content}" />
                <fmt:message key="country.name.${fn:toLowerCase(country.name)}" bundle="${info}" /></h2>
            <c:if test="${action eq 'nearest_catalog'}">
                <h3><fmt:message key="content.nearest.tours" bundle="${content}" /></h3>
            </c:if>
            <c:if test="${action eq 'last_catalog'}">
                <h3><fmt:message key="content.menu.last.added.tours" bundle="${content}" /></h3>
            </c:if>
            <c:if test="${action eq 'cheapest_catalog'}">
                <h3><fmt:message key="content.menu.cheapest.tours" bundle="${content}" /></h3>
            </c:if>
        </c:if>
        <c:if test="${place ne null and not empty place.title}">
            <h2><fmt:message key="content.catalog.for.place" bundle="${content}" /> ${place.title}</h2>
        </c:if>
    </div>
    <hr>
    <div class="row row-catalog">
        <c:choose>
            <c:when test="${tours eq null or empty tours}">
                <div class="col-md-4 col-md-offset-4">
                    <div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><fmt:message key="content.alert.info" bundle="${content}" />.</strong>
                        <fmt:message key="content.catalog.no.tours" bundle="${content}" />
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <c:forEach items="${tours}" var="elem">
                    <a href="controller?action=tour_info&tour=${elem.id}" style="text-decoration: none;">
                        <div class="col-md-3 tour text-center">
                            <c:if test="${elem.status eq 'hot'}">
                                <span class="hot pull-right">
                                    <span class="glyphicon glyphicon-fire"></span>
                                    &nbsp; <fmt:message key="content.hot"  bundle="${content}" />
                                </span>
                            </c:if>
                            <img src="images/places/${elem.place.image}" alt="Place image">
                            <h3>${elem.place.title}</h3>
                            <h4><a href="controller?action=country_info&country=${elem.place.country.id}">
                                <fmt:message key="country.name.${fn:toLowerCase(elem.place.country.name)}" bundle="${info}" /></a></h4>
                            <h6><i style="color: #044a43;">
                                <fmt:formatDate pattern="dd.MM.yyyy" value="${elem.arriveDate}"/></i></h6>
                            <c:choose>
                                <c:when test="${elem.status eq 'hot'}">
                                    <span><del>${elem.defaultPrice} $</del></span><h4><i class="hot">${ elem.price} $</i></h4>
                                </c:when>
                                <c:otherwise>
                                    <h4><i>${elem.price}$</i></h4>
                                </c:otherwise>
                            </c:choose>
                            <p class="text-center">
                                <a href="controller?action=tour_info&tour=${elem.id}">
                                    <button class="btn form-btn">
                                        <fmt:message key="content.button.details" bundle="${content}" />
                                    </button>
                                </a>
                            </p>
                        </div>
                    </a>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </div>
    <c:if test="${not empty pagesCount and pagesCount gt 1}">
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg ">
                    <c:forEach begin="1" end="${pagesCount}" var="num">
                        <li <c:if test="${page eq num}">class="active"</c:if>>
                        <c:choose>
                            <c:when test="${country ne null and not empty country.name}">
                                <c:choose>
                                    <c:when test="${action eq 'nearest_catalog'}">
                                        <a href="controller?action=nearest_catalog&country=${country.id}&page=${num}">${num}</a></li>
                                    </c:when>
                                    <c:when test="${action eq 'last_catalog'}">
                                        <a href="controller?action=last_catalog&country=${country.id}&page=${num}">${num}</a></li>
                                    </c:when>
                                    <c:when test="${action eq 'cheapest_catalog'}">
                                        <a href="controller?action=cheapest_catalog&country=${country.id}&page=${num}">${num}</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="controller?action=country_catalog&country=${country.id}&page=${num}">${num}</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${place ne null and not empty place.title}">
                                <a href="controller?action=place_catalog&place=${place.id}&page=${num}">${num}</a></li>
                            </c:when>
                            <c:otherwise>
                                <a href="controller?action=view_catalog&page=${num}">${num}</a></li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>
</div>
<br><br><br><br>
<br><br><br><br>
<jsp:include page="common/footer.jsp" />
</body>
</html>