<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ft" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.shopping.cart" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=shopping_cart">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=shopping_cart">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <br><br><br><br>
    <h1><fmt:message key="content.shopping.cart" bundle="${content}" /> ${user.login}</h1>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th class="info"><fmt:message key="content.table.country" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.place" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.arrive.date" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.return.date" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.hotel" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.price" bundle="${content}" /></th>
                    <th class="info"></th>
                    <th class="info"></th>
                </tr>
                <c:forEach items="${shoppingCart}" var="elem">
                    <tr>
                        <td><a href="#country-modal-${elem.id}" data-toggle="modal">
                            <fmt:message key="country.name.${fn:toLowerCase(elem.tour.place.country.name)}" bundle="${info}" />
                        </a></td>
                        <td><a href="#place-modal-${elem.id}" data-toggle="modal">${elem.tour.place.title}</a></td>
                        <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.arriveDate}"/>
                            / ${elem.tour.arriveTime}</td>
                        <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.returnDate}"/>
                            / ${elem.tour.returnTime}</td>
                        <td><a href="#hotel-modal-${elem.id}" data-toggle="modal">${elem.hotel.title}</a></td>
                        <td>${elem.tour.price} <c:if test="${elem.tour.status eq 'hot'}">
                            <span class="bg-hot"><span class="glyphicon glyphicon-fire"></span></span>
                        </c:if></td>
                        <td>
                            <form action="controller" method="POST" accept-charset="UTF-8">
                                <button type="submit" class="btn form-btn">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                    &nbsp; <fmt:message key="content.button.checkout" bundle="${content}" />
                                </button>
                                <input type="hidden" name="action" value="checkout_order">
                                <input type="hidden" name="cart" value="${elem.id}">
                            </form>
                        </td>
                        <td>
                            <form action="controller" method="POST" accept-charset="UTF-8">
                                <button type="submit" class="btn form-btn form-btn-red">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    &nbsp; <fmt:message key="content.button.delete" bundle="${content}" />
                                </button>
                                <input type="hidden" name="action" value="delete_from_cart">
                                <input type="hidden" name="cart" value="${elem.id}">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <c:forEach items="${shoppingCart}" var="elem">
                <div id="country-modal-${elem.id}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">
                                    <fmt:message key="content.modal.info.about" bundle="${content}" />
                                    <fmt:message key="country.name.${fn:toLowerCase(elem.tour.place.country.name)}"
                                                 bundle="${info}" />:</h3>
                            </div>
                            <div class="modal-body">
                                <img src="images/media-${fn:toLowerCase(elem.tour.place.country.name)}-700.jpg"
                                     alt="Country image"
                                     class="img img-responsive" width="100%">
                                <br>
                                <c:set var="countryDescrirption" scope="page"><fmt:message
                                        key="${elem.tour.place.country.description}" bundle="${info}"/> </c:set>
                                <p class="media-text">
                                    <c:forTokens items="${countryDescrirption}" delims=" " end="35" var="descr">
                                        ${descr}
                                    </c:forTokens>...
                                </p>
                            </div>
                            <div class="modal-footer">
                                <a class="pull-left btn form-btn"
                                   href="controller?action=country_info&country=${elem.tour.place.country.id}">
                                    <fmt:message key="content.button.view.more" bundle="${content}" />
                                </a>
                                <a href="controller?action=country_catalog&country=${elem.tour.place.country.id}&page=1"
                                   class="pull-right btn form-btn">
                                    <fmt:message key="content.button.view.tours" bundle="${content}" />
                                    &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="place-modal-${elem.id}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">
                                    <fmt:message key="content.modal.info.about" bundle="${content}" />
                                        ${elem.tour.place.title}:</h3>
                            </div>
                            <div class="modal-body">
                                <img src="images/places/${elem.tour.place.image}" alt="Place image"
                                     class="img img-responsive" width="100%">
                                <br>
                            </div>
                            <div class="modal-footer">
                                <a class="pull-left btn form-btn"
                                   href="controller?action=place_catalog&place=${elem.tour.place.id}&page=1">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    &nbsp; <fmt:message key="content.button.view.tours" bundle="${content}" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="hotel-modal-${elem.id}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">
                                    <fmt:message key="content.modal.info.about" bundle="${content}" />
                                        ${elem.hotel.title}:</h3>
                            </div>
                            <div class="modal-body">
                                <h3><fmt:message key="content.modal.main.image" bundle="${content}" />:</h3>
                                <img src="images/hotels/${elem.hotel.mainImage}" alt="Hotel image"
                                     class="img img-responsive" width="100%">
                                <br>
                                <h3><fmt:message key="content.modal.room.image" bundle="${content}" />:</h3>
                                <img src="images/hotels/${elem.hotel.roomImage}" alt="Room image"
                                     class="img img-responsive">
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <strong><fmt:message key="content.modal.stars" bundle="${content}" />:</strong>
                                        </div>
                                        <div class="col-md-6">
                                                ${elem.hotel.stars}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <strong><fmt:message key="content.modal.all.rooms" bundle="${content}" />:</strong>
                                        </div>
                                        <div class="col-md-6">
                                                ${elem.hotel.allRooms}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <c:if test="${empty shoppingCart}">
                <h4 class="bg-warning">
                    <fmt:message key="content.empty.shopping.cart" bundle="${content}" />
                </h4>
            </c:if>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
