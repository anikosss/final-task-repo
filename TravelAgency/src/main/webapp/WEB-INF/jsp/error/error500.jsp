<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title><fmt:message key="content.title.error.500" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=error_500">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=error_500">
                </div>
            </form>
        </div>
    </div>
</div>
<jsp:useBean id="user" class="by.epam.agency.entity.impl.User" scope="session"/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>500</h1>
                <h4><p><fmt:message key="content.server.error" bundle="${content}" /></p></h4>
                <a href="/controller?action=main_page"><button type="button" class="btn form-btn">
                    <fmt:message key="content.button.go.to.main" bundle="${content}" />
                     <span class="glyphicon glyphicon-circle-arrow-right"></span>
                </button></a>
            </div>
            <c:if test="${user.role.value ne null and user.role.value eq 'admin'}">
                <table class="table">
                    <tr>
                        <th><fmt:message key="content.table.request.from" bundle="${content}" />:</th>
                        <td>${pageContext.errorData.requestURI}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.exception" bundle="${content}" />:</th>
                        <td>${pageContext.exception}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.stack.trace" bundle="${content}" />:</th>
                        <td>
                            <c:forEach items="${pageContext.exception.stackTrace}" var="trace" >
                                <small>${trace}</small>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
