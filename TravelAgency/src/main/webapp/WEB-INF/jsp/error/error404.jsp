<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.error.404" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=error_404">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=error_404">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="jumbotron" style="background-color: white;">
            <div class="col-md-12 center-block">
                <h1><fmt:message key="content.page.not.found" bundle="${content}" /></h1>
            </div>
        </div>
    </div>
    <br> <br>
    <div class="row">
        <div class="col-md-4">
            <img src="images/404.png" class="img-responsive">
        </div>
        <div class="col-md-8">
            <h1><fmt:message key="content.venn.diagram" bundle="${content}" /></h1>
            <p><fmt:message key="content.venn.description" bundle="${content}" /></p>
            <hr>
            <a href="controller?action=main_page" class="btn form-btn">
                <fmt:message key="content.button.go.to.main" bundle="${content}" />
                 <span class="glyphicon glyphicon-circle-arrow-right"></span>
            </a> <br> <br>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
