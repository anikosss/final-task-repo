<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.checkout" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="order" class="by.epam.agency.entity.impl.ShoppingCart" scope="request" />
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=shopping_cart">
                    <button type="button" class="btn form-btn" data-dismiss="modal">
                        <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=shopping_cart">
                    <button type="button" class="btn form-btn" data-dismiss="modal">
                        <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <br><br><br><br>
    <h1><fmt:message key="content.checkout.order" bundle="${content}" /></h1>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <th class="info"><fmt:message key="content.table.country" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.place" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.arrive.date" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.return.date" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.hotel" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.price" bundle="${content}" /></th>
                </tr>
                <tr>
                    <td><a href="#country-modal" data-toggle="modal">
                        <fmt:message key="country.name.${fn:toLowerCase(order.tour.place.country.name)}" bundle="${info}" />
                    </a></td>
                    <td><a href="#place-modal" data-toggle="modal">${order.tour.place.title}</a></td>
                    <td><fmt:formatDate pattern="dd.MM.yyyy" value="${order.tour.arriveDate}"/>
                        / ${order.tour.arriveTime}</td>
                    <td><fmt:formatDate pattern="dd.MM.yyyy" value="${order.tour.returnDate}"/>
                        / ${order.tour.returnTime}</td>
                    <td><a href="#hotel-modal" data-toggle="modal">${order.hotel.title}</a></td>
                    <td>${order.tour.price} <c:if test="${order.tour.status eq 'hot'}">
                        <span class="bg-hot"><span class="glyphicon glyphicon-fire"></span></span>
                    </c:if></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h2><fmt:message key="content.checkout.write.info" bundle="${content}" />:</h2>
            <br>
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="form-group">
                    <c:set var="enterName" scope="page"><fmt:message key="content.form.enter.name" bundle="${content}" /></c:set>
                    <label for="name"><fmt:message key="content.form.name" bundle="${content}" />:</label>
                    <input type="text" name="name" class="form-control form-input" id="name"
                           placeholder="${enterName}" pattern="[a-zA-Zа-яА-Я\s']{3,80}" required>
                    <span class="comment" data-toggle="tooltip" title="Your real name">
                        <fmt:message key="content.form.example.name" bundle="${content}" />
                    </span>
                </div>
                <div class="form-group">
                    <c:set var="enterPhone" scope="page"><fmt:message key="content.form.enter.phone" bundle="${content}" /></c:set>
                    <label for="phone"><fmt:message key="content.form.phone" bundle="${content}" />:</label>
                    <input type="text" name="phone" class="form-control form-input" id="phone" placeholder="${enterPhone}"
                           pattern="(\+[\d]{3})(\([\d]{2}\))([\d]{3})\-([\d]{2})\-([\d]{2})" required>
                    <span class="comment" data-toggle="tooltip" title="Your mobile phone">
                        <fmt:message key="content.form.example.phone" bundle="${content}" />
                    </span>
                </div>
                <br>
                <button type="submit" class="btn form-btn">
                    <span class="glyphicon glyphicon-ok"></span>
                    &nbsp; <fmt:message key="content.button.confirm" bundle="${content}" />
                </button>
                <input type="hidden" name="action" value="user_confirm_order">
                <input type="hidden" name="cart" value="${order.id}">
            </form>
        </div>
    </div>

    <div id="country-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">
                        <fmt:message key="content.modal.info.about" bundle="${content}" />
                        <fmt:message key="country.name.${fn:toLowerCase(order.tour.place.country.name)}"
                                                bundle="${info}" />:</h3>
                </div>
                <div class="modal-body">
                    <img src="images/media-${fn:toLowerCase(order.tour.place.country.name)}-700.jpg"
                         alt="Country image"
                         class="img img-responsive" width="100%">
                    <br>
                    <c:set var="countryDescrirption" scope="page"><fmt:message
                            key="${order.tour.place.country.description}" bundle="${info}"/> </c:set>
                    <c:forTokens items="${countryDescrirption}" delims=" " end="35" var="descr">
                        ${descr}
                    </c:forTokens>...
                </div>
                <div class="modal-footer">
                    <a class="pull-left btn form-btn"
                       href="controller?action=country_info&country=${order.tour.place.country.id}">
                        <fmt:message key="content.button.view.more" bundle="${content}" />
                    </a>
                    <a href="controller?action=country_catalog&country=${order.tour.place.country.id}&page=1"
                       class="pull-right btn form-btn">
                        <fmt:message key="content.button.view.tours" bundle="${content}" />
                        &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="place-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"><fmt:message key="content.modal.info.about" bundle="${content}" />
                    ${order.tour.place.title}:</h3>
                </div>
                <div class="modal-body">
                    <img src="images/places/${order.tour.place.image}" alt="Place image"
                         class="img img-responsive" width="100%">
                    <br>
                </div>
                <div class="modal-footer">
                    <a class="pull-left btn form-btn"
                       href="controller?action=place_catalog&place=${order.tour.place.id}&page=1">
                        <fmt:message key="content.button.view.tours" bundle="${content}" />
                        &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="hotel-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"><fmt:message key="content.modal.info.about" bundle="${content}" />
                     ${order.hotel.title}:</h3>
                </div>
                <div class="modal-body">
                    <h3><fmt:message key="content.modal.main.image" bundle="${content}" />:</h3>
                    <img src="images/hotels/${order.hotel.mainImage}" alt="Hotel image"
                         class="img img-responsive" width="100%">
                    <br>
                    <h3><fmt:message key="content.modal.room.image" bundle="${content}" />:</h3>
                    <img src="images/hotels/${order.hotel.roomImage}" alt="Room image"
                         class="img img-responsive">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <strong><fmt:message key="content.modal.stars" bundle="${content}" />:</strong>
                            </div>
                            <div class="col-md-6">
                                ${order.hotel.stars}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <strong><fmt:message key="content.modal.all.rooms" bundle="${content}" />:</strong>
                            </div>
                            <div class="col-md-6">
                                ${order.hotel.allRooms}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
