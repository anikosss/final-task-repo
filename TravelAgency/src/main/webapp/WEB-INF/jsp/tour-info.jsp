<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.tour.info" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="tour" class="by.epam.agency.entity.impl.Tour" scope="request"/>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=tour_info&tour=${tour.id}">
                    <button type="button" class="btn form-btn" data-dismiss="modal">
                        <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=tour_info&tour=${tour.id}">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <br><br><br><br>
    <h1><fmt:message key="content.tour.info" bundle="${content}" />:</h1>
    <c:if test="${tour.status eq 'hot'}">
        <span class="bg-hot"><fmt:message key="content.hot" bundle="${content}" /></span>
    </c:if>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-5">
                <a href="#place-modal" data-toggle="modal" ><img src="images/places/${tour.place.image}"
                                                                 class="img-thumbnail img-info" alt="Tour image"></a>
            </div>
            <div id="place-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title">${tour.place.title}:</h3>
                        </div>
                        <div class="modal-body">
                            <img src="images/places/${tour.place.image}" alt="Tour image"
                                 class="img img-responsive">
                        </div>
                        <div class="modal-footer">
                            <a href="controller?action=place_catalog&place=${tour.place.id}&page=1" class="btn form-btn pull-left">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <fmt:message key="content.button.view.tours" bundle="${content}" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <table class="table table-hover">
                    <tr>
                        <th><fmt:message key="content.table.country" bundle="${content}" />:</th>
                        <td><a href="controller?action=country_info&country=${tour.place.country.id}">
                            <fmt:message key="country.name.${fn:toLowerCase(tour.place.country.name)}" bundle="${info}" />
                        </a></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.place" bundle="${content}" />:</th>
                        <td><a href="#place-modal" data-toggle="modal">${tour.place.title}</a></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.arrive.date" bundle="${content}" />:</th>
                        <td><fmt:formatDate pattern="dd.MM.yyyy" value="${tour.arriveDate}"/>
                            / ${tour.arriveTime} </td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.return.date" bundle="${content}" />:</th>
                        <td><fmt:formatDate pattern="dd.MM.yyyy" value="${tour.returnDate}"/>
                            / ${tour.returnTime}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.all.places" bundle="${content}" />:</th>
                        <td>${tour.allPlaces}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.free.places" bundle="${content}" />:</th>
                        <td>${tour.freePlaces}</td>
                    </tr>
                    <c:if test="${tour.status eq 'hot'}">
                        <tr>
                            <th><fmt:message key="content.table.default.price" bundle="${content}" />:</th>
                            <td><del>${tour.defaultPrice} $</del></td>
                        </tr>
                    </c:if>
                    <tr>
                        <th><fmt:message key="content.table.price" bundle="${content}" />:</th>
                        <td><i <c:if test="${tour.status eq 'hot'}"> style="font-size: 18px; font-weight: bold; color: #E63630;" </c:if>>${tour.price} $</i></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><a href="#hotels"><fmt:message key="content.choose.hotel" bundle="${content}" /></a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <h1><fmt:message key="content.available.hotels" bundle="${content}" />:</h1>
    <br>
    <c:choose>
        <c:when test="${tour.place.hotels ne null and not empty tour.place.hotels}">
            <form action="" role="form">
                <div class="row" id="hotels">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <tr>
                                <th class="info"><fmt:message key="content.table.title" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.stars" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.all.rooms" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.free.rooms" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.hotel.image" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.room.image" bundle="${content}" /></th>
                                <th class="info"></th>
                            </tr>
                            <c:forEach items="${tour.place.hotels}" var="elem">
                                <tr>
                                    <td>${elem.title}</td>
                                    <td>${elem.stars}</td>
                                    <td>${elem.allRooms}</td>
                                    <td>${elem.freeRooms}</td>
                                    <td><a href="#hotel-main-image-modal-${elem.id}" data-toggle="modal">
                                        <fmt:message key="content.table.show.image" bundle="${content}" />
                                    </a></td>
                                    <td><a href="#hotel-room-image-modal-${elem.id}" data-toggle="modal">
                                        <fmt:message key="content.table.show.image" bundle="${content}" />
                                    </a></td>
                                    <td>
                                        <c:if test="${elem.freeRooms > 0}">
                                            <label class="radio-inline"><input name="hotel" value="${elem.id}" type="radio">
                                            <fmt:message key="content.choose.hotel" bundle="${content}" /></label>
                                        </c:if>
                                    </td>
                                </tr>
                                <div class="text-center">
                                    <div id="hotel-main-image-modal-${elem.id}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title">
                                                        <fmt:message key="content.modal.image.for" bundle="${content}" />
                                                            ${elem.title}:</h3>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <img src="images/hotels/${elem.mainImage}" alt="Hotel image"
                                                         class="img img-responsive" width="100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <div id="hotel-room-image-modal-${elem.id}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title">
                                                        <fmt:message key="content.modal.image.for.room" bundle="${content}" />
                                                            ${elem.title}:</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="images/hotels/${elem.roomImage}" alt="Room image"
                                                         class="img img-responsive" width="100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </table>
                        <input type="hidden" name="action" value="add_to_cart">
                        <input type="hidden" name="tour" value="${tour.id}">
                        <input type="hidden" name="page" value="tour_info">
                        <c:choose>
                            <c:when test="${not empty user.login}">
                                <c:choose>
                                    <c:when test="${tour.freePlaces > 0}">
                                        <button type="submit" class="btn form-btn">
                                            <span class="glyphicon glyphicon-plus"></span>
                                            <fmt:message key="content.button.add.to.cart" bundle="${content}" />
                                        </button>
                                    </c:when>
                                    <c:otherwise>
                                        <h4 class="bg-primary">
                                            <fmt:message key="content.cart.no.places" bundle="${content}" />.
                                        </h4>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <div class="row">
                                    <div class="col-md-5">
                                        <h4 class="bg-primary">
                                            <fmt:message key="content.cart.sign.in" bundle="${content}" />.
                                        </h4>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </form>
        </c:when>
        <c:otherwise>
            <div class="row">
                <div class="col-md-5">
                    <h4 class="bg-primary">
                        <fmt:message key="content.no.hotels.in.place" bundle="${content}" />
                    </h4>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<hr>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
