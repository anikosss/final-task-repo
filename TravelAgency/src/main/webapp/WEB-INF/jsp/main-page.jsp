<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.main.page" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=main_page">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=main_page">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
</div>
<jsp:include page="common/carousel.jsp" />
<div class="container">
    <div class="row">
        <jsp:include page="common/main-title.jsp" />
    </div>
    <br>
    <hr class="menu-hr">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2><fmt:message key="content.nearest.tours" bundle="${content}" /></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <c:choose>
                <c:when test="${tours eq null or empty tours}">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-info fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <fmt:message key="content.alert.no.tours" bundle="${content}" />
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${tours}" var="elem">
                        <a href="controller?action=tour_info&tour=${elem.id}" style="text-decoration: none;">
                            <div class="col-md-3 tour text-center">
                                <c:if test="${elem.status eq 'hot'}">
                                    <span class="hot pull-right">
                                        <span class="glyphicon glyphicon-fire"></span>
                                        &nbsp; <fmt:message key="content.hot"  bundle="${content}" />
                                    </span>
                                </c:if>
                                <img src="images/places/${elem.place.image}" alt="Place image">
                                <h3>${elem.place.title}</h3>
                                <h4><a href="controller?action=country_info&country=${elem.place.country.id}">
                                    <fmt:message key="country.name.${fn:toLowerCase(elem.place.country.name)}" bundle="${info}" /></a></h4>
                                <h6><i style="color: #044a43;">
                                    <fmt:formatDate pattern="dd.MM.yyyy" value="${elem.arriveDate}"/></i></h6>
                                <c:choose>
                                    <c:when test="${elem.status eq 'hot'}">
                                        <span><del>${elem.defaultPrice} $</del></span><h4><i class="hot">${elem.price} $</i></h4>
                                    </c:when>
                                    <c:otherwise>
                                        <h4><i>${elem.price}$</i></h4>
                                    </c:otherwise>
                                </c:choose>
                                <p class="text-center">
                                    <a href="controller?action=tour_info&tour=${elem.id}">
                                        <button class="btn form-btn">
                                            <fmt:message key="content.button.details" bundle="${content}" />
                                        </button>
                                    </a>
                                </p>
                            </div>
                        </a>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <h2 id="about-countries"><fmt:message key="content.country.info" bundle="${content}" />:</h2>
            </div>
        </div>
        <br>
        <c:forEach items="${countriesList}" var="elem">
            <div class="row">
                <div class="col-md-12">
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="#">
                                <img class="media-object"
                                     src="images/media-${fn:toLowerCase(elem.name)}-700.jpg" alt="Title">
                            </a>
                        </div>
                        <div class="media-body">
                            <h2 class="media-heading">
                                <fmt:message key="country.name.${fn:toLowerCase(elem.name)}" bundle="${info}" /></h2>
                            <c:set var="countryDescrirption" scope="page"><fmt:message
                                    key="${elem.description}" bundle="${info}"/> </c:set>
                            <p class="media-text">
                                <c:forTokens items="${countryDescrirption}" delims=" " end="35" var="descr">
                                    ${descr}
                                </c:forTokens>...
                            </p>
                            <a class="media-href btn form-btn" href="controller?action=country_info&country=${elem.id}">
                                <fmt:message key="content.button.view.more" bundle="${content}" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </c:forEach>
    </div>
    <br>
    <br>
    <br>
</div>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp" />
</body>
</html>