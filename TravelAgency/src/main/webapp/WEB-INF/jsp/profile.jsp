<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.profile" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=profile">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=profile">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
            <jsp:include page="common/left-menu.jsp" />
        </div>
    </div>
</div>
<br><br><br>
<div class="container">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-8">
                <h1><fmt:message key="content.profile" bundle="${content}" /> ${user.login}</h1>
            </div>
            <div class="col-md-4">
                <img src="images/photo_not_found.png" alt="Photo" class="img-responsive" height="200px" width="200px">
            </div>
        </div>
    </div>
    <hr>
    <h2><fmt:message key="content.orders.in.waiting" bundle="${content}" />:</h2>
    <div class="row">
        <div class="col-md-12">
            <c:choose>
                <c:when test="${orders eq null}">
                    <h4 class="bg-warning">
                        <fmt:message key="content.profile.null.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:when test="${empty orders}">
                    <h4 class="bg-info">
                        <fmt:message key="content.profile.no.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:otherwise>
                    <table class="table">
                        <tr>
                            <th class="info"><fmt:message key="content.table.country" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.place" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.arrive.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.return.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.hotel" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.price" bundle="${content}" /></th>
                            <th class="info"></th>
                        </tr>
                        <c:forEach items="${orders}" var="elem">
                            <tr class="warning">
                                <td><a href="#country-modal-${elem.id}" data-toggle="modal">
                                    <fmt:message key="country.name.${fn:toLowerCase(elem.tour.place.country.name)}" bundle="${info}" />
                                </a></td>
                                <td><a href="#place-modal-${elem.id}" data-toggle="modal">${elem.tour.place.title}</a></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.arriveDate}"/>
                                    / ${elem.tour.arriveTime}</td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.returnDate}"/>
                                    / ${elem.tour.returnTime}</td>
                                <td><a href="#hotel-modal-${elem.id}" data-toggle="modal">${elem.hotel.title}</a></td>
                                <td>${elem.price} <c:if test="${elem.tourStatus eq 'hot'}">
                                    <span class="bg-hot"><fmt:message key="content.hot" bundle="${content}" /></span>
                                </c:if></td>
                                <td>
                                    <form action="controller" method="POST" accept-charset="UTF-8">
                                        <button type="submit" class="btn form-btn form-btn-red">
                                            <span class="glyphicon glyphicon-remove"></span>
                                             <fmt:message key="content.button.cancel" bundle="${content}" />
                                        </button>
                                        <input type="hidden" name="action" value="cancel_order">
                                        <input type="hidden" name="order" value="${elem.id}">
                                    </form>
                                </td>
                            </tr>
                            <div id="country-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}" />
                                             ${elem.tour.place.country.name}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/media-${fn:toLowerCase(elem.tour.place.country.name)}-700.jpg"
                                                 alt="Country image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                            <c:set var="countryDescrirption" scope="page"><fmt:message
                                                    key="${elem.tour.place.country.description}" bundle="${info}"/></c:set>
                                            <p>
                                                <c:forTokens items="${countryDescrirption}" delims=" " end="35" var="descr">
                                                    ${descr}
                                                </c:forTokens>...
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="pull-left btn form-btn"
                                               href="controller?action=country_info&country=${elem.tour.place.country.id}">
                                                <fmt:message key="content.button.view.more" bundle="${content}" />
                                            </a>
                                            <a href="controller?action=country_catalog&country=${elem.tour.place.country.id}&page=1"
                                               class="pull-right btn form-btn">
                                                <span class="glyphicon glyphicon-arrow-right"></span>
                                                &nbsp; <fmt:message key="content.button.view.tours" bundle="${content}" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="place-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}" />
                                             ${elem.tour.place.title}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/places/${elem.tour.place.image}" alt="Place image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="pull-left btn form-btn"
                                               href="controller?action=place_catalog&place=${elem.tour.place.id}&page=1">
                                                <span class="glyphicon glyphicon-arrow-right"></span>
                                                &nbsp; <fmt:message key="content.button.view.tours" bundle="${content}" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="hotel-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}"/>
                                             ${elem.hotel.title}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <h3><fmt:message key="content.modal.main.image" bundle="${content}" />:</h3>
                                            <img src="images/hotels/${elem.hotel.mainImage}" alt="Hotel image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                            <h3><fmt:message key="content.modal.room.image" bundle="${content}" />:</h3>
                                            <img src="images/hotels/${elem.hotel.roomImage}" alt="Room image"
                                                 class="img img-responsive">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <strong><fmt:message key="content.modal.stars" bundle="${content}" />:</strong>
                                                    </div>
                                                    <div class="col-md-6">
                                                            ${elem.hotel.stars}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <strong><fmt:message key="content.modal.all.rooms" bundle="${content}" />:</strong>
                                                    </div>
                                                    <div class="col-md-6">
                                                            ${elem.hotel.allRooms}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="pull-left btn form-btn" href="">
                                                <fmt:message key="content.button.view.more" bundle="${content}" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <h2><fmt:message key="content.confirmed.orders" bundle="${content}" />:</h2>
    <div class="row">
        <div class="col-md-12">
            <c:choose>
                <c:when test="${confirmedOrders eq null}">
                    <h4 class="bg-warning">
                        <fmt:message key="content.null.confirmed.orders" bundle="${content}" />.
                    </h4>
                </c:when>
                <c:when test="${empty confirmedOrders}">
                    <h4 class="bg-info">
                        <fmt:message key="content.no.confirmed.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:otherwise>
                    <table class="table">
                        <tr>
                            <th class="info"><fmt:message key="content.table.country" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.place" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.arrive.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.return.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.hotel" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.price" bundle="${content}" /></th>
                            <th class="info"></th>
                        </tr>
                        <c:forEach items="${confirmedOrders}" var="elem" begin="0" end="4">
                            <tr class="success">
                                <td><a href="#country-modal-${elem.id}" data-toggle="modal">
                                    <fmt:message key="country.name.${fn:toLowerCase(elem.tour.place.country.name)}" bundle="${info}" />
                                </a></td>
                                <td><a href="#place-modal-${elem.id}" data-toggle="modal">${elem.tour.place.title}</a></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.arriveDate}"/>
                                    / ${elem.tour.arriveTime}</td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.tour.returnDate}"/>
                                    / ${elem.tour.returnTime}</td>
                                <td><a href="#hotel-modal-${elem.id}" data-toggle="modal">${elem.hotel.title}</a></td>
                                <td>${elem.price}$ <c:if test="${elem.tourStatus eq 'hot'}">
                                    <span class="bg-hot"><fmt:message key="content.hot" bundle="${content}" /></span>
                                </c:if></td>
                                <td></td>
                            </tr>
                            <div id="country-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}" />
                                             ${elem.tour.place.country.name}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/media-${fn:toLowerCase(elem.tour.place.country.name)}-700.jpg"
                                                 alt="Country image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                            <c:set var="countryDescrirption" scope="page"><fmt:message
                                                    key="${elem.tour.place.country.description}" bundle="${info}"/></c:set>
                                            <p>
                                                <c:forTokens items="${countryDescrirption}" delims=" " end="35" var="descr">
                                                    ${descr}
                                                </c:forTokens>...
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="pull-left btn form-btn"
                                               href="controller?action=country_info&country=${elem.tour.place.country.id}">
                                                <fmt:message key="content.button.view.more" bundle="${content}" /></a>
                                            <a href="controller?action=country_catalog&country=${elem.tour.place.country.id}&page=1"
                                               class="pull-right btn form-btn">
                                                <fmt:message key="content.button.view.tours" bundle="${content}" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="place-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}" />
                                             ${elem.tour.place.title}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/places/${elem.tour.place.image}" alt="Place image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="pull-left btn form-btn"
                                               href="controller?action=place_catalog&place=${elem.tour.place.id}&page=1">
                                                <fmt:message key="content.button.view.tours" bundle="${content}" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="hotel-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about" bundle="${content}" />
                                             ${elem.hotel.title}:</h3>
                                        </div>
                                        <div class="modal-body">
                                            <h3><fmt:message key="content.modal.main.image" bundle="${content}" />:</h3>
                                            <img src="images/hotels/${elem.hotel.mainImage}" alt="Hotel image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                            <h3><fmt:message key="content.modal.room.image" bundle="${content}" />:</h3>
                                            <img src="images/hotels/${elem.hotel.roomImage}" alt="Room image"
                                                 class="img img-responsive">
                                            <br>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <strong><fmt:message key="content.modal.stars" bundle="${content}" />:</strong>
                                                </div>
                                                <div class="col-md-3">
                                                        ${elem.hotel.stars}
                                                </div>
                                                <br>
                                                <div class="col-md-3">
                                                    <strong><fmt:message key="content.modal.all.rooms" bundle="${content}" />:</strong>
                                                </div>
                                                <div class="col-md-3">
                                                        ${elem.hotel.allRooms}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
