<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.choose.place" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_tour">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_tour">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h2><fmt:message key="content.button.choose.place" bundle="${content}" />:</h2>
    <a href="controller?action=all_tours&page=1" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.menu.all.tours" bundle="${content}" />
    </a>
    <br>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th class="info"><fmt:message key="content.table.place.id" bundle="${content}" /></th>
                    <th class="info"><fmt:message key="content.table.place.title" bundle="${content}" /></th>
                    <th class="info"></th>
                </tr>
                <c:forEach items="${places}" var="elem">
                    <form action="controller" method="POST" accept-charset="UTF-8">
                        <tr>
                            <td>${elem.id}</td>
                            <td>${elem.title}</td>
                            <td>
                                <button type="submit" class="btn form-btn">
                                    <fmt:message key="content.button.choose.place" bundle="${content}" />
                                    &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                                </button>
                            </td>
                        </tr>
                        <input type="hidden" name="action" value="choose_tour_place">
                        <input type="hidden" name="country" value="${country}">
                        <input type="hidden" name="place" value="${elem.id}">
                    </form>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
