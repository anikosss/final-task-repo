<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.write.info" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_tour">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_tour">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h2><fmt:message key="content.title.write.info" bundle="${content}" />:</h2>
    <a href="controller?action=all_tours&page=1" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.menu.all.tours" bundle="${content}" />
    </a>
    <br>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="form-group">
                    <label for="arrive-date"><fmt:message key="content.table.arrive.date" bundle="${content}" />:</label>
                    <c:set var="enterArriveDate" scope="page">
                        <fmt:message key="content.form.enter.arrive.date" bundle="${content}" />
                    </c:set>
                    <input name="arriveDate" type="date" class="form-control form-input"
                           id="arrive-date" placeholder="${enterArriveDate}" pattern="[\d]{2}.[\d]{2}.[\d]{4}" required>
                </div>
                <div class="form-group">
                    <label for="arrive-time"><fmt:message key="content.table.arrive.time" bundle="${content}" />:</label>
                    <c:set var="enterArriveTime" scope="page">
                        <fmt:message key="content.form.enter.arrive.time" bundle="${content}" />
                    </c:set>
                    <input name="arriveTime" type="time" class="form-control form-input"
                           id="arrive-time" placeholder="${enterArriveTime}" pattern="[\d]{2}:[\d]{2}" required
                           value="00:00">
                </div>
                <div class="form-group">
                    <label for="return-date"><fmt:message key="content.table.return.date" bundle="${content}" />:</label>
                    <c:set var="enterReturnDate" scope="page">
                        <fmt:message key="content.form.enter.return.date" bundle="${content}" />
                    </c:set>
                    <input name="returnDate" type="date" class="form-control form-input"
                           id="return-date" placeholder="${enterReturnDate}" pattern="[\d]{2}.[\d]{2}.[\d]{4}" required>
                </div>
                <div class="form-group">
                    <label for="return-time"><fmt:message key="content.table.return.time" bundle="${content}" />:</label>
                    <c:set var="enterReturnTime" scope="page">
                        <fmt:message key="content.form.enter.return.time" bundle="${content}" />
                    </c:set>
                    <input name="returnTime" type="time" class="form-control form-input"
                           id="return-time" placeholder="${enterReturnTime}" pattern="[\d]{2}:[\d]{2}" required
                           value="00:00">
                </div>
                <div class="form-group">
                    <label for="price"><fmt:message key="content.table.price" bundle="${content}" />:</label>
                    <c:set var="enterPrice" scope="page">
                        <fmt:message key="content.form.enter.price" bundle="${content}" />
                    </c:set>
                    <input name="price" type="text" class="form-control form-input"
                           id="price" placeholder="${enterPrice}" pattern="[\d]{1,7}[,\d]{0,4}"
                           required value="1000">
                </div>
                <div class="form-group">
                    <label><fmt:message key="content.table.seats.class" bundle="${content}" />:</label>
                    <c:forEach items="${seatsClasses}" var="elem">
                        <label class="radio-inline"><input type="radio" name="seatsClass" value="${elem}">
                            <fmt:message key="seats.class.${fn:toLowerCase(elem)}" bundle="${info}" /></label>
                    </c:forEach>
                </div>
                <br>
                <button type="submit" class="btn form-btn">
                    <span class="glyphicon glyphicon-ok"></span>
                    &nbsp; <fmt:message key="content.button.add.tour" bundle="${content}" />
                </button>
                <input type="hidden" name="action" value="add_tour">
                <input type="hidden" name="country" value="${country}">
                <input type="hidden" name="place" value="${place}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
