<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.change.tour" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="tour" class="by.epam.agency.entity.impl.Tour" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_tour&tour=${tour.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_tour&tour=${tour.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <br><br><br><br><br>
    <h2><fmt:message key="content.change.tour" bundle="${content}" />:</h2>
    <a href="controller?action=tour_details&tour=${tour.id}" class="btn form-btn form-btn-info">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.button.back" bundle="${content}" />
    </a>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <img src="images/places/${tour.place.image}" alt="Place image" class="img img-responsive" width="100%">
        </div>
        <div class="col-md-8">
            <form action="controller" method="POST">
                <table class="table table=hover">
                    <tr>
                        <th><fmt:message key="content.table.id" bundle="${content}" />:</th>
                        <td>
                            ${tour.id} <c:if test="${tour.status eq 'hot'}">
                            <span class="bg-hot"><fmt:message key="content.hot" bundle="${content}" /></span>
                        </c:if>
                        </td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.country" bundle="${content}" /></th>
                        <td><fmt:message key="country.name.${fn:toLowerCase(tour.place.country.name)}"
                                         bundle="${info}" /></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.place" bundle="${content}" />:</th>
                        <td>${tour.place.title}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.arrive.date" bundle="${content}" />:</th>
                        <td><input type="date" name="arriveDate" value="${tour.arriveDate}"
                                   pattern="[\d]{2}.[\d]{2}.[\d]{4}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.arrive.time" bundle="${content}" />:</th>
                        <td><input type="time" name="arriveTime" value="${tour.arriveTime}"
                                   pattern="[\d]{2}:[\d]{2}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.return.date" bundle="${content}" />:</th>
                        <td><input type="date" name="returnDate" value="${tour.returnDate}"
                                   pattern="[\d]{2}.[\d]{2}.[\d]{4}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.return.time" bundle="${content}" />:</th>
                        <td><input type="time" name="returnTime" value="${tour.returnTime}"
                                   pattern="[\d]{2}:[\d]{2}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.seats.class" bundle="${content}" />:</th>
                        <td>
                            <c:forEach items="${seatsClasses}" var="elem">
                                <label class="radio-inline"><input type="radio"
                                <c:if test="${elem eq fn:toLowerCase(tour.seatsClass.value)}" >
                                        checked
                                </c:if> name="seatsClass" value="${elem}">
                                    <fmt:message key="seats.class.${fn:toLowerCase(elem)}" bundle="${info}"/></label>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.default.price" bundle="${content}" />:</th>
                        <td>
                            <input type="text" name="price" value="${tour.defaultPrice}"
                               pattern="[\d]{1,7}[.\d]{0,4}" required>
                        </td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.price" bundle="${content}" />:</th>
                        <td>${tour.price}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.sale" bundle="${content}" />:</th>
                        <td>
                            <input type="text" name="sale" value="${tour.sale}"
                                   pattern="(?m)^[1-9]{1}[\.\d]{0,1}" required>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <button type="submit" class="btn form-btn">
                                <span class="glyphicon glyphicon-ok"></span>
                                &nbsp; <fmt:message key="content.button.change" bundle="${content}" />
                            </button>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="action" value="change_tour">
                <input type="hidden" name="tour" value="${tour.id}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
