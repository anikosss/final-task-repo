<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.all.hotels" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=all_hotels&page=1">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=all_hotels&page=1">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="menu-item-tours" class="col-md-2 col-md-offset-1 menu-title">
                <a href="controller?action=all_tours&page=1">
                    <fmt:message key="content.menu.tours" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-places" class="col-md-2 menu-title">
                <a href="controller?action=all_places&page=1">
                    <fmt:message key="content.menu.places" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-hotels" class="col-md-2 menu-title  menu-title-active">
                <a href="controller?action=all_hotels&page=1">
                    <fmt:message key="content.menu.hotels" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-orders" class="col-md-2 menu-title">
                <a href="controller?action=all_orders&page=1">
                    <fmt:message key="content.menu.orders" bundle="${content}" />
                </a>
            </div>
        </div>
    </div>
    <hr class="menu-hr">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><fmt:message key="content.menu.all.hotels" bundle="${content}" /></h2>
                <br>
                <form action="controller" method="GET" accept-charset="UTF-8">
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-plus"></span>
                        &nbsp; <fmt:message key="content.button.add.new" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="go_to_add_hotel">
                </form>
                <br>
                <c:choose>
                    <c:when test="${hotels eq null}">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-info fade in">
                                <fmt:message key="content.null.hotels" bundle="${content}" />.
                            </div>
                        </div>
                    </c:when>
                    <c:when test="${empty hotels}">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-info fade in">
                                <fmt:message key="content.no.hotels" bundle="${content}" />.
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-hover">
                            <tr>
                                <th class="info"><fmt:message key="content.table.title" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.stars" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.all.rooms" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.main.image" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.room.image" bundle="${content}" /></th>
                                <th class="info"></th>
                                <th class="info"></th>
                            </tr>
                            <c:forEach var="elem" items="${hotels}">
                                <form action="controller" method="POST" accept-charset="UTF-8">
                                    <tr>
                                        <td>${elem.title}</td>
                                        <td>${elem.stars}</td>
                                        <td>${elem.allRooms}</td>
                                        <td><a href="#" data-toggle="modal"
                                               data-target="#hotel-main-image-modal-${elem.id}">
                                            <fmt:message key="content.table.show.image" bundle="${content}" />
                                        </a></td>
                                        <td><a href="#" data-toggle="modal"
                                               data-target="#hotel-room-image-modal-${elem.id}">
                                            <fmt:message key="content.table.show.image" bundle="${content}" />
                                        </a></td>
                                        <td><a href="controller?action=hotel_details&hotel=${elem.id}" class="btn form-btn form-btn-info">
                                            <span class="glyphicon glyphicon-cog"></span>
                                            &nbsp; <fmt:message key="content.button.details" bundle="${content}" />
                                        </a></td>
                                        <td>
                                            <button type="submit" class="btn form-btn form-btn-red">
                                                <span class="glyphicon glyphicon-trash"></span>
                                                &nbsp; <fmt:message key="content.button.delete" bundle="${content}" />
                                            </button>
                                        </td>
                                    </tr>
                                    <input type="hidden" name="hotel" value="${elem.id}"/>
                                    <input type="hidden" name="action" value="delete_hotel">
                                </form>
                                <div id="hotel-main-image-modal-${elem.id}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">
                                                <fmt:message key="content.modal.image.for" bundle="${content}" />
                                                 ${elem.title}</h3>
                                            </div>
                                            <div class="modal-body">
                                                <img src="images/hotels/${elem.mainImage}" alt="Hotel image"
                                                     class="img img-responsive" width="100%">
                                                <br>
                                                <button type="button" class="btn form-btn"
                                                        data-dismiss="modal">
                                                    <fmt:message key="content.button.close" bundle="${content}" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="hotel-room-image-modal-${elem.id}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">
                                                <fmt:message key="content.modal.image.for.room" bundle="${content}" />
                                                 ${elem.title}</h3>
                                            </div>
                                            <div class="modal-body">
                                                <img src="images/hotels/${elem.roomImage}" alt="Room image"
                                                     class="img img-responsive" width="100%">
                                                <br>
                                                <button type="button" class="btn form-btn"
                                                        data-dismiss="modal">
                                                    <fmt:message key="content.button.close" bundle="${content}" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <c:if test="${not empty pagesCount and pagesCount gt 1}">
            <br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="pagination pagination-lg ">
                        <c:forEach begin="1" end="${pagesCount}" var="num">
                            <li <c:if test="${page eq num}">class="active"</c:if>>
                                <a href="controller?action=all_hotels&page=${num}">${num}</a></li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </c:if>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
