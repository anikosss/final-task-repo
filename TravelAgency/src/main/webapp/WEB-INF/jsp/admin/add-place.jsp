<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.add.place" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_place">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_place">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <h2><fmt:message key="content.title.add.place" bundle="${content}" />:</h2>
            <a href="controller?action=all_places&page=1" class="btn form-btn">
                <span class="glyphicon glyphicon-chevron-left"></span>
                &nbsp; <fmt:message key="content.menu.all.places" bundle="${content}" />
            </a>
            <hr>
            <div class="col-md-6">
                <form role="form" action="controller" method="POST">
                    <div class="form-group">
                        <label><fmt:message key="content.table.country" bundle="${content}" />:</label>
                        <br>
                        <c:forEach items="${countries}" var="elem">
                            <label class="radio-inline">
                                <input type="radio" name="country" value="${elem.id}">
                                    <fmt:message key="country.name.${fn:toLowerCase(elem.name)}" bundle="${info}" /></label>
                        </c:forEach>
                    </div>
                    <div class="form-group">
                        <label for="title"><fmt:message key="content.table.title" bundle="${content}" />:</label>
                        <c:set var="enterTitle" scope="page">
                            <fmt:message key="content.form.enter.title" bundle="${content}" />
                        </c:set>
                        <input type="text" name="title" class="form-control form-input" id="title"
                               placeholder="${enterTitle}" pattern="[а-яА-Я\w\s'\-]{3,50}" required>
                    </div>
                    <div class="form-group">
                        <label for="image"><fmt:message key="content.modal.main.image" bundle="${content}" />:</label>
                        <input type="file" name="image" class="form-control form-input" id="image"
                               pattern="(place)-[\w\-_'\.]{1,80}" value="place-image-not-found.jpg">
                    </div>
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-plus"></span>
                        &nbsp; <fmt:message key="content.button.add.place" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="add_place">
                </form>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
