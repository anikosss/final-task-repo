<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.all.tours" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=all_tours&page=1">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=all_tours&page=1">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="menu-item-tours" class="col-md-2 col-md-offset-1 menu-title menu-title-active">
                <a href="controller?action=all_tours&page=1">
                    <fmt:message key="content.menu.tours" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-places" class="col-md-2 menu-title">
                <a href="controller?action=all_places&page=1">
                    <fmt:message key="content.menu.places" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-hotels" class="col-md-2 menu-title">
                <a href="controller?action=all_hotels&page=1">
                    <fmt:message key="content.menu.hotels" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-orders" class="col-md-2 menu-title">
                <a href="controller?action=all_orders&page=1">
                    <fmt:message key="content.menu.orders" bundle="${content}" />
                </a>
            </div>
        </div>
    </div>
    <hr class="menu-hr">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><fmt:message key="content.menu.all.tours" bundle="${content}" /></h2>
                <br>
                <form action="controller" method="GET" accept-charset="UTF-8">
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-plus"></span>
                        &nbsp; <fmt:message key="content.button.add.new" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="go_to_add_tour">
                </form>
                <br>
                <c:if test="${tours ne null and not empty tours}" >
                    <form action="controller" method="POST">
                        <button type="submit" class="btn form-btn form-btn-red">
                            <span class="glyphicon glyphicon-trash"></span>
                            &nbsp; <fmt:message key="content.button.delete.old" bundle="${content}" />
                        </button>
                        <input type="hidden" name="action" value="delete_old_tours">
                    </form>
                </c:if>
                <br>
                <c:choose>
                    <c:when test="${tours eq null}">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-info fade in">
                                <fmt:message key="content.null.tours" bundle="${content}" />.
                            </div>
                        </div>
                    </c:when>
                    <c:when test="${empty tours}">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-info fade in">
                                <fmt:message key="content.no.tours" bundle="${content}" />
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-hover">
                            <tr>
                                <th class="info"><fmt:message key="content.table.place" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.country" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.arrive.date" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.return.date" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.price" bundle="${content}" /></th>
                                <th class="info"><fmt:message key="content.table.sale" bundle="${content}" /></th>
                                <th class="info"></th>
                                <th class="info"></th>
                                <th class="info"></th>
                            </tr>
                            <c:forEach var="elem" items="${tours}">
                                <tr>
                                    <td>${elem.place.title}</td>
                                    <td><fmt:message key="country.name.${fn:toLowerCase(elem.place.country.name)}" bundle="${info}" /></td>
                                    <td>${elem.arriveDate} / ${elem.arriveTime}</td>
                                    <td>${elem.returnDate} / ${elem.returnTime}</td>
                                    <td>${elem.price} $</td>
                                    <td>${elem.sale} %</td>
                                    <td><a href="controller?action=tour_details&tour=${elem.id}" class="btn form-btn form-btn-info">
                                        <span class="glyphicon glyphicon-cog"></span>
                                        &nbsp; <fmt:message key="content.button.details" bundle="${content}" />
                                    </a></td>
                                    <td>
                                        <form action="controller" method="POST" accept-charset="UTF-8">
                                            <button type="submit" class="btn form-btn form-btn-red">
                                                <span class="glyphicon glyphicon-trash"></span>
                                                &nbsp; <fmt:message key="content.button.delete" bundle="${content}" />
                                            </button>
                                            <input type="hidden" name="tour" value="${elem.id}">
                                            <input type="hidden" name="action" value="delete_tour">
                                        </form>
                                    </td>
                                    <td>
                                        <c:set var="buttonClass" scope="page" value="btn form-btn form-btn-yellow" />
                                        <c:if test="${elem.status eq 'hot'}">
                                            <c:set var="buttonClass" scope="page" value="btn form-btn form-btn-yellow-active" />
                                        </c:if>
                                        <a href="#make-hot-modal-${elem.id}" data-toggle="modal" class="${buttonClass}">
                                            <span class="glyphicon glyphicon-fire"></span>
                                        </a>
                                    </td>
                                </tr>
                                <div id="make-hot-modal-${elem.id}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">
                                                    <fmt:message key="content.modal.make.hot" bundle="${content}" />
                                                </h3>
                                            </div>
                                            <div class="modal-body">
                                                <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                                                    <div class="form-group">
                                                        <label for="sale"><fmt:message key="content.form.sale" bundle="${content}" /></label>
                                                        <c:set var="enterSale" scope="page">
                                                            <fmt:message key="content.form.enter.sale" bundle="${content}" />
                                                        </c:set>
                                                        <input name="sale" type="text" class="form-control form-input"
                                                               id="sale" placeholder="${enterSale}"
                                                               required pattern="(?m)^[1-9]{1}[\d]{0,1}">
                                                    </div>
                                                    <br>
                                                    <button type="submit" class="btn form-btn">
                                                        <span class="glyphicon glyphicon-ok"></span>
                                                        &nbsp; <fmt:message key="content.button.confirm" bundle="${content}" />
                                                    </button>
                                                    <input type="hidden" name="action" value="make_tour_hot">
                                                    <input type="hidden" name="tour" value="${elem.id}">
                                                </form>
                                                <c:if test="${elem.status eq 'hot'}">
                                                    <form role="form" action="controller" method="POST">
                                                        <br>
                                                        <button type="submit" class="btn form-btn form-btn-red">
                                                            <span class="glyphicon glyphicon-remove"></span>
                                                            &nbsp; <fmt:message key="content.button.remove.sale" bundle="${content}" />
                                                        </button>
                                                        <input type="hidden" name="action" value="remove_sale">
                                                        <input type="hidden" name="tour" value="${elem.id}">
                                                    </form>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </table>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <c:if test="${not empty pagesCount and pagesCount gt 1}">
            <br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="pagination pagination-lg ">
                        <c:forEach begin="1" end="${pagesCount}" var="num">
                            <li <c:if test="${page eq num}">class="active"</c:if>>
                                <a href="controller?action=all_tours&page=${num}">${num}</a></li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </c:if>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
