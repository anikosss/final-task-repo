<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.agency" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.hotel.details" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="hotel" class="by.epam.agency.entity.impl.Hotel" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=change_hotel&hotel=${hotel.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=change_hotel&hotel=${hotel.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
</div>
<div class="container">
    <br><br><br><br><br>
    <h2><fmt:message key="content.change.hotel" bundle="${content}" />:</h2>
    <a href="controller?action=hotel_details&hotel=${hotel.id}" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.button.back" bundle="${content}" />
    </a>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <img src="images/hotels/${hotel.mainImage}" alt="Place image" class="img img-responsive" width="100%">
            <br>
            <img src="images/hotels/${hotel.roomImage}" alt="Room image" class="img img-responsive" width="100%">
        </div>
        <div class="col-md-8">
            <form action="controller" method="POST">
                <table class="table table=hover" border="0">
                    <tr>
                        <th><fmt:message key="content.table.id" bundle="${content}" />:</th>
                        <td>${hotel.id}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.title" bundle="${content}" />:</th>
                        <td><input name="title" type="text" class="form-control form-input" value="${hotel.title}"
                                   title="title" pattern="[а-яА-Я\w\s']{1,}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.stars" bundle="${content}" /></th>
                        <td><input name="stars" type="text" class="form-control form-input" value="${hotel.stars}"
                                   title="stars" pattern="[1234567]{1}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.all.rooms" bundle="${content}" /></th>
                        <td><input name="allRooms" type="text" class="form-control form-input" value="${hotel.allRooms}"
                                   title="allRooms"  pattern="(?m)^[1-9][\d]{1,}" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.modal.main.image" bundle="${content}" />:</th>
                        <td><input type="file" name="mainImage" class="form-control form-input"
                                   pattern="(hotel)-[\w\-_'\.]{1,80}" value="${hotel.mainImage}"></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.modal.room.image" bundle="${content}" />:</th>
                        <td><input type="file" name="roomImage" class="form-control form-input"
                                   pattern="(room)-[\w\-_'\.]{1,80}" value="${hotel.roomImage}"></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><button type="submit" class="btn form-btn">
                            <span class="glyphicon glyphicon-ok"></span>
                            &nbsp; <fmt:message key="content.button.change" bundle="${content}" />
                        </button></td>
                    </tr>
                </table>
                <input type="hidden" name="action" value="change_hotel">
                <input type="hidden" name="hotel" value="${hotel.id}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
