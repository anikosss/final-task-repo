<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.write.info" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_hotel">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_hotel">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
            <jsp:include page="../common/left-menu.jsp" />
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h2><fmt:message key="content.title.write.info" bundle="${content}" />:</h2>
    <a href="controller?action=all_hotels&page=1" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.menu.all.hotels" bundle="${content}" />
    </a>
    <br>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="form-group">
                    <label for="title"><fmt:message key="content.table.title" bundle="${content}" />:</label>
                    <c:set var="enterTitle" scope="page">
                        <fmt:message key="content.form.enter.title" bundle="${content}" />
                    </c:set>
                    <input name="title" type="text" class="form-control form-input"
                           id="title" placeholder="${enterTitle}" pattern="[а-яА-Я\w\s\-'\.]{1,}" required>
                </div>
                <div class="form-group">
                    <label for="stars"><fmt:message key="content.table.stars" bundle="${content}" />:</label>
                    <c:set var="enterStars" scope="page">
                        <fmt:message key="content.form.enter.stars" bundle="${content}" />
                    </c:set>
                    <input name="stars" type="text" class="form-control form-input"
                           id="stars" placeholder="${enterStars}" pattern="[1234567]{1}" required>
                </div>
                <div class="form-group">
                    <label for="all-rooms"><fmt:message key="content.modal.all.rooms" bundle="${content}" />:</label>
                    <c:set var="enterAllRooms" scope="page">
                        <fmt:message key="content.form.enter.all.rooms" bundle="${content}" />
                    </c:set>
                    <input name="allRooms" type="text" class="form-control form-input"
                           id="all-rooms" placeholder="${enterAllRooms}" pattern="(?m)^[1-9]{1}[\d]{0,}" required>
                </div>
                <div class="form-group">
                    <label for="main-image"><fmt:message key="content.table.main.image" bundle="${content}" />:</label>
                    <input type="file" name="mainImage" class="form-control form-input" id="main-image"
                           pattern="(hotel)-[\w\-_'\.]{1,80}" value="hotel-image-not-found.jpg">
                </div>
                <div class="form-group">
                    <label for="room-image"><fmt:message key="content.table.room.image" bundle="${content}" />:</label>
                    <input type="file" name="roomImage" class="form-control form-input" id="room-image"
                           pattern="(room)-[\w\-_'\.]{1,80}" value="hotel-image-not-found.jpg">
                </div>
                <br>
                <button type="submit" class="btn form-btn">
                    <span class="glyphicon glyphicon-ok"></span>
                    &nbsp; <fmt:message key="content.button.add.hotel" bundle="${content}" />
                </button>
                <input type="hidden" name="action" value="add_hotel">
                <input type="hidden" name="country" value="${country}">
                <input type="hidden" name="place" value="${place}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
