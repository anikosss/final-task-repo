<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#about-countries"><img class="first-slide" src="images/main-montenegro.jpg" alt="First slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <c:set var="countryName" scope="page">
                        <fmt:message key="country.name.${fn:toLowerCase(countriesList[0].name)}" bundle="${info}" /></c:set>
                    <h1 class="carousel-h1">${fn:toUpperCase(countryName)}</h1>
                    <br>
                    <p><a class="btn btn-lg btn-primary btn-carousel" href="#about-countries" role="button">
                        <fmt:message key="content.carousel.view.more" bundle="${content}" />
                    </a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <a href="#about-countries"><img class="second-slide" src="images/main-bulgaria.jpg" alt="Second slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="carousel-h1"><c:set var="countryName" scope="page">
                        <fmt:message key="country.name.${fn:toLowerCase(countriesList[1].name)}" bundle="${info}" /></c:set>
                        <h1 class="carousel-h1">${fn:toUpperCase(countryName)}</h1></h1>
                    <br>
                    <p><a class="btn btn-lg btn-primary btn-carousel" href="#about-countries" role="button">
                        <fmt:message key="content.carousel.view.more" bundle="${content}" />
                    </a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <a href="#about-countries"><img class="third-slide" src="images/main-greece.jpg" alt="Third slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="carousel-h1"><c:set var="countryName" scope="page">
                        <fmt:message key="country.name.${fn:toLowerCase(countriesList[2].name)}" bundle="${info}" /></c:set>
                        <h1 class="carousel-h1">${fn:toUpperCase(countryName)}</h1></h1>
                    <br>
                    <p><a class="btn btn-lg btn-primary btn-carousel" href="#about-countries" role="button">
                        <fmt:message key="content.carousel.view.more" bundle="${content}" />
                    </a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->
