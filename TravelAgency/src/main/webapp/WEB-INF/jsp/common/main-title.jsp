<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<div class="col-md-7">
    <img src="images/main-logo.png" alt="" class="img-responsive">
</div>
<div class="col-md-5">
    <br>
    <br>
    <h2>
        <fmt:message key="content.main.title" bundle="${content}" />
    </h2>
    <br>
    <p><a class="btn btn-lg btn-primary btn-main" href="controller?action=view_catalog&page=1" role="button">
        <fmt:message key="content.button.view.more" bundle="${content}" />
    </a></p>
</div>
