<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<div class="cbp-spmenu-push">
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <c:forEach items="${countriesList}" var="elem">
            <a href="controller?action=country_info&country=${elem.id}"
               class="menu-first"><span class="glyphicon glyphicon-chevron-right"></span>
                <c:set var="countryName" scope="page">
                    <fmt:message key="country.name.${fn:toLowerCase(elem.name)}" bundle="${info}" /></c:set>
                    ${fn:toUpperCase(countryName)}</a>
            <a href="controller?action=nearest_catalog&country=${elem.id}&page=1" class="menu-second">
                &#9;&#9;<fmt:message key="content.menu.nearest.tour" bundle="${content}" /></a>
            <a href="controller?action=last_catalog&country=${elem.id}&page=1" class="menu-second">
                &#9;&#9;<fmt:message key="content.menu.last.added.tours" bundle="${content}" /></a>
            <a href="controller?action=cheapest_catalog&country=${elem.id}&page=1" class="menu-second">
                &#9;&#9;<fmt:message key="content.menu.cheapest.tours" bundle="${content}" /></a>
        </c:forEach>
    </nav>
</div>
