<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.messages" var="msg" />
<jsp:useBean id="user" class="by.epam.agency.entity.impl.User" scope="session"/>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-brand-logo" href="#">
            <img id="showLeft" class="img-header-menu" src="images/menu-logo.png" alt=""></a>
        <a class="navbar-brand navbar-brand-logo" href="controller?action=main_page">
            <img class="img-header-logo" src="images/logo-small.png" alt=""></a>
        <a class="navbar-brand" href="controller?action=main_page">TRAVEL AGENCY</a>
        <span class="vert-line">|</span>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav menu-ul">
            <li class="menu-li"><a href="controller?action=main_page"><span class="glyphicon glyphicon-home"></span>&nbsp;
            <fmt:message key="content.header.home" bundle="${content}" /></a></li>
            <li class="menu-li"><a href="controller?action=view_catalog&page=1">
                <span class="glyphicon glyphicon-menu-hamburger"></span>&nbsp;
                <fmt:message key="content.header.catalog" bundle="${content}" /></a></li>
            <li class="menu-li"><a href="#footer"><span class="glyphicon glyphicon-edit"></span>&nbsp;
                <fmt:message key="content.header.contacts" bundle="${content}" /></a></li>
            <li>
                <a href="#lang-modal-ru" data-toggle="modal">
                    <img class="img-responsive img-header-lang" src="images/lang-rus.png"
                                 alt="Russian language"></a>
            </li>
            <li><a href="#lang-modal-en" data-toggle="modal">
                <img class="img-responsive img-header-lang" src="images/lang-us.png"
                                 alt="Russian language"></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <c:choose>
                <c:when test="${user.login ne null}">
                    <c:if test="${user.role.value eq 'admin'}">
                        <li class="menu-li"><a class="nav-bar-href" href="#" data-toggle="modal"
                                               data-target="#admin-modal">
                            <span class="glyphicon glyphicon-star"></span>&nbsp;
                        <fmt:message key="content.header.admin" bundle="${content}" /> </a></li>
                    </c:if>
                    <li class="menu-li"><a class="nav-bar-href" href="controller?action=profile">
                        <span class="glyphicon glyphicon-user"></span>&nbsp;
                    <fmt:message key="content.header.profile" bundle="${content}" /></a></li>
                    <li class="menu-li"><a class="nav-bar-href"
                                           href="#" data-toggle="modal"
                                           data-target="#exit-modal">
                                <span class="glyphicon glyphicon-log-out"></span>&nbsp;
                    <fmt:message key="content.header.exit" bundle="${content}" /></a>
                    </li>
                    <li class="menu-li"><a class="nav-bar-href last-nav-bar-href"
                                           href="controller?action=shopping_cart">
                        <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;
                    </a></li>
                </c:when>
                <c:otherwise>
                    <li class="menu-li"><a class="nav-bar-href" href="#" data-toggle="modal"
                                           data-target="#sign-in-modal"><span class="glyphicon glyphicon-lock">
                                                                </span>&nbsp;
                    <fmt:message key="content.header.sign.in" bundle="${content}" /></a></li>
                    <li class="menu-li"><a class="nav-bar-href last-nav-bar-href" href="#" data-toggle="modal"
                                           data-target="#sign-up-modal">
                        <span class="glyphicon glyphicon-user"></span>&nbsp;
                    <fmt:message key="content.header.sign.up" bundle="${content}" /></a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div><!--/.nav-collapse -->

</nav>
<!--Nav end -->
<c:if test="${not empty errorMessage}">
    <div class="col-md-4 col-md-offset-4">
        <div class="alert alert-danger alert-header fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <fmt:message key="${errorMessage}" bundle="${msg}" />.
        </div>
    </div>
</c:if>
<c:if test="${not empty successMessage}">
    <div class="col-md-4 col-md-offset-4">
        <div class="alert alert-success alert-header fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <fmt:message key="${successMessage}" bundle="${msg}"/>.
        </div>
    </div>
</c:if>
<c:if test="${not empty infoMessage}">
    <div class="col-md-4 col-md-offset-4">
        <div class="alert alert-info alert-header fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <fmt:message key="${infoMessage}" bundle="${msg}"/>.
        </div>
    </div>
</c:if>

<c:if test="${user.role.value eq 'admin'}">
    <div id="admin-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">
                        <fmt:message key="content.modal.go.to.admin" bundle="${content}" />
                    </h3>
                </div>
                <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                    <div class="modal-body">
                        <br>
                        <input type="hidden" name="action" value="all_tours">
                        <input type="hidden" name="page" value="1">
                        <button type="submit" class="btn form-btn">
                            <span class="glyphicon glyphicon-ok"></span>
                            &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                        </button>
                        <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span>
                            &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</c:if>

<div id="exit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.exit" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="logout">
                    <input type="hidden" name="page" value="${pageContext.request.requestURI}">
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="sign-in-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.form.authorization" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sign-in-login"><fmt:message key="content.form.login" bundle="${content}" /></label>
                        <c:set var="enterLogin" scope="page">
                            <fmt:message key="content.form.enter.login" bundle="${content}" />
                        </c:set>
                        <input name="login" type="text" class="form-control form-input"
                               id="sign-in-login" placeholder="${enterLogin}"
                               required pattern="^[a-zA-Z](.[\w9_-]*){3,20}">
                    </div>
                    <div class="form-group">
                        <label for="sign-in-pwd"><fmt:message key="content.form.password" bundle="${content}" />:</label>
                        <c:set var="enterPassword" scope="page">
                            <fmt:message key="content.form.enter.password" bundle="${content}" />
                        </c:set>
                        <input name="password" type="password" class="form-control form-input"
                               id="sign-in-pwd" placeholder="${enterPassword}"
                               required pattern="[\w]{6,30}">
                    </div>
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.sign.in" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="login">
                    <input type="hidden" name="page" value="${pageContext.request.requestURI}">
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.close" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="sign-up-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.form.registration" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="POST" accept-charset="UTF-8">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sign-up-login"><fmt:message key="content.form.login" bundle="${content}" /></label>
                        <input name="login" type="text" class="form-control form-input"
                               id="sign-up-login" placeholder="${enterLogin}"
                               required pattern="^[a-zA-Z](.[\w9_-]*){3,20}">
                        <span class="comment">
                        <fmt:message key="content.form.example.login" bundle="${content}" /></span>

                    </div>
                    <div class="form-group">
                        <label for="sign-up-pwd"><fmt:message key="content.form.password" bundle="${content}" /></label>
                        <input name="password" type="password" class="form-control form-input"
                               id="sign-up-pwd" placeholder="${enterPassword}"
                               required pattern="[\w]{6,30}">
                        <span class="comment">
                            <fmt:message key="content.form.example.password" bundle="${content}" />
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="sign-up-repeat"><fmt:message key="content.form.repeat.password" bundle="${content}" />:</label>
                        <c:set var="repeatPassword" scope="page"><fmt:message key="content.form.repeat.password" bundle="${content}" /></c:set>
                        <input name="repeatPassword" type="password" class="form-control form-input"
                               id="sign-up-repeat" placeholder="${repeatPassword}"
                               required pattern="[\w]{6,30}">
                        <span class="comment" data-toggle="tooltip" title="Repeat password">
                            <fmt:message key="content.form.example.password" bundle="${content}" />
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="sign-up-email"><fmt:message key="content.form.email" bundle="${content}" /></label>
                        <c:set var="enterEmail" scope="page"><fmt:message key="content.form.enter.email" bundle="${content}" /></c:set>
                        <input name="email" type="email" class="form-control form-input"
                        id="sign-up-email" placeholder="${enterEmail}"
                        required pattern="[\w]{1,20}(@){1}[\w]{2,6}(\.)[\w]{2,4}">
                        <span class="comment" data-toggle="tooltip" title="Your e-mail">
                            <fmt:message key="content.form.example.password" bundle="${content}" />
                        </span>
                    </div>
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.sign.up" bundle="${content}" /></button>
                    <input type="hidden" name="action" value="registration">
                    <input type="hidden" name="page" value="${pageContext.request.requestURI}">
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.close" bundle="${content}" />
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
