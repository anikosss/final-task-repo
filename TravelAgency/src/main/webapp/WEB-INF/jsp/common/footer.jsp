<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p><strong><span class="glyphicon glyphicon-road"></span>&nbsp;
                    <fmt:message key="content.footer.addresses" bundle="${content}" />
                </strong></p>
                <p><fmt:message key="content.footer.address.street" bundle="${content}" /></p>
                <p><fmt:message key="content.footer.address.bsuir" bundle="${content}" /></p>
            </div>
            <div class="col-md-4">
                <p><strong><span class="glyphicon glyphicon-earphone"></span>&nbsp;
                    <fmt:message key="content.footer.phones" bundle="${content}" />
                </strong></p>
                <p>+375 (33) 356-15-67</p>
                <p>+375 (25) 646-17-40</p>
                <p>8 (017) 201-17-59</p>
            </div>
            <div class="col-md-4">
                <p><strong><span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;
                    <fmt:message key="content.footer.social.networks" bundle="${content}" />
                </strong></p>
                <a href="http://vk.com"><fmt:message key="content.footer.vkontakte" bundle="${content}" /></a><br>
                <a href="http://twitter.com/Dima_Anikos"><fmt:message key="content.footer.twitter" bundle="${content}" /></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-4 col-md-4">
                <p class="text-center copyright"><user-tag:copyright email="anikosss94@gmail.com"/></p>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/left.menu.js"></script>

