var toursTable = document.getElementById('menu-item-tours'),
    placesTable = document.getElementById('menu-item-places'),
    hotelsTable = document.getElementById('menu-item-hotels'),
    ordersTable = document.getElementById('menu-item-orders'),
    toursActive = 0,
    placesActive = 0,
    hotelsActive = 0,
    ordersActive = 0,
    defaultClassName = 'col-md-2 menu-title ';

toursTable.onclick = function() {
    if (!toursActive) {
        toursTable.className = 'col-md-offset-1 ' + defaultClassName + ' menu-title-active';
        toursActive = 1;
    } else {
        toursTable.className = 'col-md-offset-1 ' + defaultClassName;
        toursActive = 0;
    }

}

placesTable.onclick = function() {
    if (!placesActive) {
        placesTable.className = defaultClassName + ' menu-title-active';
        placesActive = 1;
    } else {
        placesTable.className = defaultClassName;
        placesActive = 0;
    }
}

hotelsTable.onclick = function() {
    if (!hotelsActive) {
        hotelsTable.className = defaultClassName + ' menu-title-active';
        hotelsActive = 1;
    } else {
        hotelsTable.className = defaultClassName;
        hotelsActive = 0;
    }
}

ordersTable.onclick = function() {
    if (!ordersActive) {
        ordersTable.className = defaultClassName + ' menu-title-active';
        ordersActive = 1;
    } else {
        ordersTable.className = defaultClassName;
        ordersActive = 0;
    }
}