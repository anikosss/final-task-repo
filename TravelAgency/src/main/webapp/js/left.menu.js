var
    menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
    showLeft = document.getElementById( 'showLeft' ),
    menuActive = 0,
    body = document.body;


    showLeft.onclick = function() {
        if (!menuActive) {
            menuLeft.className = 'cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left cbp-spmenu-open'
            menuActive = 1;
        } else {
            menuLeft.className = 'cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left'
            menuActive = 0;
        }
    }

